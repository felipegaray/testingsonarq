﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using OTTWeb.Common.Http.API;
using System.Threading.Tasks;

namespace OTTWeb.Common.Middlewares
{
    /// <summary>
    /// Middleware used to initialize IForwardHeadersService 
    /// </summary>
    public class ForwardedHeadersMiddlewere
    {
        private readonly RequestDelegate _next;


        public ForwardedHeadersMiddlewere(RequestDelegate next)
        {
            this._next = next;
        }

        public Task Invoke(HttpContext httpcontext, IForwardHeadersService proxiedHeaderService)
        {
            proxiedHeaderService.Initialize(httpcontext.Request);
            return _next(httpcontext);
        }
    }

    public static class ForwardedHeadersMiddlewereExtensions
    {
        public static IApplicationBuilder UseForwardedHeadersMiddlewere(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ForwardedHeadersMiddlewere>();
        }
    }
}
