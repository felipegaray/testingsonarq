﻿using OTTWeb.Common.Services.Models;
using System;

namespace OTTWeb.Common.Services.Infrastructure
{
    public interface IServiceResponseFactory
    {
        /// <summary>
        /// This method creates a specific response.
        /// </summary>
        /// <param name="contentData">The response</param>
        /// <returns>A response Object</returns>
        ServiceResponse<T> CreateServiceResponse<T>(T contentData) where T : IServiceModel;

        /// <summary>
        /// This method creates the ServiceResponse with the respective exception
        /// </summary>
        /// <param name="exception">The Exception</param>
        /// <param name="statusCode">The HttpStatusCode</param>
        /// <returns></returns>
        ServiceResponse<T> CreateServiceResponse<T>(T contentData, Exception exception) where T : IServiceModel;
    }
}