﻿using OTTWeb.Common.Services.Models;
using OTTWeb.Common.Proxy.Exceptions;
using OTTWeb.Common.Services.Models.Errors;
using System.Net;
using Newtonsoft.Json;
using System;

namespace OTTWeb.Common.Services.Infrastructure
{
    public class ServiceResponseFactory : IServiceResponseFactory
    {
        /// <summary>
        /// This method creates a specific response.
        /// </summary>
        /// <param name="contentData">The response</param>
        /// <returns>A response Object</returns>
        public ServiceResponse<T> CreateServiceResponse<T>(T contentData) where T : IServiceModel
        {
            ServiceResponse<T> serviceResponse = new ServiceResponse<T>(contentData)
            {
                IsSuccessful = true,
                StatusCode = HttpStatusCode.OK,
                StatusDescription = "OK"
            };
            return serviceResponse;
        }

        /// <summary>
        /// This method creates the ServiceResponse with the respective exception
        /// </summary>
        /// <param name="exception">The Exception</param>
        /// <param name="statusCode">The HttpStatusCode</param>
        /// <returns></returns>
        public ServiceResponse<T> CreateServiceResponse<T>(T contentData, Exception exception) where T : IServiceModel

        {
            ServiceResponse<T> serviceResponse = new ServiceResponse<T>(contentData)
            {
                IsSuccessful = false,
                StatusDescription = exception.Message,
                ErrorData = new ErrorData()
                {
                    ErrorCode = (exception.Data["ErrorCode"] == null ? string.Empty : exception.Data["ErrorCode"].ToString()),
                    ErrorMessage = (exception.Data["ErrorMessage"] == null ? string.Empty : exception.Data["ErrorMessage"].ToString())
                }
            };
            switch (exception)
            {
                case ProxyBusinessException ex:
                    serviceResponse.StatusCode = (HttpStatusCode)422;                   
                    break;
                case ProxyGatewayException ex:
                    serviceResponse.StatusCode = HttpStatusCode.BadGateway;
                    break;
                case ProxyUnhandledException ex:
                    serviceResponse.StatusCode = HttpStatusCode.InternalServerError;
                    break;
                case ProxyUnauthorizedException ex:
                    serviceResponse.StatusCode = HttpStatusCode.Unauthorized;
                    break;
                case TimeoutException te:
                case JsonException je:
                default:
                    serviceResponse.StatusCode = HttpStatusCode.BadGateway;
                    break;
            };
            return serviceResponse;
        }
    }
}
