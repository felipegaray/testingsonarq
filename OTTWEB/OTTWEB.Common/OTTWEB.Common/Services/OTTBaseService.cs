﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace OTTWEB.Common.Services
{
    public class OTTBaseService
    {
        protected OTTBaseService() { }

        /// <summary>
        /// Creates a collection with all parameters and values from a search request by reflection
        /// </summary>
        /// <param name="request">The request object</param>
        /// <returns>A collection with Property Name as a Key and the Property Value as Value </returns>
        protected Dictionary<string, string> CreateQueryStringForProxy(object request)
        {
            Dictionary<string, string> requestCollection = new Dictionary<string, string>();
            //By Reflection gets the type of the instance
            Type requestType = request.GetType();
            //By Reflection gets all properties
            PropertyInfo[] properties = requestType.GetProperties();
            //Iterates the properties collection
            foreach (PropertyInfo property in properties)
            {
                //Gets the property name
                string param_name = property.Name.ToString().ToLower();
                //Gets the property value if contains this one
                string param_value = (property.GetValue(request) != null ? property.GetValue(request).ToString() : null);
                if (param_value != null)
                    //Add the Property Name as a Key and the Property Value as Value in the collection
                    requestCollection.Add(param_name, param_value);
            }

            return requestCollection;
        }
    }
}
