﻿using Newtonsoft.Json;
using OTTWeb.Common.Services.Models.Errors;
using System.Net;

namespace OTTWeb.Common.Services.Models
{
    public class ServiceResponse<T>: IServiceResponse where T : IServiceModel
    {
        public ServiceResponse(T contentData)
        {
            ContentData = contentData;
        }

        public T ContentData { get; set; }
        public bool IsSuccessful { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string StatusDescription { get; set; }
        [JsonIgnore]
        public ErrorData ErrorData { get; set; }
    }
}
