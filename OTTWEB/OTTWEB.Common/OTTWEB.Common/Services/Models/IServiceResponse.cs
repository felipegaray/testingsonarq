﻿using OTTWeb.Common.Services.Models.Errors;
using System.Net;

namespace OTTWeb.Common.Services.Models
{
    public interface IServiceResponse
    {
         bool IsSuccessful { get; set; }
         HttpStatusCode StatusCode { get; set; }
         string StatusDescription { get; set; }       
         ErrorData ErrorData { get; set; }
    }

}
