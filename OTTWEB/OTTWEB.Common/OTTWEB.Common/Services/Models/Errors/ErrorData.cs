﻿namespace OTTWeb.Common.Services.Models.Errors
{
    public class ErrorData
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
