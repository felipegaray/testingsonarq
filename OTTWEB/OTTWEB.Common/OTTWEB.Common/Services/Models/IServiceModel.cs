﻿namespace OTTWeb.Common.Services.Models
{
    /// <summary>
    /// Represents the payload on the Service Layer DTO <see cref="ServiceResponse{T}"/>
    /// </summary>
    public interface IServiceModel { };
}
