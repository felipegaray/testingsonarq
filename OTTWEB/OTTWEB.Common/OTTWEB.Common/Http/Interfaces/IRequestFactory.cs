﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace OTTWeb.Common.Http.Interfaces
{
    public interface IRequestFactory
    {
        Task<HttpResponseMessage> Get(UriService requestUri);
        Task<HttpResponseMessage> Get(UriService requestUri, string token);
        Task<HttpResponseMessage> Get(UriService requestUri, string token, Dictionary<string, string> queryString);

        Task<HttpResponseMessage> Post(UriService requestUri, object content);
        Task<HttpResponseMessage> Post(UriService requestUri, object content, string token);

        Task<HttpResponseMessage> Put(UriService requestUri, object content);
        Task<HttpResponseMessage> Put(UriService requestUri, object content, string token);

        Task<HttpResponseMessage> Delete(UriService requestUri);
    }
}