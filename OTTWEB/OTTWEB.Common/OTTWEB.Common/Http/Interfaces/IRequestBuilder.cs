﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace OTTWeb.Common.Http.Interfaces
   
{
    public interface IRequestBuilder
    {
        /// <summary>
        /// Sets the Http Method
        /// </summary>
        /// <param name="method">A RestSharp Http Method from the enum</param>
        /// <returns>The RestSharpRequestBuilder Object</returns>
        HttpRequestBuilder AddMethod(HttpMethod method);

        /// <summary>
        /// Sets the Service Base Url
        /// </summary>
        /// <param name="baseUri">The Service Base Url</param>
        /// <returns>The RestSharpRequestBuilder Object</returns>
        HttpRequestBuilder AddBaseUri(string baseUri);

        /// <summary>
        /// Sets the endpoint path
        /// </summary>
        /// <param name="endpoint">The endpoint path</param>
        /// <returns>The RestSharpRequestBuilder Object</returns>
        HttpRequestBuilder AddRelativeUri(string endpoint);

        /// <summary>
        /// Sets the Content object to be sent in the Http method
        /// </summary>
        /// <param name="content">The content</param>
        /// <returns>The RestSharpRequestBuilder Object</returns>
        HttpRequestBuilder AddContent(HttpContent content);

        /// <summary>
        /// Sets the Token to be sent in the Authorization header
        /// </summary>
        /// <param name="bearerToken">The token value</param>
        /// <returns>The RestSharpRequestBuilder Object</returns>
        HttpRequestBuilder AddBearerToken(string bearerToken);

        /// <summary>
        /// Sets the Accept Header type
        /// </summary>
        /// <param name="acceptHeader">The Accept Header type</param>
        /// <returns>The RestSharpRequestBuilder Object</returns>
        HttpRequestBuilder AddAcceptHeader(string acceptHeader);

        /// <summary>
        /// Sets the Allow redirect value
        /// </summary>
        /// <param name="allowAutoRedirect">The allow redirect value</param>
        /// <returns>The RestSharpRequestBuilder Object</returns>
        HttpRequestBuilder AddAllowAutoRedirect(bool allowAutoRedirect);

        /// <summary>
        /// Sets the Timeout
        /// </summary>
        /// <param name="timeout">the timeout</param>
        /// <returns>The RestSharpRequestBuilder Object</returns>
        HttpRequestBuilder AddTimeout(TimeSpan timeout);

        /// <summary>
        /// Sets a collection of querystring parameters and values
        /// </summary>
        /// <param name="queryString">The Querystring collection</param>
        /// <returns>The RestSharpRequestBuilder Object</returns>
        HttpRequestBuilder AddQueryStringParameter(Dictionary<string, string> queryString);

        /// <summary>
        /// Execute the Http Method via HttpClient
        /// </summary>
        /// <returns>An Http Response Message</returns>
        Task<HttpResponseMessage> SendAsync();
    }
}