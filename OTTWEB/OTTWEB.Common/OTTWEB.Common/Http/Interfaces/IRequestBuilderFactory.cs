﻿namespace OTTWeb.Common.Http.Interfaces
{
    public interface IRequestBuilderFactory
    {
        IRequestBuilder GetInstance();
    }
}