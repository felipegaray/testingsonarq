﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace OTTWeb.Common.Http.Serializer
{
    public class JsonContent : StringContent
    {
        public JsonContent(object value)
            : base(JsonConvert.SerializeObject(value, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore                
            }),
                  Encoding.UTF8, "application/json")
        {
        }

        public JsonContent(object value, string mediaType)
            : base(JsonConvert.SerializeObject(value, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            }),
                  Encoding.UTF8, mediaType)
        {
        }
    }
}
