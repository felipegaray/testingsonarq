﻿using Microsoft.Extensions.DependencyInjection;
using OTTWeb.Common.Http.API;
using OTTWeb.Common.Http.API.Configuration;
using OTTWeb.Common.Http.Interfaces;

namespace OTTWeb.Common.Http.Extensions.Microsoft.DependencyInjection

{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddHForwardHeadersService(this IServiceCollection services, ProxySettings config)
        {
            services.AddScoped<IForwardHeadersService>(x => new ForwardHeadersService(config.ForwardHeaderServiceSettings));
            services.AddScoped<IRequestBuilderService>(x => new RequestBuilderService(config.RequestBuilderSettings));
            services.AddScoped<IRequestBuilderFactory, HttpRequestBuilderFactory>();
            services.AddScoped<IRequestFactory, HttpRequestFactory>();
            return services;
        }
    }
}
