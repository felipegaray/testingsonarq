﻿using OTTWeb.Common.Http.API;
using OTTWeb.Common.Http.API.Configuration;
using OTTWeb.Common.Http.Interfaces;

namespace OTTWeb.Common.Http
{
    public class HttpRequestBuilderFactory : IRequestBuilderFactory
    {
        private IForwardHeadersService _forwarddHeaderService = null;
        private IRequestBuilderService _requestBuilderService = null;

        public HttpRequestBuilderFactory() { }

        public HttpRequestBuilderFactory(IForwardHeadersService forwardHeaderService, IRequestBuilderService requestBuilderService)
        {
            _forwarddHeaderService = forwardHeaderService;
            _requestBuilderService = requestBuilderService;
        }

        public IRequestBuilder GetInstance()
        {
            if (_forwarddHeaderService != null)
            {
                return new HttpRequestBuilder(_forwarddHeaderService, _requestBuilderService);
            }
            return new HttpRequestBuilder();

        }

    }
}
