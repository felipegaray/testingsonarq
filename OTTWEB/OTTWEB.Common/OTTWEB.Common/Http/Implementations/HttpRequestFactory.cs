﻿using OTTWeb.Common.Http.Interfaces;
using OTTWeb.Common.Http.Serializer;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace OTTWeb.Common.Http
{
    public class HttpRequestFactory : IRequestFactory
    {
        IRequestBuilderFactory _requestBuilderFactory;

        public HttpRequestFactory(IRequestBuilderFactory requestBuilderFactory)
        {
            _requestBuilderFactory = requestBuilderFactory;
        }

        #region GET
        public async Task<HttpResponseMessage> Get(UriService requestUri)=> await Get(requestUri, "");
        public async Task<HttpResponseMessage> Get(UriService requestUri, string token) => await Get(requestUri, token, null);
        public async Task<HttpResponseMessage> Get(UriService requestUri, string token, Dictionary<string, string> queryString)
        {
            var builder = _requestBuilderFactory.GetInstance()
                                .AddMethod(HttpMethod.Get)
                                .AddBaseUri(requestUri.ServiceBaseUri)
                                .AddRelativeUri(requestUri.ServiceRelativeUri)
                                .AddBearerToken(token)
                                .AddQueryStringParameter(queryString);

            return await builder.SendAsync();
        }
        #endregion GET

        #region POST
        public async Task<HttpResponseMessage> Post(UriService requestUri, object value) => await Post(requestUri, value, "");
        public async Task<HttpResponseMessage> Post(UriService requestUri, object value, string token)
        {
            var builder = _requestBuilderFactory.GetInstance()
                                .AddMethod(HttpMethod.Post)
                                .AddBaseUri(requestUri.ServiceBaseUri)
                                .AddRelativeUri(requestUri.ServiceRelativeUri)
                                .AddContent(new JsonContent(value))
                                .AddBearerToken(token);

            return await builder.SendAsync();
        }
        #endregion POST

        #region PUT
        public async Task<HttpResponseMessage> Put(UriService requestUri, object value) => await Put(requestUri, value, "");
        public async Task<HttpResponseMessage> Put(UriService requestUri, object value, string token)
        {
            var builder = _requestBuilderFactory.GetInstance()
                                .AddMethod(HttpMethod.Put)
                                .AddBaseUri(requestUri.ServiceBaseUri)
                                .AddRelativeUri(requestUri.ServiceRelativeUri)
                                .AddContent(new JsonContent(value))
                                .AddBearerToken(token);

            return await builder.SendAsync();
        }
        #endregion POST

        #region DELETE
        public async Task<HttpResponseMessage> Delete(UriService requestUri)
        {
            var builder = _requestBuilderFactory.GetInstance()
                                .AddMethod(HttpMethod.Delete)
                                .AddBaseUri(requestUri.ServiceBaseUri)
                                .AddRelativeUri(requestUri.ServiceRelativeUri);
            return await builder.SendAsync();
        }
        #endregion DELETE

        #region "Another Http Methods"
        //public async Task<HttpResponseMessage> Patch(UriService requestUri, object value)
        //    => await Patch(requestUri, value, "");

        //public async Task<HttpResponseMessage> Patch(
        //    UriService requestUri, object value, string token)
        //{
        //    var builder = _requestBuilderFactory.GetInstance()
        //                        .AddMethod(new HttpMethod("PATCH"))
        //                        .AddBaseUri(requestUri.ServiceBaseUri)
        //                        .AddRelativeUri(requestUri.ServiceRelativeUri)
        //                        .AddContent(new PatchContent(value))
        //                        .AddBearerToken(token);

        //    return await builder.SendAsync();
        //}

        //public async Task<HttpResponseMessage> PostFile(UriService requestUri, string filePath, string apiParamName)
        //    => await PostFile(requestUri, filePath, apiParamName, "");

        //public async Task<HttpResponseMessage> PostFile(UriService requestUri, string filePath, string apiParamName, string token)
        //{
        //    var builder = _requestBuilderFactory.GetInstance()
        //                        .AddMethod(HttpMethod.Post)
        //                        .AddBaseUri(requestUri.ServiceBaseUri)
        //                        .AddRelativeUri(requestUri.ServiceRelativeUri)
        //                        .AddContent(new FileContent(filePath, apiParamName))
        //                        .AddBearerToken(token);

        //    return await builder.SendAsync();
        //}
        #endregion "Another Http Methods"
    }
}