﻿using Microsoft.AspNetCore.WebUtilities;
using OTTWeb.Common.Http.API;
using OTTWeb.Common.Http.Interfaces;
using OTTWEB.Common.Http.Extensions;
using OTTWEB.Common.Http.Implementations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace OTTWeb.Common.Http
{
    public class HttpRequestBuilder : IRequestBuilder
    {
        #region "Private Properties"
        private HttpMethod method = null;
        private Uri baseUri = null;
        private string endpoint = null;
        private HttpContent content = null;
        private string bearerToken = string.Empty;
        private string acceptHeader = "application/json";
        private TimeSpan? timeout = null; // The default timeout comes from the TimeoutHandler
        private bool allowAutoRedirect = false;
        private Dictionary<string, string> queryString = null;
        private IForwardHeadersService _forwarddHeaderService;
        private IRequestBuilderService _requestBuilderService;
        #endregion "Private Properties"

        public HttpRequestBuilder()
        {
        }

        /// <summary>
        /// Constructor With Forward Header Object
        /// </summary>
        /// <param name="forwardHeaderService">The Forward Header Service to be injected.</param>
        /// <param name="requestBuilderService">The Request Builder Service to be injected.</param>
        public HttpRequestBuilder(IForwardHeadersService forwardHeaderService, IRequestBuilderService requestBuilderService)
        {
            _forwarddHeaderService = forwardHeaderService;
            _requestBuilderService = requestBuilderService;
        }

        public HttpRequestBuilder AddMethod(HttpMethod method)
        {
            this.method = method;
            return this;
        }

        public HttpRequestBuilder AddContent(HttpContent content)
        {
            this.content = content;
            return this;
        }

        public HttpRequestBuilder AddBearerToken(string bearerToken)
        {
            this.bearerToken = bearerToken;
            return this;
        }

        public HttpRequestBuilder AddAcceptHeader(string acceptHeader)
        {
            this.acceptHeader = acceptHeader;
            return this;
        }

        public HttpRequestBuilder AddTimeout(TimeSpan timeout)
        {
            this.timeout = timeout;
            return this;
        }

        public HttpRequestBuilder AddAllowAutoRedirect(bool allowAutoRedirect)
        {
            this.allowAutoRedirect = allowAutoRedirect;
            return this;
        }

        public HttpRequestBuilder AddBaseUri(string baseUri)
        {
            this.baseUri = new Uri(baseUri);
            return this;
        }

        public HttpRequestBuilder AddRelativeUri(string relativeUri)
        {
            this.endpoint = string.IsNullOrEmpty(relativeUri) ? string.Empty : relativeUri;
            return this;
        }

        public HttpRequestBuilder AddQueryStringParameter(Dictionary<string, string> queryString)
        {
            this.queryString = queryString;
            return this;
        }

        public async Task<HttpResponseMessage> SendAsync()
        {
            //Set the timeout from the request builder settings
            if (_requestBuilderService.Timeout.HasValue)
                this.timeout = TimeSpan.FromSeconds(_requestBuilderService.Timeout.Value);

            // Check required arguments
            EnsureArguments();

            //specify to use TLS 1.2 as default connection
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            // Set up request
            var request = new HttpRequestMessage
            {
                Method = this.method,
                RequestUri = new Uri(this.baseUri, this.endpoint)
            };

            //Clean all headers
            request.Headers.Clear();

            //Set all Forwarded Headers
            if (_forwarddHeaderService != null)
            {
                _forwarddHeaderService.SetForwardedHeaders(request);
            }

            //Set content
            if (this.content != null)
                request.Content = this.content;

            //Set token if it was sent
            if (!string.IsNullOrEmpty(this.bearerToken))
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", this.bearerToken);

            //Clean all Accept Headers
            request.Headers.Accept.Clear();

            //Set the Accept Headers
            if (!string.IsNullOrEmpty(this.acceptHeader))
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(this.acceptHeader));

            //Validate and add querystring
            if (this.queryString != null)
            {
                string requestUriWithParameters = QueryHelpers.AddQueryString(request.RequestUri.AbsoluteUri, queryString);
                request.RequestUri = new Uri(requestUriWithParameters);
            }

            // Set the request timeout
            if (this.timeout.HasValue)
            {
                request.SetTimeout(this.timeout.Value);
            }            

            // Setup client
            //TODO: Possible enhancement >> Expose the Handler in order to sent Performance framework and Retry Startegy handlers
            var handler = new TimeoutHandler
            {
                InnerHandler = new HttpClientHandler
                {
                    AllowAutoRedirect = this.allowAutoRedirect
                }
            };

            using (var client = new HttpClient(handler))
            {
                client.Timeout = Timeout.InfiniteTimeSpan;

                return await client.SendAsync(request);
            };
        }

        #region " Private "
        private void EnsureArguments()
        {
            if (this.method == null)
                throw new ArgumentNullException("Http Method Missed");

            if (this.baseUri == null)
                throw new ArgumentNullException("Base Uri Empty");
        }
        #endregion
    }
}
