﻿using Microsoft.Extensions.Primitives;
using System.Collections.Generic;

namespace OTTWeb.Common.Http.API.Configuration
{
    public class ForwardHeaderServiceSettings
    {
        public List<string> ForwardedHeaderkeys { get; set; }
        public Dictionary<string, string> FixedHeaders { get; set; }

    }
}
