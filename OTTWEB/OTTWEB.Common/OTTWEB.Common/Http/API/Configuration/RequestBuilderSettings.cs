﻿namespace OTTWeb.Common.Http.API.Configuration
{
    public class RequestBuilderSettings
    {
        public int? Timeout { get; set; }
        public string TimeoutEnvironmentVariableName { get; set; }
    }
}
