﻿namespace OTTWeb.Common.Http.API.Configuration
{
    public class ProxySettings
    {
        public ForwardHeaderServiceSettings ForwardHeaderServiceSettings { get; set; }
        public RequestBuilderSettings RequestBuilderSettings { get; set; }
    }
}
