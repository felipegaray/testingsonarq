﻿using OTTWeb.Common.Http.API.Configuration;


namespace OTTWeb.Common.Http.API
{
    public class RequestBuilderService : IRequestBuilderService
    {
        public int? Timeout { get; private set; }

        /// <summary>
        ///  Creates a new instance of RequestBuilderService
        /// </summary>
        /// <param name="settings">Configuration class holding all initialization settings</param>
        public RequestBuilderService(RequestBuilderSettings settings) : this(settings?.Timeout) { }

        /// <summary>
        /// Creates a new instance of RequestBuilderService
        /// </summary>
        /// <param name="timeout">The timeout for the service</param>
        public RequestBuilderService(int? timeout) => Timeout = timeout;
    }

    public interface IRequestBuilderService
    {
        int? Timeout { get; }
    }
}
