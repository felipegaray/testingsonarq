﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using OTTWeb.Common.Http.API.Configuration;
using System.Collections.Generic;
using System.Net.Http;

namespace OTTWeb.Common.Http.API
{
    public class ForwardHeadersService : IForwardHeadersService
    {
        private Dictionary<string, StringValues> _forwardedheaders;
        private IDictionary<string, string> _fixedheaders;
        private ISet<string> _forwardedHeadersKeys;

        /// <summary>
        ///  Creates a new instance of ForwardHeadersService
        /// </summary>
        /// <param name="settings">Configuration class holding all initialization settings</param>
        public ForwardHeadersService(ForwardHeaderServiceSettings settings) : this(settings.ForwardedHeaderkeys, settings.FixedHeaders) { }

        /// <summary>
        /// Creates a new instance of ForwardHeadersService
        /// </summary>
        /// <param name="forwardedHeadersKeys">A collection of header keys to be forwarded</param>
        /// <param name="fixedheaders"> An IDictionary including the fixed headers key value pairs</param>
        public ForwardHeadersService(IEnumerable<string> forwardedHeadersKeys, IDictionary<string, string> fixedheaders)
        {
            _forwardedHeadersKeys = forwardedHeadersKeys != null ? new HashSet<string>(forwardedHeadersKeys) : new HashSet<string>();
            _fixedheaders = fixedheaders != null ? fixedheaders : new Dictionary<string, string>();
            _forwardedheaders = new Dictionary<string, StringValues>();
        }

        /// <summary>
        /// Initialize the internal forwardHeaders collection with the actual Headers values present in the AspNetCore Http Request provided
        /// </summary>
        /// <param name="httpcontext">the httprequest object containing the headers values to be forwarded</param>
        public void Initialize(HttpRequest httpRequest)
        {
            foreach (var key in _forwardedHeadersKeys)
            {
                var header = httpRequest.Headers[key];
                if (header != StringValues.Empty)
                {
                    AddHeader(key, header);
                }
            }
        }

        /// <summary>
        /// Set forwarded and fixed headers configured into an HttpRequestMessage object
        /// </summary>
        /// <param name="request">The HttpRequestMessage object whose headers want to be set</param>
        public void SetForwardedHeaders(HttpRequestMessage request)
        {
            foreach (var item in _fixedheaders)
            {
                //request.AddHeader(item.Key, item.Value);
                request.Headers.Add(item.Key, item.Value.ToString());
            }

            foreach (var item in _forwardedheaders)
            {
                if (!request.Headers.Contains(item.Key))
                {
                    //request.AddHeader(item.Key, item.Value);
                    request.Headers.Add(item.Key, item.Value.ToString());
                }
            }
        }
        
        #region Private Methods

        private void AddHeader(string key, StringValues value)
        {
            _forwardedheaders.Add(key, value);
        }

        #endregion Private Methods
    }

    public interface IForwardHeadersService
    {
        void SetForwardedHeaders(HttpRequestMessage request);
        void Initialize(HttpRequest httpcontext);
    }

}
