﻿namespace OTTWeb.Common.Http
{
    public class UriService
    {
        public string ServiceBaseUri { get; set; }
        public string ServiceRelativeUri { get; set; }
    }
}
