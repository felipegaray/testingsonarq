﻿using Newtonsoft.Json;
using System;

namespace OTTWeb.Common.Proxy.Models
{
    public class OrbisResponseModel
    {
        [JsonProperty(PropertyName = "requestID")]
        public string RequestID { get; set; }
        [JsonProperty(PropertyName ="timestamp")]
        public DateTime TimeStamp { get; set; }
    }
}
