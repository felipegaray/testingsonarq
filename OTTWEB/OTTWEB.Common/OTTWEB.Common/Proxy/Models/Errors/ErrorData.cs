﻿using Newtonsoft.Json;

namespace OTTWeb.Common.Proxy.Models.Errors
{
    public class ErrorData : OrbisResponseModel
    {
        [JsonProperty(PropertyName = "errorCode")]
        public string ErrorCode { get; set; }
        [JsonProperty(PropertyName = "errorMessage")]
        public string ErrorMessage { get; set; }
    }
}
