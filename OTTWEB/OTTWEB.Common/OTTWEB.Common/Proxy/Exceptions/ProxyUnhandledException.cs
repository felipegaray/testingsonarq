﻿using OTTWeb.Common.Proxy.Models.Errors;
using System;

namespace OTTWeb.Common.Proxy.Exceptions
{
    public class ProxyUnhandledException : ProxyBaseException
    {
        public ProxyUnhandledException()
        {
        }

        public ProxyUnhandledException(string message) : base(message)
        {
        }

        public ProxyUnhandledException(string message, ErrorData errorData) : base(message, errorData)
        {
        }

        public ProxyUnhandledException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
