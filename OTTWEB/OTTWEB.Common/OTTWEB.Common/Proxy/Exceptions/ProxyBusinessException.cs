﻿using OTTWeb.Common.Proxy.Models.Errors;
using System;

namespace OTTWeb.Common.Proxy.Exceptions
{
    public class ProxyBusinessException : ProxyBaseException
    {
        public ProxyBusinessException()
        {
        }

        public ProxyBusinessException(string message) : base(message)
        {
        }

        public ProxyBusinessException(string message, ErrorData errorData) : base(message, errorData)
        {
        }

        public ProxyBusinessException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
