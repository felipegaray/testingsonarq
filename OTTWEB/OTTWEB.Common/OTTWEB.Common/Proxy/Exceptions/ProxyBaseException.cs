﻿using OTTWeb.Common.Proxy.Models.Errors;
using System;
using System.Collections;

namespace OTTWeb.Common.Proxy.Exceptions
{
    public class ProxyBaseException : Exception
    {
        public override IDictionary Data { get; }
        public ProxyBaseException()
        {
        }

        public ProxyBaseException(string message) : base(message)
        {
        }

        public ProxyBaseException(string message, ErrorData errorData) : base($"{message} Code: {errorData.ErrorCode} >> Message: {errorData.ErrorMessage}")
        {
            Data = new System.Collections.Generic.Dictionary<string, string>()
            {
                {"ErrorCode", errorData.ErrorCode },
                {"ErrorMessage", errorData.ErrorMessage }
            };
        }

        public ProxyBaseException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
