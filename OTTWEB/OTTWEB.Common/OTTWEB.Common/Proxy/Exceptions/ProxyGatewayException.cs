﻿using OTTWeb.Common.Proxy.Models.Errors;
using System;

namespace OTTWeb.Common.Proxy.Exceptions
{
    public class ProxyGatewayException : ProxyBaseException
    {
        public ProxyGatewayException()
        {
        }

        public ProxyGatewayException(string message) : base(message)
        {
        }

        public ProxyGatewayException(string message, ErrorData errorData) : base(message, errorData)
        {
        }

        public ProxyGatewayException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
