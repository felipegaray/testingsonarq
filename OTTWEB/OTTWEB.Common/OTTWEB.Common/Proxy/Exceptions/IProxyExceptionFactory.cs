﻿using Microsoft.Extensions.Logging;
using System.Net.Http;

namespace OTTWeb.Common.Proxy.Exceptions
{
    public interface IProxyExceptionFactory
    {
        ProxyBaseException CreateException(HttpResponseMessage response, ILogger logger);
    }
}