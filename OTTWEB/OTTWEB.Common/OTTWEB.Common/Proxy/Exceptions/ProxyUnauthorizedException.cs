﻿using OTTWeb.Common.Proxy.Models.Errors;
using System;

namespace OTTWeb.Common.Proxy.Exceptions
{
    public class ProxyUnauthorizedException : ProxyBaseException
    {
        public ProxyUnauthorizedException()
        {
        }

        public ProxyUnauthorizedException(string message) : base(message)
        {
        }

        public ProxyUnauthorizedException(string message, ErrorData errorData) : base(message, errorData)
        {
        }

        public ProxyUnauthorizedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
