﻿using Microsoft.Extensions.Logging;
using OTTWeb.Common.Http.Extensions;
using OTTWeb.Common.Proxy.Models.Errors;
using System;
using System.Net;
using System.Net.Http;

namespace OTTWeb.Common.Proxy.Exceptions
{
    public class ProxyExceptionFactory : IProxyExceptionFactory
    {
        public ProxyBaseException CreateException(HttpResponseMessage response, ILogger logger)
        {
            if (response != null && !response.IsSuccessStatusCode)
            {
                ErrorData errorData = response.ContentAsType<ErrorData>();

                logger.LogError($"Time: {errorData.TimeStamp} - Url: {response.RequestMessage.RequestUri} " +
                    $"- Details:  StatusCode: {response.StatusCode} " +
                    $"- Orbis RequestID: {errorData.RequestID} - Orbis Error - {errorData.ErrorMessage}");

                switch (response.StatusCode)
                {
                    case HttpStatusCode.Unauthorized:
                        return new ProxyUnauthorizedException("Unauthorized", errorData);
                    case HttpStatusCode.InternalServerError:
                        return new ProxyGatewayException("Orbis Unhandled Exception.", errorData);
                    default:
                        return new ProxyBusinessException("Orbis Error.", errorData);
                }
            }
            else
            {
                ErrorData errorData = new ErrorData() { ErrorCode = "UnhandledException", ErrorMessage = "Unhandled Exception" };
                logger.LogError($"Time: {DateTime.Now.ToString()} - Url: {response.RequestMessage.RequestUri} " +
                    $"- Details: Unhandled Exception - ErrorCode: {errorData.ErrorCode} - ErrorMessage: {errorData.ErrorMessage}");
                return new ProxyUnhandledException("Unhandled Exception", errorData);
            }
        }
    }


}





