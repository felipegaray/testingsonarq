﻿using OTTWeb.Common.Http;
using System.Runtime.CompilerServices;

namespace OTTWEB.Common.Proxy
{
    public class OTTBaseProxy
    {
        protected OTTBaseProxy() { }

        /// <summary>
        /// Creates the uri service object as the base url and endpoint
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="endpoint"></param>
        /// <returns></returns>
        protected UriService CreateUriService(string baseUrl, string endpoint)
        {
            UriService uriServiceData = new UriService()
            {
                ServiceBaseUri = baseUrl,
                ServiceRelativeUri = endpoint
            };

            return uriServiceData;
        }

        /// <summary>
        /// Gets the name of the current async method
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected string GetActualAsyncMethodName([CallerMemberName]string name = null) => name;        
    }
}
