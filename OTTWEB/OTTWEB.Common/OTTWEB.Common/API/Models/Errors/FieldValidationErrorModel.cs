﻿using Newtonsoft.Json;

namespace OTTWeb.Common.API.Models.Errors
{
    public class FieldValidationErrorModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Field { get; }

        public string Message { get; }

        public FieldValidationErrorModel(string field, string message)
        {
            Field = field != string.Empty ? field : null;
            Message = message;
        }
    }
}
