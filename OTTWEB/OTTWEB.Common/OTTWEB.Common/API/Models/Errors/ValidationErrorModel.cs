﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace OTTWeb.Common.API.Models.Errors
{
    public class ValidationErrorModel
    {
        public string Message { get; }

        public List<FieldValidationErrorModel> Errors { get; }

        public ValidationErrorModel(ModelStateDictionary modelState)
        {
            Message = "Validation Failed";
            Errors = modelState.Keys
                    .SelectMany(key => modelState[key].Errors.Select(x => new FieldValidationErrorModel(key, x.ErrorMessage)))
                    .ToList();
        }
    }
}
