﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace OTTWeb.Common.API.Models.Errors
{
    public class ErrorResultModel
    {
        public bool IsUserFriendly { get; set; }
        public string Message { get; }
        public string Code { get; }
        public object Details { get; set; }

        public ErrorResultModel(ModelStateDictionary modelState)
        {
            Message = "Validation Failed";
            IsUserFriendly = false;
            Details = new ValidationErrorModel(modelState);
        }
        public ErrorResultModel(string message, string code, bool isUserFriendly)
        {
            Message = message;
            Code = code;
            IsUserFriendly = isUserFriendly;
        }
    }
}
