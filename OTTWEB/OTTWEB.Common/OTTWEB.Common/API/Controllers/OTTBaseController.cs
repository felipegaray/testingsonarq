using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;

namespace OTTWeb.Common.API.Controllers
{
    /// <summary>
    /// Controller Base
    /// </summary>
    public class OTTBaseController
        : Controller
    {
        /// <summary>
        /// Executes everytime when controller is called
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            StringValues header = string.Empty;
            bool converted = ControllerContext.HttpContext.Request.Headers.TryGetValue("X-Forwarded-For", out header);
            string host = (converted) ?
                header.ToString() : string.Empty;

            LogicalThreadContext.Properties["forwared"] = host;
            base.OnActionExecuting(context);
        }
    }
}