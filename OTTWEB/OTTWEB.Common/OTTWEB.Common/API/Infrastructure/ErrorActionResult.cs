﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using OTTWeb.Common.API.Models.Errors;

namespace OTTWeb.Common.API.Infrastructure
{
    public class ErrorActionResult : ObjectResult, IActionResult
    {

        public ErrorActionResult(ErrorResultModel value) : base(value)
        { }             
        public ErrorActionResult(ModelStateDictionary modelState)
           : base(new ErrorResultModel(modelState))
        {
            StatusCode = StatusCodes.Status400BadRequest;
        }
    }
}
