﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace OTTWeb.Common.API.Infrastructure
{
    /// <summary>
    /// This Attribute Validates The Model State in the Controller
    /// </summary>
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new ErrorActionResult(context.ModelState);
            }
            base.OnActionExecuting(context);
        }
    }
}
