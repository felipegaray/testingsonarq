﻿using OTTWeb.Common.API.Models.Errors;
using OTTWeb.Common.Services.Models;
using System;

namespace OTTWeb.Common.API.Infrastructure
{
    public class ErrorActionResultFactory : IErrorActionResultFactory
    {
        public ErrorActionResult GetErrorActionResult(IServiceResponse serviceResponse)
        {
            if (serviceResponse.ErrorData == null)
            {
                throw new ArgumentNullException("The errorData parametter can not be null");
            }

            //The IsUserFriendly flag will be set to true only for 400 status code returned from the service layer
            bool isUserFriendly = serviceResponse.StatusCode == System.Net.HttpStatusCode.BadRequest;

            var errorModel = new ErrorResultModel(serviceResponse.ErrorData.ErrorMessage, serviceResponse.ErrorData.ErrorCode, isUserFriendly);
            var result = new ErrorActionResult(errorModel);
            result.StatusCode = (int)serviceResponse.StatusCode;
            return result;
        }
    }
}


