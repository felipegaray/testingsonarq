﻿using OTTWeb.Common.Services.Models;
namespace OTTWeb.Common.API.Infrastructure
{
    public interface IErrorActionResultFactory
    {
        ErrorActionResult GetErrorActionResult(IServiceResponse serviceResponse);
    }
}