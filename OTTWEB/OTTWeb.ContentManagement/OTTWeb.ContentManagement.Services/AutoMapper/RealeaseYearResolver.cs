﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using DOMAIN = OTTWeb.ContentManagement.OCM.Models;

namespace OTTWeb.ContentManagement.Services.AutoMapper
{
    public class RealeaseYearResolver : IMemberValueResolver<object, object, string, int>
    {
        public int Resolve(object source, object destination, string sourceMember, int destMember, ResolutionContext context)
        {
            if (DateTime.TryParse(sourceMember, out DateTime startTime))
            {
                return startTime.Year;
            }

            return 0;
        }
    }
}
