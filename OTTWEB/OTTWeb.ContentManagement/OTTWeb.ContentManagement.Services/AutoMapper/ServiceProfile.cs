﻿using AutoMapper;
using OTTWeb.ContentManagement.OCM.Models.OCM;
using System;
using System.Collections.Generic;
using Domain = OTTWeb.ContentManagement.OCM.Models;
using SERVICE = OTTWeb.ContentManagement.Services.Models;

namespace OTTWeb.ContentManagement.Services.AutoMapper
{
    /// <summary>
    /// Contains all mapping configurations needed at service level
    /// </summary>
    public class ServiceProfile : Profile
    {
        public ServiceProfile()
        {
            CreateMap<string, DateTime?>().ConvertUsing<NullableDateTimeConverter>();

            CreateMap<Domain.Asset, SERVICE.Asset>();

            //TODO: As always we get 1 or 0 asset ID, we transform the collection to a string. Posiblle future issue if Orbis change the collection
            CreateMap<Domain.SearchAsset, SERVICE.SearchAsset>()
                .ForMember(dest => dest.AssetID, opt => opt.MapFrom(src => src.AssetIDs[0]));

            CreateMap<Domain.GetPageResponse, SERVICE.GetPageResult>();

            CreateMap<Domain.GetPageSectionResponse, SERVICE.GetPageSectionResult>();

            CreateMap<SectionAsset, SERVICE.SectionAsset>()
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, SectionAsset>(src => src));

            CreateMap<List<SERVICE.SchedulesByStation>, SERVICE.ScheduleCollection>()
                .ForPath(dest => dest.List, opt => opt.MapFrom(src => src));

            CreateMap<Page, SERVICE.Page>();

            CreateMap<Section, SERVICE.Section>();

            CreateMap<SubSection, SERVICE.SubSection>();

            CreateMap<Domain.Schedule, SERVICE.Schedule>();

            CreateMap<Domain.Station, SERVICE.Station>()
                .ForPath(dest => dest.AssetID, opt => opt.MapFrom(src => src.AssetIDs != null && src.AssetIDs.Length > 0 ? src.AssetIDs[0] : string.Empty));

            CreateMap<List<Domain.Station>, SERVICE.StationsList>()
                .ForPath(dest => dest.List, opt => opt.MapFrom(src => src));

            CreateMap<Domain.RetrieveEPGMovie, SERVICE.Movie>()
                .ForPath(dest => dest.Genres, opt => opt.MapFrom(src => src.EpgMovies[0].Genres))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.EpgMovies[0].Id))
                .ForMember(dest => dest.ResourceType, opt => opt.MapFrom(src => src.EpgMovies[0].ResourceType))
                .ForMember(dest => dest.StartTime, opt => opt.MapFrom(src => src.EpgMovies[0].StartTime))
                .ForMember(dest => dest.EndTime, opt => opt.MapFrom(src => src.EpgMovies[0].EndTime))
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.EpgMovies[0].Duration))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.EpgMovies[0].Name))
                .ForMember(dest => dest.ShortName, opt => opt.MapFrom(src => src.EpgMovies[0].ShortName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.EpgMovies[0].Description))
                .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.EpgMovies[0].ShortDescription))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.EpgMovies[0].Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.EpgMovies[0].PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.EpgMovies[0].Pictures))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.EPGMovie>(src => src.EpgMovies[0]))
                .ForMember(dest => dest.RatingAdvisories, opt => opt.MapFrom(src => src.EpgMovies[0].RatingAdvisories))
                .ForMember(dest => dest.StationID, opt => opt.MapFrom(src => src.EpgMovies[0].StationID))
                .ForMember(dest => dest.AssetID, opt => opt.MapFrom(src => src.EpgMovies[0].AssetIDs[0]))
                .ForMember(dest => dest.RealeaseYear, opt => opt.MapFrom(src => src.EpgMovies[0].RealeaseYear));

            CreateMap<Domain.RetrieveEPGShowEpisode, SERVICE.Episode>()
                .ForPath(dest => dest.Genres, opt => opt.MapFrom(src => src.EpgEpisodes[0].Genres))
                .ForMember(dest => dest.SeasonNumber, opt => opt.MapFrom(src => src.EpgEpisodes[0].Season))
                .ForMember(dest => dest.EpisodeNumber, opt => opt.MapFrom(src => src.EpgEpisodes[0].Episode))
                .ForMember(dest => dest.ShowName, opt => opt.MapFrom(src => src.EpgEpisodes[0].ShowName))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.EpgEpisodes[0].Id))
                .ForMember(dest => dest.ResourceType, opt => opt.MapFrom(src => src.EpgEpisodes[0].ResourceType))
                .ForMember(dest => dest.StartTime, opt => opt.MapFrom(src => src.EpgEpisodes[0].StartTime))
                .ForMember(dest => dest.EndTime, opt => opt.MapFrom(src => src.EpgEpisodes[0].EndTime))
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.EpgEpisodes[0].Duration))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.EpgEpisodes[0].Name))
                .ForMember(dest => dest.ShortName, opt => opt.MapFrom(src => src.EpgEpisodes[0].ShortName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.EpgEpisodes[0].Description))
                .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.EpgEpisodes[0].ShortDescription))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.EpgEpisodes[0].Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.EpgEpisodes[0].PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.EpgEpisodes[0].Pictures))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.EPGEpisode>(src => src.EpgEpisodes[0]))
                .ForMember(dest => dest.RatingAdvisories, opt => opt.MapFrom(src => src.EpgEpisodes[0].RatingAdvisories))
                .ForMember(dest => dest.StationID, opt => opt.MapFrom(src => src.EpgEpisodes[0].StationID))
                .ForMember(dest => dest.AssetID, opt => opt.MapFrom(src => src.EpgEpisodes[0].AssetIDs[0]));

            CreateMap<Domain.RetrieveEpgEvent, SERVICE.Event>()
                .ForPath(dest => dest.Genres, opt => opt.MapFrom(src => src.EpgEvents[0].Genres))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.EpgEvents[0].Id))
                .ForMember(dest => dest.ResourceType, opt => opt.MapFrom(src => src.EpgEvents[0].ResourceType))
                .ForMember(dest => dest.StartTime, opt => opt.MapFrom(src => src.EpgEvents[0].StartTime))
                .ForMember(dest => dest.EndTime, opt => opt.MapFrom(src => src.EpgEvents[0].EndTime))
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.EpgEvents[0].Duration))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.EpgEvents[0].Name))
                .ForMember(dest => dest.ShortName, opt => opt.MapFrom(src => src.EpgEvents[0].ShortName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.EpgEvents[0].Description))
                .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.EpgEvents[0].ShortDescription))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.EpgEvents[0].Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.EpgEvents[0].PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.EpgEvents[0].Pictures))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.EPGEvent>(src => src.EpgEvents[0]))
                .ForMember(dest => dest.RatingAdvisories, opt => opt.MapFrom(src => src.EpgEvents[0].RatingAdvisories))
                .ForMember(dest => dest.StationID, opt => opt.MapFrom(src => src.EpgEvents[0].StationID))
                .ForMember(dest => dest.AssetID, opt => opt.MapFrom(src => src.EpgEvents[0].AssetIDs[0]));

            CreateMap<Domain.RetrieveEpgTeamCompetition, SERVICE.TeamCompetition>()
                .ForPath(dest => dest.Genres, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].Genres))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].Id))
                .ForMember(dest => dest.ResourceType, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].ResourceType))
                .ForMember(dest => dest.StartTime, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].StartTime))
                .ForMember(dest => dest.EndTime, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].EndTime))
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].Duration))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].Name))
                .ForMember(dest => dest.ShortName, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].ShortName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].Description))
                .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].ShortDescription))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].Pictures))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.EPGTeamCompetition>(src => src.EpgTeamCompetitions[0]))
                .ForMember(dest => dest.RatingAdvisories, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].RatingAdvisories))
                .ForMember(dest => dest.StationID, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].StationID))
                .ForMember(dest => dest.AssetID, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].AssetIDs[0]))
                .ForMember(dest => dest.SportID, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].SportID))
                .ForMember(dest => dest.SportName, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].SportName))
                .ForMember(dest => dest.TournamentID, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].TournamentID))
                .ForMember(dest => dest.TournamentName, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].TournamentName))
                .ForMember(dest => dest.PrimaryTeamID, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].PrimaryTeamID))
                .ForMember(dest => dest.PrimaryTeamName, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].PrimaryTeamName))
                .ForMember(dest => dest.PrimaryTeamPictureID, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].PrimaryTeamPictureID))
                .ForMember(dest => dest.SecondaryTeamID, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].SecondaryTeamID))
                .ForMember(dest => dest.SecondaryTeamName, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].SecondaryTeamName))
                .ForMember(dest => dest.SecondaryTeamPictureID, opt => opt.MapFrom(src => src.EpgTeamCompetitions[0].SecondaryTeamPictureID));

            CreateMap<Domain.RetrieveEpgCompetition, SERVICE.Competition>()
                .ForPath(dest => dest.Genres, opt => opt.MapFrom(src => src.EpgCompetitions[0].Genres))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.EpgCompetitions[0].Id))
                .ForMember(dest => dest.ResourceType, opt => opt.MapFrom(src => src.EpgCompetitions[0].ResourceType))
                .ForMember(dest => dest.StartTime, opt => opt.MapFrom(src => src.EpgCompetitions[0].StartTime))
                .ForMember(dest => dest.EndTime, opt => opt.MapFrom(src => src.EpgCompetitions[0].EndTime))
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.EpgCompetitions[0].Duration))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.EpgCompetitions[0].Name))
                .ForMember(dest => dest.ShortName, opt => opt.MapFrom(src => src.EpgCompetitions[0].ShortName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.EpgCompetitions[0].Description))
                .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.EpgCompetitions[0].ShortDescription))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.EpgCompetitions[0].Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.EpgCompetitions[0].PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.EpgCompetitions[0].Pictures))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.EPGCompetition>(src => src.EpgCompetitions[0]))
                .ForMember(dest => dest.RatingAdvisories, opt => opt.MapFrom(src => src.EpgCompetitions[0].RatingAdvisories))
                .ForMember(dest => dest.StationID, opt => opt.MapFrom(src => src.EpgCompetitions[0].StationID))
                .ForMember(dest => dest.AssetID, opt => opt.MapFrom(src => src.EpgCompetitions[0].AssetIDs[0]))
                .ForMember(dest => dest.SportID, opt => opt.MapFrom(src => src.EpgCompetitions[0].SportID))
                .ForMember(dest => dest.SportName, opt => opt.MapFrom(src => src.EpgCompetitions[0].SportName))
                .ForMember(dest => dest.TournamentID, opt => opt.MapFrom(src => src.EpgCompetitions[0].TournamentID))
                .ForMember(dest => dest.TournamentName, opt => opt.MapFrom(src => src.EpgCompetitions[0].TournamentName))
                .ForMember(dest => dest.PrimaryPersonID, opt => opt.MapFrom(src => src.EpgCompetitions[0].PrimaryPersonID))
                .ForMember(dest => dest.PrimaryPersonName, opt => opt.MapFrom(src => src.EpgCompetitions[0].PrimaryPersonName))
                .ForMember(dest => dest.PrimaryPersonPictureID, opt => opt.MapFrom(src => src.EpgCompetitions[0].PrimaryPersonPictureID))
                .ForMember(dest => dest.SecondaryPersonID, opt => opt.MapFrom(src => src.EpgCompetitions[0].SecondaryPersonID))
                .ForMember(dest => dest.SecondaryPersonName, opt => opt.MapFrom(src => src.EpgCompetitions[0].SecondaryPersonName))
                .ForMember(dest => dest.SecondaryPersonPictureID, opt => opt.MapFrom(src => src.EpgCompetitions[0].SecondaryPersonPictureID));

            CreateMap<Domain.RetrieveVODMovie, SERVICE.Movie>()
                .ForPath(dest => dest.Genres, opt => opt.MapFrom(src => src.VodMovies[0].Genres))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.VodMovies[0].Id))
                .ForMember(dest => dest.ResourceType, opt => opt.MapFrom(src => src.VodMovies[0].ResourceType))
                .ForMember(dest => dest.StartTime, opt => opt.Ignore())
                .ForMember(dest => dest.EndTime, opt => opt.Ignore())
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.VodMovies[0].Duration))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.VodMovies[0].Name))
                .ForMember(dest => dest.ShortName, opt => opt.MapFrom(src => src.VodMovies[0].ShortName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.VodMovies[0].Description))
                .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.VodMovies[0].ShortDescription))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.VodMovies[0].Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.VodMovies[0].PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.VodMovies[0].Pictures))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.VODMovie>(src => src.VodMovies[0]))
                .ForMember(dest => dest.RatingAdvisories, opt => opt.MapFrom(src => src.VodMovies[0].RatingAdvisories))
                .ForMember(dest => dest.AssetID, opt => opt.MapFrom(src => src.VodMovies[0].AssetIDs[0]))
                .ForMember(dest => dest.RealeaseYear, opt => opt.MapFrom(src => src.VodMovies[0].RealeaseYear));

            CreateMap<Season, SERVICE.Season>()
                .ForMember(dest => dest.ReleaseYear, opt => opt.ResolveUsing<RealeaseYearResolver, string>(src => src.StartTime));

            CreateMap<Domain.GetShowResponse, SERVICE.GetShowResult>()
                .ForPath(dest => dest.SeasonsList, opt => opt.MapFrom(src => src.Seasons))
                .ForMember(dest => dest.Genres, opt => opt.MapFrom(src => src.Show[0].Genres))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Show[0].Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Show[0].Name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Show[0].Description))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.Show>(src => src.Show[0]))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.Show[0].Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.Show[0].PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.Show[0].Pictures))
                .ForMember(dest => dest.Seasons, opt => opt.MapFrom(src => src.Show[0].Seasons))
                .ForMember(dest => dest.ReleaseYear, opt => opt.ResolveUsing<RealeaseYearResolver, string>(src => src.Show[0].StartTime));

            CreateMap<Domain.SeasonEpisode, SERVICE.SeasonEpisode>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ResourceType, opt => opt.MapFrom(src => src.ResourceType))
                .ForMember(dest => dest.ShowName, opt => opt.MapFrom(src => src.ShowName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.Pictures))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.SeasonEpisode>(src => src))
                .ForMember(dest => dest.Season, opt => opt.MapFrom(src => src.Season))
                .ForMember(dest => dest.Episode, opt => opt.MapFrom(src => src.Episode.ToString("D2")))
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.Duration))
                .ForMember(dest => dest.AssetIDs, opt => opt.MapFrom(src => src.AssetIDs))
                .ForMember(dest => dest.ReleaseYear, opt => opt.ResolveUsing<RealeaseYearResolver, string>(src => src.StartTime));

            CreateMap<Domain.GetSeasonResponse, SERVICE.GetSeasonResult>()
                .ForPath(dest => dest.Episodes, opt => opt.MapFrom(src => src.Episodes))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Seasons[0].Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Seasons[0].Name))
                .ForMember(dest => dest.ShortName, opt => opt.MapFrom(src => src.Seasons[0].ShortName))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.Seasons[0].Language))
                .ForMember(dest => dest.RegionIDs, opt => opt.MapFrom(src => src.Seasons[0].RegionIDs))
                .ForMember(dest => dest.EpisodeIDs, opt => opt.MapFrom(src => src.Seasons[0].EpisodeIDs))
                .ForMember(dest => dest.SeasonNumber, opt => opt.MapFrom(src => src.Seasons[0].SeasonNumber))
                .ForMember(dest => dest.EpisodeCount, opt => opt.MapFrom(src => src.Seasons[0].EpisodeCount))
                .ForMember(dest => dest.ReleaseYear, opt => opt.ResolveUsing<RealeaseYearResolver, string>(src => src.Seasons[0].StartTime));

            CreateMap<Domain.RetrieveVODCompetition, SERVICE.Competition>()
                .ForPath(dest => dest.Genres, opt => opt.MapFrom(src => src.VodCompetitions[0].Genres))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.VodCompetitions[0].Id))
                .ForMember(dest => dest.ResourceType, opt => opt.MapFrom(src => src.VodCompetitions[0].ResourceType))
                .ForMember(dest => dest.StartTime, opt => opt.Ignore())
                .ForMember(dest => dest.EndTime, opt => opt.Ignore())
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.VodCompetitions[0].Duration))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.VodCompetitions[0].Name))
                .ForMember(dest => dest.ShortName, opt => opt.MapFrom(src => src.VodCompetitions[0].ShortName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.VodCompetitions[0].Description))
                .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.VodCompetitions[0].ShortDescription))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.VodCompetitions[0].Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.VodCompetitions[0].PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.VodCompetitions[0].Pictures))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.VODCompetition>(src => src.VodCompetitions[0]))
                .ForMember(dest => dest.RatingAdvisories, opt => opt.MapFrom(src => src.VodCompetitions[0].RatingAdvisories))
                .ForMember(dest => dest.AssetID, opt => opt.MapFrom(src => src.VodCompetitions[0].AssetIDs[0]))
                .ForMember(dest => dest.SportName, opt => opt.MapFrom(src => src.VodCompetitions[0].SportName))
                .ForMember(dest => dest.TournamentName, opt => opt.MapFrom(src => src.VodCompetitions[0].TournamentName))
                .ForMember(dest => dest.PrimaryPersonName, opt => opt.MapFrom(src => src.VodCompetitions[0].PrimaryPersonName))
                .ForMember(dest => dest.PrimaryPersonPictureID, opt => opt.MapFrom(src => src.VodCompetitions[0].PrimaryPersonPictureID))
                .ForMember(dest => dest.SecondaryPersonName, opt => opt.MapFrom(src => src.VodCompetitions[0].SecondaryPersonName))
                .ForMember(dest => dest.SecondaryPersonPictureID, opt => opt.MapFrom(src => src.VodCompetitions[0].SecondaryPersonPictureID));

            CreateMap<Domain.RetrieveVODTeamCompetition, SERVICE.TeamCompetition>()
                .ForPath(dest => dest.Genres, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].Genres))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].Id))
                .ForMember(dest => dest.ResourceType, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].ResourceType))
                .ForMember(dest => dest.StartTime, opt => opt.Ignore())
                .ForMember(dest => dest.EndTime, opt => opt.Ignore())
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].Duration))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].Name))
                .ForMember(dest => dest.ShortName, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].ShortName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].Description))
                .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].ShortDescription))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].Pictures))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.VODTeamCompetition>(src => src.VodTeamCompetitions[0]))
                .ForMember(dest => dest.RatingAdvisories, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].RatingAdvisories))
                .ForMember(dest => dest.AssetID, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].AssetIDs[0]))
                .ForMember(dest => dest.SportName, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].SportName))
                .ForMember(dest => dest.TournamentName, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].TournamentName))
                .ForMember(dest => dest.PrimaryTeamName, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].PrimaryTeamName))
                .ForMember(dest => dest.PrimaryTeamPictureID, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].PrimaryTeamPictureID))
                .ForMember(dest => dest.SecondaryTeamName, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].SecondaryTeamName))
                .ForMember(dest => dest.SecondaryTeamPictureID, opt => opt.MapFrom(src => src.VodTeamCompetitions[0].SecondaryTeamPictureID));

            CreateMap<Domain.RetrieveVODShowEpisode, SERVICE.Episode>()
                .ForPath(dest => dest.Genres, opt => opt.MapFrom(src => src.VodShowEpisodes[0].Genres))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.VodShowEpisodes[0].Id))
                .ForMember(dest => dest.ResourceType, opt => opt.MapFrom(src => src.VodShowEpisodes[0].ResourceType))
                .ForMember(dest => dest.StartTime, opt => opt.Ignore())
                .ForMember(dest => dest.EndTime, opt => opt.Ignore())
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.VodShowEpisodes[0].Duration))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.VodShowEpisodes[0].Name))
                .ForMember(dest => dest.ShortName, opt => opt.MapFrom(src => src.VodShowEpisodes[0].ShortName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.VodShowEpisodes[0].Description))
                .ForMember(dest => dest.ShortDescription, opt => opt.MapFrom(src => src.VodShowEpisodes[0].ShortDescription))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.VodShowEpisodes[0].Language))
                .ForMember(dest => dest.PictureID, opt => opt.MapFrom(src => src.VodShowEpisodes[0].PictureID))
                .ForMember(dest => dest.Pictures, opt => opt.MapFrom(src => src.VodShowEpisodes[0].Pictures))
                .ForMember(dest => dest.Rating, opt => opt.ResolveUsing<RatingsResolver, Domain.VODShowEpisode>(src => src.VodShowEpisodes[0]))
                .ForMember(dest => dest.RatingAdvisories, opt => opt.MapFrom(src => src.VodShowEpisodes[0].RatingAdvisories))
                .ForMember(dest => dest.AssetID, opt => opt.MapFrom(src => src.VodShowEpisodes[0].AssetIDs[0]))
                .ForMember(dest => dest.SeasonNumber, opt => opt.MapFrom(src => src.VodShowEpisodes[0].SeasonNumber.ToString("D2")))
                .ForMember(dest => dest.EpisodeNumber, opt => opt.MapFrom(src => src.VodShowEpisodes[0].EpisodeNumber.ToString("D2")))
                .ForMember(dest => dest.ShowName, opt => opt.MapFrom(src => src.VodShowEpisodes[0].ShowName))
                .ForMember(dest => dest.ShowID, opt => opt.MapFrom(src => src.VodShowEpisodes[0].ShowID))
                .ForMember(dest => dest.SeasonID, opt => opt.MapFrom(src => src.VodShowEpisodes[0].SeasonID))
                .ForMember(dest => dest.ReleaseYear, opt => opt.ResolveUsing<RealeaseYearResolver, string>(src => src.VodShowEpisodes[0].StartTime));
        }
    }
}