﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using DOMAIN = OTTWeb.ContentManagement.OCM.Models;

namespace OTTWeb.ContentManagement.Services.AutoMapper
{
    public class NullableDateTimeConverter : ITypeConverter<string, DateTime?>
    {
        public DateTime? Convert(string source, DateTime? destination, ResolutionContext context)
        {
            if (DateTime.TryParse(source, out DateTime result))
            {
                return result;
            }

            return null;
        }
    }
}
