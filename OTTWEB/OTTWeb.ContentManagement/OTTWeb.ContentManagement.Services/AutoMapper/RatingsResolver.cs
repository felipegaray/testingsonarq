﻿using AutoMapper;
using OTTWeb.ContentManagement.Services.Interfaces;
using DOMAIN = OTTWeb.ContentManagement.OCM.Models;

namespace OTTWeb.ContentManagement.Services.AutoMapper
{
    public class RatingsResolver : IMemberValueResolver<object, object, DOMAIN.OCMContent, string>
    {
        private readonly IParentalRating _parentalRating;

        public RatingsResolver(IParentalRating parentalRating)
        {
            _parentalRating = parentalRating;
        }

        public string Resolve(object source, object destination, DOMAIN.OCMContent sourceMember, string destinationMember, ResolutionContext context)
        {
            return _parentalRating.Get(sourceMember.Ratings, sourceMember.Language, sourceMember.ResourceType);
        }
    }
}