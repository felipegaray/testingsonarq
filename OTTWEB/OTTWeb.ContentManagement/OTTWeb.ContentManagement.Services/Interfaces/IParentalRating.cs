﻿namespace OTTWeb.ContentManagement.Services.Interfaces
{
    public interface IParentalRating
    {
        /// <summary>
        /// Gets the Parental Rating to be showed
        /// </summary>
        /// <param name="ratings">The rating collection received from Orbis</param>
        /// <param name="language">The language set in the website to determinate the rating</param>
        /// <param name="resourceType">The resource Type</param>
        /// <returns>The rating to be showed</returns>
        string Get(string[] ratings, string language, string resourceType);
    }
}
