﻿using AppDynamics;
using OTTWeb.Common.Services.Models;
using OTTWeb.ContentManagement.Services.Models;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.Services.Interfaces
{
    public interface IScheduleService
    {
        /// <summary>
        /// Gets the Schedule list
        /// </summary>
        /// <param name="scheduleParameters">The parameters for filtering the query</param>
        /// <returns>A ServiceResponse Object with the Schedules</returns>
        Task<ServiceResponse<ScheduleCollection>> GetSchedules(ScheduleParameters scheduleParameters, BusinessTransaction bt);

        /// <summary>
        /// Gets the Movie scheduled by the Id provided
        /// </summary>
        /// <param name="scheduledMovieId">The Movie Id</param>
        /// <returns>A ServiceResponse Object with the Movie</returns>
        Task<ServiceResponse<Movie>> GetScheduledMovie(string scheduledMovieId, GetScheduledContentParameters parameters, BusinessTransaction bt);

        /// <summary>
        /// Gets the Episode scheduled by the Id provided
        /// </summary>
        /// <param name="scheduledEpisodeId">The Episode Id</param>
        /// <returns>A ServiceResponse Object with the Event/Episode</returns>
        Task<ServiceResponse<Episode>> GetScheduledEpisode(string scheduledEpisodeId, GetScheduledContentParameters parameters, BusinessTransaction bt);

        /// <summary>
        /// Gets the Event scheduled by the Id provided
        /// </summary>
        /// <param name="scheduledEventId">The Event Id</param>
        /// <returns>A ServiceResponse Object with the Event</returns>
        Task<ServiceResponse<Event>> GetScheduledEvent(string scheduledEventId, GetScheduledContentParameters parameters, BusinessTransaction bt);

        /// <summary>
        /// Gets the Team Competition scheduled by the Id provided
        /// </summary>
        /// <param name="scheduledTeamCompetitionId">The Team Competition Id</param>
        /// <returns>A ServiceResponse Object with the Team Competition</returns>
        Task<ServiceResponse<TeamCompetition>> GetScheduledTeamCompetition(string scheduledTeamCompetitionId, GetScheduledContentParameters parameters, BusinessTransaction bt);

        /// <summary>
        /// Gets the Competition scheduled by the Id provided
        /// </summary>
        /// <param name="scheduledCompetitionId">The Competition Id</param>
        /// <returns>A ServiceResponse Object with the Competition</returns>
        Task<ServiceResponse<Competition>> GetScheduledCompetition(string scheduledCompetitionId, GetScheduledContentParameters parameters, BusinessTransaction bt);
    }
}
