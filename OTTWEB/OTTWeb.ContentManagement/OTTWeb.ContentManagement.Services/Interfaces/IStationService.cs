﻿using AppDynamics;
using OTTWeb.Common.Services.Models;
using OTTWeb.ContentManagement.Services.Models;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.Services.Interfaces
{
    public interface IStationService
    {
        /// <summary>
        /// Get a List of Sations
        /// </summary>
        /// <param name="stationParameters">The parameters for filtering the query</param>
        /// <param name="sessionToken">The user session token</param>
        /// <returns>A ServiceResponse Object with the Stations</returns>
        Task<ServiceResponse<StationsList>> GetStations(GetStationsParameters stationParameters, BusinessTransaction bt);

        /// <summary>
        /// Get a Station
        /// </summary>
        /// <param name="stationId">The Station Id to be searched</param>
        /// <param name="getStationParameters">The parameters for filtering the query</param>
        /// <param name="sessionToken">The user session token</param>
        /// <returns>A ServiceResponse Object with the Stations</returns>
        Task<ServiceResponse<Station>> GetStation(string stationId, GetStationParameters getStationParameters, BusinessTransaction bt);
    }
}
