﻿using AppDynamics;
using OTTWeb.Common.Services.Models;
using OTTWeb.ContentManagement.Services.Models;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.Services.Interfaces
{
    public interface IShowService
    {
        Task<ServiceResponse<GetShowResult>> GetShowAndSeasons(string showId, GetShowParameters parameters, BusinessTransaction bt);
        /// <summary>
        /// Returns a season detail and its episodes       
        /// </summary>
        /// <param name="seasonId">The id of the season to be searched</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>a season response object</returns>
        Task<ServiceResponse<GetSeasonResult>> GetSeason(string seasonId, GetSeasonParameters parameters, BusinessTransaction bt);
    }
}
