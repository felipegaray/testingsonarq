﻿using AppDynamics;
using OTTWeb.Common.Services.Models;
using OTTWeb.ContentManagement.Services.Models;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.Services.Interfaces
{
    public interface IVodService
    {
        /// <summary>
        /// Gets the vod Movie by the Id provided
        /// </summary>
        /// <param name="vodMovieId">The vod Movie Id</param>
        /// <param name="parameters">The Parameters to be sent to the proxy</param>
        /// <returns>A ServiceResponse Object with the Movie</returns>
        Task<ServiceResponse<Movie>> GetVodMovie(string vodMovieId, GetScheduledContentParameters parameters, BusinessTransaction bt);

        /// <summary>
        /// Gets the vod Commpetition by the Id provided
        /// </summary>
        /// <param name="competitionId">The competition Id</param>
        /// <param name="parameters">The Parameters to be sent to the proxy</param>
        /// <returns>A ServiceResponse Object with the Competition</returns>
        Task<ServiceResponse<Competition>> GetVodCompetition(string competitionId, GetScheduledContentParameters parameters, BusinessTransaction bt);

        /// <summary>
        /// Gets the vod Team Commpetition by the Id provided
        /// </summary>
        /// <param name="teamCompetitionId">The team competition Id</param>
        /// <param name="parameters">The Parameters to be sent to the proxy</param>
        /// <returns>A ServiceResponse Object with the Team Competition</returns>
        Task<ServiceResponse<TeamCompetition>> GetVodTeamCompetition(string teamCompetitionId, GetScheduledContentParameters parameters, BusinessTransaction bt);

        /// <summary>
        /// Gets the vod Show Episode by the Id provided
        /// </summary>
        /// <param name="episodeId">The Episode Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
       //<returns>A ServiceResponse Object with the Episode</returns>
        Task<ServiceResponse<Episode>> GetVodShowEpisode(string episodeId, GetScheduledContentParameters parameters, BusinessTransaction bt);
    }
}
