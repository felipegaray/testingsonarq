﻿using System.Collections.Generic;
using OTTWeb.ContentManagement.Services.Models;
using OTTWeb.Common.Services.Models;
using System.Threading.Tasks;
using AppDynamics;

namespace OTTWeb.ContentManagement.Services
{
    public interface IContentManagementService
    {  
        Task<ServiceResponse<GetPageResult>> GetPage(string pageId, BusinessTransaction bt);
        Task<ServiceResponse<GetPageSectionResult>> GetPageSection(int top, string pageId, string sectionId, BusinessTransaction bt);        
        Task<ServiceResponse<SearchResult>> SearchAssetsByKeyword(SearchByKeywordParameters searchByKeywordParameters, BusinessTransaction bt);
        Task<ServiceResponse<SearchAsset>> SearchAssetById(string assetID, BusinessTransaction bt);
    }
}
