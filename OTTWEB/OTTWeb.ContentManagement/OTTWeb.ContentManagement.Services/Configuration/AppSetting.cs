﻿namespace OTTWeb.ContentManagement.Services.Configuration
{
    public class AppSetting
    {
        public string OCMServiceUrl { get; set; }
        public string DirecTvOttCMApiUri { get; set; }
    }
}
