﻿using OTTWeb.ContentManagement.Services.Interfaces;
using System;
using System.Linq;

namespace OTTWeb.ContentManagement.Services.Infrastructure
{
    public class ParentalRating : IParentalRating
    {
        /// <summary>
        /// Gets the Parental Rating to be showed
        /// </summary>
        /// <param name="ratings">The rating collection received from Orbis</param>
        /// <param name="language">The language set in the website to determinate the rating</param>
        /// <param name="resourceType">The resource Type</param>
        /// <returns>The rating to be showed</returns>
        public string Get(string[] ratings, string language, string resourceType) => GetParentalRating(ratings, language, resourceType);

        #region "PRIVATE METHODS TO DETERMINATE RATING VALUE"
        #region "CONSTANTS"
        const string BRAZIL_PARENTALRATING_OFFICE = "Departamento de Justiça, Classificação, Títulos e Qualificação";
        const string MOVIE_PARENTALRATING_OFFICE = "Motion Picture Association of America";
        const string OTHER_PARENTALRATING_OFFICE = "USA Parental Rating";
        #endregion "CONSTANTS"

        /// <summary>
        /// Gets the Parental Rating to be showed
        /// </summary>
        /// <param name="ratings">The rating collection received from Orbis</param>
        /// <param name="language">The language set in the website to determinate the rating</param>
        /// <param name="resourceType">The resource Type</param>
        /// <returns>The rating to be showed</returns>
        private string GetParentalRating(string[] ratings, string language, string resourceType)
        {
            string rating = string.Empty;

            if (language.ToLower() == "pt") //Brazil
            {
                rating = GetBrazilParentalRating(ratings);
            }
            else //Out of Brazil
            {
                string ratingValueUnmapped = GetOutBrazilParentalRating(ratings, resourceType);
                rating = MappingRatingValueAgainstROLAC(ratingValueUnmapped);
            }
            return rating;
        }

        /// <summary>
        /// Gets the Parental Rating for an user inside Brazil
        /// </summary>
        /// <param name="ratings">The rating collection received from Orbis</param>
        /// <returns>The rating to be showed</returns>
        private string GetBrazilParentalRating(string[] ratings)
        {
            string rating = string.Empty;
            if (ratings != null)
                rating = ratings.Where(x => x.Contains(BRAZIL_PARENTALRATING_OFFICE)).SingleOrDefault();
            return GetRatingValue(rating);
        }

        /// <summary>
        /// Gets the Parental Rating for an user outside Brazil
        /// </summary>
        /// <param name="ratings">The rating collection received from Orbis</param>
        /// <param name="resourceType">The resource Type</param>
        /// <returns>The rating to be showed</returns>
        private string GetOutBrazilParentalRating(string[] ratings, string resourceType)
        {
            string rating = string.Empty;
            if (ratings != null)
            {
                if (resourceType.ToLower().Contains("movie")) //Movies
                    rating = ratings.Where(x => x.Contains(MOVIE_PARENTALRATING_OFFICE)).SingleOrDefault();
                else //Shows, Episodes, Events, Competitions and Team Competitions
                    rating = ratings.Where(x => x.Contains(OTHER_PARENTALRATING_OFFICE)).SingleOrDefault();
            }
            return GetRatingValue(rating);
        }

        /// <summary>
        /// Gets the Rating value as PG, PG13, R, TVG, etc
        /// </summary>
        /// <param name="ratings">The rating collection received from Orbis</param>
        /// <returns>The rating value</returns>
        private string GetRatingValue(string rating)
        {
            string value = string.Empty;
            if (!string.IsNullOrEmpty(rating))
            {
                string[] r = rating.Split('/', StringSplitOptions.RemoveEmptyEntries);
                value = r[r.Length - 1].Replace('{', ' ').Replace('}', ' ').TrimStart().TrimEnd();
            }
            return value;
        }

        /// <summary>
        /// Maps the US Parental Ratings against the ROLAC Ratings
        /// </summary>
        /// <param name="value">The US parental rating</param>
        /// <returns>The ROLAC rating value</returns>
        private string MappingRatingValueAgainstROLAC(string value)
        {
            switch (value)
            {
                case "AO":
                    return "A";
                case "G":
                    return "TP";
                case "NC-17":
                    return "21+";
                case "NR":
                    return "SC";
                case "PG":
                    return "12+";
                case "PG-13":
                    return "15+";
                case "R":
                    return "18+";
                case "TV14":
                    return "TV14";
                case "TVG":
                    return "TV TP";
                case "TVMA":
                    return "TVWA";
                case "TVPG":
                    return "TV 12";
                case "TVY":
                    return "TV J";
                case "TVY7":
                    return "TV J7";
                case "TVY7FV":
                    return "TV J7 VF";
                default:
                    return value;
            }
        }
        #endregion "PRIVATE METHODS TO DETERMINATE RATING VALUE"
    }
}
