﻿using OTTWeb.Common.Services.Models;
using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class OCMContentBase : IServiceModel
    {
        public string Id { get; set; }
        public string ResourceType { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public Int32 Duration { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Language { get; set; }
        public string PictureID { get; set; }
        public Dictionary<string, string> Pictures { get; set; }
        public string Rating { get; set; }
        public string[] RatingAdvisories { get; set; }
        public string StationID { get; set; }
        public List<Genre> Genres { get; set; }
        public string AssetID { get; set; }
    }
}
