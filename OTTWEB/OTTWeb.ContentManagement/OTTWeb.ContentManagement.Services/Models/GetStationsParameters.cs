﻿namespace OTTWeb.ContentManagement.Services.Models
{
    public class GetStationsParameters
    {
        /// <summary>
        /// The language especifier <remarks>[es|en|pt]</remarks>
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// The region specifier
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// Maximum number of results returned, used for pagination. If this parameter is not set, by default 10 records will be returned
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// Offset of the returned result set, used for pagination. If this parameter is not set, by default is set in 0
        /// </summary>
        public int Offset { get; set; }
    }
}
