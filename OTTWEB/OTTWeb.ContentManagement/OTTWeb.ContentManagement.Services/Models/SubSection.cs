﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class SubSection
    {
        /// <summary>
        /// The block ID
        /// </summary>        
        public string BlockId { get; set; }
        /// <summary>
        /// The block Label (name) of a subsection
        /// </summary>        
        public string BlockLabel { get; set; }
        /// <summary>
        /// The block name of a subsection
        /// </summary>        
        public string SubsectionName { get; set; }
        /// <summary>
        /// The List of Assets
        /// </summary>        
        public List<SectionAsset> Items { get; set; }
    }
}
