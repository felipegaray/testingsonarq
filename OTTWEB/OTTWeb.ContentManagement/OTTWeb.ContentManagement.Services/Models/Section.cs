﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class Section
    {
        /// <summary>
        /// The label
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// The section ID
        /// </summary>
        public string SectionID { get; set; }
        /// <summary>
        /// The lis of subsections in a section
        /// </summary>
        public List<SubSection> Subsections { get; set; }
    }
}
