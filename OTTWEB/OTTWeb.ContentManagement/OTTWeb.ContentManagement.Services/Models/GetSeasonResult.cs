﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class GetSeasonResult : Season
    {
        public List<SeasonEpisode> Episodes { get; set; }
    }
}
