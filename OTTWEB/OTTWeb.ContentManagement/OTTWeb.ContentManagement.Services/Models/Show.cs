﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class Show : OCMContentBase
    {
        public int Seasons { get; set; }
        public string[] SeasonIDs { get; set; }
    }
}
