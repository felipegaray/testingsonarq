﻿using OTTWeb.Common.Services.Models;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    /// <summary>
    /// The result of a search for an specific result
    /// </summary>
    public class SearchResult : IServiceModel
    {
        /// <summary>
        /// The list of assets.
        /// </summary>
        public List<SearchAsset> Assets { get; set; }
    }
}
