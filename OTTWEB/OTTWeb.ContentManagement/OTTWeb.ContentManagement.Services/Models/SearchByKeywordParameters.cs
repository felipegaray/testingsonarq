﻿namespace OTTWeb.ContentManagement.Services.Models
{
    public class SearchByKeywordParameters
    {
        public string Query { get; set; }
        public string Language { get; set; }
        public string Types { get; set; }
        public int Count { get; set; }
        public int Offset { get; set; }
        public string Include { get; set; } = "entitlements";
    }
}
