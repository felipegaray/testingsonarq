﻿using OTTWeb.Common.Services.Models;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class StationsList : IServiceModel
    {
        public List<Station> List { get; set; }
    }

    public class Station : IServiceModel
    {
        public string Id { get; set; }

        public string ResourceType { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string StationNumber { get; set; }

        public string Language { get; set; }

        public string LanguageRegion { get; set; }

        public string PictureID { get; set; }
        
        public string CallSign { get; set; }

        public string BlackoutStation { get; set; }

        public string[] RegionIDs { get; set; }

        public string AssetID { get; set; }

        public bool Entitled { get; set; }
    }
}
