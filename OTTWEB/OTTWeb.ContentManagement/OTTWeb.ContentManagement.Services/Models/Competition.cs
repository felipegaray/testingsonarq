﻿using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class Competition : OCMContentBase
    {
        public string SportID { get; set; }
        public string SportName { get; set; }
        public string TournamentID { get; set; }
        public string TournamentName { get; set; }
        public string PrimaryPersonID { get; set; }
        public string PrimaryPersonName { get; set; }
        public string PrimaryPersonPictureID { get; set; }
        public string SecondaryPersonID { get; set; }
        public string SecondaryPersonName { get; set; }
        public string SecondaryPersonPictureID { get; set; }
    }
}
