﻿namespace OTTWeb.ContentManagement.Services.Models
{
    public class Genre
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
    }
}
