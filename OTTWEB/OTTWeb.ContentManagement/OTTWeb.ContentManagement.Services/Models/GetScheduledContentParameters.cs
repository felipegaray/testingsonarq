﻿namespace OTTWeb.ContentManagement.Services.Models
{
    public class GetScheduledContentParameters
    {
        public string Language { get; set; }
    }
}
