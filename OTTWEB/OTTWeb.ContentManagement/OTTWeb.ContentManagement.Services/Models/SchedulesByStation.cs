﻿using OTTWeb.Common.Services.Models;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class ScheduleCollection : IServiceModel
    {
        public List<SchedulesByStation> List { get; set; }
    }

    public class SchedulesByStation
    {
        public string StationId { get; set; }
        public List<Schedule> Schedules { get; set; }
    }
}
