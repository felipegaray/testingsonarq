﻿using OTTWeb.Common.Services.Models;
using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class SearchAsset : IServiceModel
    {
        /// <summary>
        /// The asset ID
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The resource type
        /// </summary>
        public string ResourceType { get; set; }
        /// <summary>
        /// The start Time
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// The end Time
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// The duration
        /// </summary>
        public int Duration { get; set; }
        /// <summary>
        /// The name of the asset
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Short name of the asset
        /// </summary>
        public string ShortName { get; set; }
        /// <summary>
        /// The description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The short description
        /// </summary>
        public string ShortDescription { get; set; }
        /// <summary>
        /// The picture ID
        /// </summary>
        public string PictureID { get; set; }
        /// <summary>
        /// The dictonary of picture formats
        /// </summary>
        public Dictionary<string, string> Pictures { get; set; }
        public Dictionary<string, Dictionary<string, string>> LiveURLs { get; set; }
        public Dictionary<string, Dictionary<string, string>> VodURLs { get; set; }
        public string ShowName { get; set; }
        /// <summary>
        /// The list of genres
        /// </summary>
        public List<Genre> Genres { get; set; }
        /// <summary>
        /// The list of genres ids
        /// </summary>
        public string[] GenreIDs { get; set; }
        /// <summary>
        /// The language of the asset
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// The language region
        /// </summary>
        public string LanguageRegion { get; set; }
        /// <summary>
        /// The list of ratings
        /// </summary>
        public string[] Ratings { get; set; }
        /// <summary>
        /// The list of rating advisories
        /// </summary>
        public string[] RatingAdvisories { get; set; }
        /// <summary>
        /// The region IDs
        /// </summary>
        public string[] RegionIDs { get; set; }
        /// <summary>
        /// The number of the season
        /// </summary>
        public short Season { get; set; }
        /// <summary>
        /// The total number of seasons
        /// </summary>
        public short? Seasons { get; set; }
        /// <summary>
        /// The number of episode
        /// </summary>
        public int Episode { get; set; }
        /// <summary>
        /// The release Year
        /// </summary>
        public short ReleaseYear { get; set; }
        /// <summary>
        /// The asset ID
        /// </summary>
        public string AssetID { get; set; }
        /// <summary>
        /// The sport ID
        /// </summary>
        public string SportID { get; set; }
        /// <summary>
        /// The sport name
        /// </summary>
        public string SportName { get; set; }
        /// <summary>
        /// The tournament ID
        /// </summary>
        public string TournamentID { get; set; }
        /// <summary>
        /// The tournament Name
        /// </summary>
        public string TournamentName { get; set; }
        /// <summary>
        /// The primary person ID
        /// </summary>
        public string PrimaryPersonID { get; set; }
        /// <summary>
        /// The primary person Name
        /// </summary>
        public string PrimaryPersonName { get; set; }
        /// <summary>
        /// The primery person picture ID
        /// </summary>
        public string PrimaryPersonPictureID { get; set; }
        /// <summary>
        /// The secondary person ID
        /// </summary>
        public string SecondaryPersonID { get; set; }
        /// <summary>
        /// The secondary person Name
        /// </summary>
        public string SecondaryPersonName { get; set; }
        /// <summary>
        /// The secondary person picture ID
        /// </summary>
        public string SecondaryPersonPictureID { get; set; }
        /// <summary>
        /// The position of the asset in a subsection
        /// </summary>
        public short Position { get; set; }
        /// <summary>
        /// The station Number
        /// </summary>
        public string StationNumber { get; set; }
        /// <summary>
        /// The station Name
        /// </summary>
        public string StationName { get; set; }
        /// <summary>
        /// The station ID
        /// </summary>
        public string StationID { get; set; }
        /// <summary>
        /// The provider of the station
        /// </summary>
        public string Provider { get; set; }
        /// <summary>
        /// The provider Id of the station
        /// </summary>
        public string ProviderID { get; set; }
        /// <summary>
        /// The call sign of the station
        /// </summary>
        public string CallSign { get; set; }
        /// <summary>
        /// The blackout station
        /// </summary>
        public bool Blackout { get; set; }
        /// <summary>
        /// Is the user entitled to this asset
        /// </summary>
        public bool Entitled { get; set; }
    }
}
