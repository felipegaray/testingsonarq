﻿using OTTWeb.Common.Services.Models;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class GetPageResult : IServiceModel
    {
        public Page Page { get; set; }
    }
}
