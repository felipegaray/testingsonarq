﻿using OTTWeb.Common.Services.Models;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class GetPageSectionResult : IServiceModel
    {
        public Section Section { get; set; }
    }
}
