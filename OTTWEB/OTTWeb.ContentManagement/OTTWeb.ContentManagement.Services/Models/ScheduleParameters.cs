﻿using System;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class ScheduleParameters
    {
        /// <summary>
        /// A list of station IDs separated by comma
        /// </summary>
        public string StationIds { get; set; }
        /// <summary>
        /// The language es|en|pt
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// The region
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// Maximum number of results returned, used for pagination. If this parameter is not set, by default 10 records will be returned
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// From Datetime
        /// </summary>
        public DateTime FromDatetime { get; set; }
        /// <summary>
        /// To Datetime
        /// </summary>
        public DateTime ToDatetime { get; set; }
        /// <summary>
        /// The Resource types to be used as filter. A list of resource type separated by comma
        /// </summary>
        public string Types { get; set; }
    }
}
