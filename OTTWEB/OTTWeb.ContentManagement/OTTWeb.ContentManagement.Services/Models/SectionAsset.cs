﻿using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class SectionAsset
    {        
        public string Id { get; set; }        
        public string ResourceType { get; set; }        
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int Duration { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string PictureID { get; set; }
        public Dictionary<string, string> Pictures { get; set; }
        public string ShowName { get; set; }
        public List<Genre> Genres { get; set; }
        public string[] GenreIDs { get; set; }
        public string Language { get; set; }
        public string Rating { get; set; }
        public string[] RatingAdvisories { get; set; }
        public string[] RegionIDs { get; set; }
        public short Season { get; set; }
        public short? Seasons { get; set; }
        public int Episode { get; set; }
        public short ReleaseYear { get; set; }
        public string[] AssetIDs { get; set; }
        public string SportName { get; set; }
        public string TournamentName { get; set; }
        public string PrimaryPersonName { get; set; }
        public string PrimaryPersonPictureID { get; set; }
        public string SecondaryPersonName { get; set; }
        public string SecondaryPersonPictureID { get; set; }
        public short Position { get; set; }
        public string StationNumber { get; set; }
        public string StationName { get; set; }
        public string StationID { get; set; }
        public string Provider { get; set; }
        public string ProviderID { get; set; }
        public string CallSign { get; set; }
        public bool Blackout { get; set; }
        public bool Entitled { get; set; }
    }
}
