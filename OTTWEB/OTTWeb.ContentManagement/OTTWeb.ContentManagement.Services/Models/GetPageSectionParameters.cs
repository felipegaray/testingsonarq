﻿namespace OTTWeb.ContentManagement.Services.Models
{
    public class GetPageSectionParameters
    {
        public string Region { get; set; }
    }
}
