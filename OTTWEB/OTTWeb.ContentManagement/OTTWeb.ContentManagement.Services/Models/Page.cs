﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class Page
    {
        /// <summary>
        /// The title of the page
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// The list of sections in the page
        /// </summary>
        public List<Section> Sections { get; set; }
    }
}
