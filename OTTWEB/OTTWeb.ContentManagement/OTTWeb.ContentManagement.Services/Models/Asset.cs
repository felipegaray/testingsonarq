﻿using OTTWeb.Common.Services.Models;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class Asset : IServiceModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Keywords { get; set; }
        public List<string> Categories { get; set; }
        public Dictionary<string, string> LivePlaybackURLs { get; set; }
        public Dictionary<string, string> VodPlaybackURLs { get; set; }
        public Dictionary<string, string> ImageURLs { get; set; }
        public bool Islive { get; set; }
        public string SubType { get; set; }

    }
}
