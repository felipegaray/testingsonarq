﻿using OTTWeb.Common.Services.Models;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class Season : IServiceModel
    {
        public string Id { get; set; } 
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Language { get; set; }
        public string[] RegionIDs { get; set; }        
        public string[] EpisodeIDs { get; set; }
        public int ReleaseYear { get; set; }
        public short SeasonNumber { get; set; }
        public short EpisodeCount { get; set; }
    }
}
