﻿using OTTWeb.Common.Services.Models;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class GetShowResult : IServiceModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Language { get; set; }
        public string Rating { get; set; }
        public int ReleaseYear { get; set; }
        public string PictureID { get; set; }
        public Dictionary<string, string> Pictures { get; set; }
        public int Seasons { get; set; }    
        public List<Season> SeasonsList { get; set; }
        public List<Genre> Genres { get; set; }
    }
}
