﻿namespace OTTWeb.ContentManagement.Services.Models
{
    public class Episode : OCMContentBase
    {
        public string ShowName { get; set; }
        public string EpisodeNumber { get; set; }
        public string SeasonNumber { get; set; }
        public string ShowID { get; set; }
        public string SeasonID { get; set; }
        public Show Show { get; set; }
        public int ReleaseYear { get; set; }
    }
}
