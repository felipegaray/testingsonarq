﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OTTWeb.ContentManagement.Services.Models
{
    public class SeasonEpisode:OCMContentBase
    {  
        public string ShowName { get; set; }          
        public string Season { get; set; }
        public string Episode { get; set; }        
        public string[] AssetIDs { get; set; }
        public int ReleaseYear { get; set; }
    }
}
