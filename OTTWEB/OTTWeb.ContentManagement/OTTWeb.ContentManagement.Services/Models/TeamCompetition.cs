﻿namespace OTTWeb.ContentManagement.Services.Models
{
    public class TeamCompetition : OCMContentBase
    {
        public string SportID { get; set; }
        public string SportName { get; set; }
        public string TournamentID { get; set; }
        public string TournamentName { get; set; }
        public string PrimaryTeamID { get; set; }
        public string PrimaryTeamName { get; set; }
        public string PrimaryTeamPictureID { get; set; }
        public string SecondaryTeamID { get; set; }
        public string SecondaryTeamName { get; set; }
        public string SecondaryTeamPictureID { get; set; }
    }
}
