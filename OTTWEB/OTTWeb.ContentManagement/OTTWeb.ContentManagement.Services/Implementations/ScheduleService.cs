﻿using AppDynamics;
using AutoMapper;
using Microsoft.Extensions.Options;
using OTTWeb.Common.Services.Infrastructure;
using OTTWeb.Common.Services.Models;
using OTTWeb.ContentManagement.OCM.Interfaces;
using OTTWeb.ContentManagement.Services.Configuration;
using OTTWeb.ContentManagement.Services.Interfaces;
using OTTWeb.ContentManagement.Services.Models;
using OTTWEB.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Domain = OTTWeb.ContentManagement.OCM.Models;

namespace OTTWeb.ContentManagement.Services.Implementations
{
    public class ScheduleService : OTTBaseService, IScheduleService
    {
        #region Injected Services
        public AppSetting _appSettings;
        private IMapper _mapper;
        private IOCMProxy _proxy;
        private IServiceResponseFactory _responseFactory;
        #endregion

        #region Constructors
        public ScheduleService(IOptions<AppSetting> settings, IMapper mapper, IOCMProxy proxy, IServiceResponseFactory responseFactory)
        {
            _appSettings = settings.Value;
            _mapper = mapper;
            _proxy = proxy;
            _responseFactory = responseFactory;
        }
        #endregion

        #region "ISchedule IMember Actions"
        /// <summary>
        /// Gets the Schedule list
        /// </summary>
        /// <param name="scheduleParameters">The parameters for filtering the query</param>       
        /// <returns>A ServiceResponse Object with the Schedules</returns>
        public async Task<ServiceResponse<ScheduleCollection>> GetSchedules(ScheduleParameters scheduleParameters, BusinessTransaction bt)
        {
            ServiceResponse<ScheduleCollection> serviceResponse = null;
            try
            {
                string querystring = $"stationIDs={scheduleParameters.StationIds}" +
                                     $"&language={scheduleParameters.Language}" +
                                     $"&region={scheduleParameters.Region}" +
                                     $"&count={scheduleParameters.Count}" +
                                     $"&startTime.lte={scheduleParameters.ToDatetime.ToString("s")}Z" +
                                     $"&endTime.gte={scheduleParameters.FromDatetime.ToString("s")}Z" +
                                     (!string.IsNullOrEmpty(scheduleParameters.Types) ? $"&types={scheduleParameters.Types}" : $"{string.Empty}");
                //Gets all schedules for all stations id sent
                List<Domain.Schedule> response = await _proxy.ReturnsAListOfScheduledContent(querystring, bt);
                if (response != null && response.Count > 0)
                {
                    List<SchedulesByStation> schedulesByStationList = new List<SchedulesByStation>();
                    List<Schedule> schedules = _mapper.Map<List<Schedule>>(response);

                    schedulesByStationList = schedules
                                        //Group by Station ID
                                        .GroupBy(u => u.StationID)
                                        //Project to List<SchedulesByStation>
                                        .Select(grp => new SchedulesByStation() { StationId = grp.Key, Schedules = grp.OrderBy(s => s.StartTime).ToList() })
                                        .ToList();

                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<ScheduleCollection>(schedulesByStationList));
                }
                else
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(new ScheduleCollection());
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<ScheduleCollection>(null, proxyExeption);
            }
        }

        public async Task<ServiceResponse<Movie>> GetScheduledMovie(string scheduledMovieId, GetScheduledContentParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<Movie> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the asset information in the response, specially the LIVE or VOD Url
                querystring.Add("include", "assets");
                //Gets the movie information
                Domain.RetrieveEPGMovie response = await _proxy.RetrieveScheduledMovieFromTheEPG(scheduledMovieId, querystring, bt);

                if (response != null)
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<Movie>(response));
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<Movie>(null, proxyExeption);
            }
        }

        public async Task<ServiceResponse<Episode>> GetScheduledEpisode(string scheduledEpisodeId, GetScheduledContentParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<Episode> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the asset information in the response, specially the LIVE or VOD Url
                querystring.Add("include", "assets");
                //Gets the movie information
                Domain.RetrieveEPGShowEpisode response = await _proxy.RetrieveScheduledShowEpisodeFromTheEPG(scheduledEpisodeId, querystring, bt);

                if (response != null)
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<Episode>(response));
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<Episode>(null, proxyExeption);
            }
        }

        public async Task<ServiceResponse<Event>> GetScheduledEvent(string scheduledEventId, GetScheduledContentParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<Event> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the asset information in the response, specially the LIVE or VOD Url
                querystring.Add("include", "assets");
                //Gets the Event information
                Domain.RetrieveEpgEvent response = await _proxy.RetrieveScheduledEventFromTheEPG(scheduledEventId, querystring, bt);

                if (response != null)
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<Event>(response));
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<Event>(null, proxyExeption);
            }
        }

        public async Task<ServiceResponse<TeamCompetition>> GetScheduledTeamCompetition(string scheduledTeamCompetitionId, GetScheduledContentParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<TeamCompetition> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the asset information in the response, specially the LIVE or VOD Url
                querystring.Add("include", "assets");
                //Gets the Event information
                Domain.RetrieveEpgTeamCompetition response = await _proxy.RetrieveScheduledTeamGameFromTheEPG(scheduledTeamCompetitionId, querystring, bt);

                if (response != null)
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<TeamCompetition>(response));
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<TeamCompetition>(null, proxyExeption);
            }
        }

        public async Task<ServiceResponse<Competition>> GetScheduledCompetition(string scheduledCompetitionId, GetScheduledContentParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<Competition> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the asset information in the response, specially the LIVE or VOD Url
                querystring.Add("include", "assets");
                //Gets the Event information
                Domain.RetrieveEpgCompetition response = await _proxy.RetrieveScheduledGameFromTheEPG(scheduledCompetitionId, querystring, bt);

                if (response != null)
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<Competition>(response));
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<Competition>(null, proxyExeption);
            }
        }
        #endregion "ISchedule IMember Actions"       
    }
}
