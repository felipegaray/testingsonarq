﻿using AppDynamics;
using AutoMapper;
using Microsoft.Extensions.Options;
using OTTWeb.Common.Services.Infrastructure;
using OTTWeb.Common.Services.Models;
using OTTWeb.ContentManagement.OCM.Interfaces;
using OTTWeb.ContentManagement.Services.Configuration;
using OTTWeb.ContentManagement.Services.Interfaces;
using OTTWeb.ContentManagement.Services.Models;
using OTTWEB.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Domain = OTTWeb.ContentManagement.OCM.Models;

namespace OTTWeb.ContentManagement.Services.Implementations
{
    public class ShowService : OTTBaseService, IShowService
    {
        #region Injected Services
        public AppSetting _appSettings;
        private IMapper _mapper;
        private IOCMProxy _proxy;
        private IServiceResponseFactory _responseFactory;
        #endregion

        #region Constructors
        public ShowService(IOptions<AppSetting> settings, IMapper mapper, IOCMProxy proxy, IServiceResponseFactory responseFactory)
        {
            _appSettings = settings.Value;
            _mapper = mapper;
            _proxy = proxy;
            _responseFactory = responseFactory;
        }
        #endregion

        #region "IShow IMember Actions"

        public async Task<ServiceResponse<GetShowResult>> GetShowAndSeasons(string showId, GetShowParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<GetShowResult> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the seasons information of the show
                querystring.Add("include", "seasons");
                //Gets the movie information
                Domain.GetShowResponse response = await _proxy.GetShowAndSeasons(showId, querystring, bt);

                if (response != null)
                {
                    GetShowResult show = _mapper.Map<GetShowResult>(response);
                    serviceResponse = _responseFactory.CreateServiceResponse(show);
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<GetShowResult>(null, proxyExeption);
            }
        }

        /// <summary>
        /// Returns a season detail and its episodes       
        /// </summary>
        /// <param name="seasonId">The id of the season to be searched</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>a season response object</returns>
        public async Task<ServiceResponse<GetSeasonResult>> GetSeason(string seasonId, GetSeasonParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<GetSeasonResult> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the episodes information of the season
                querystring.Add("include", "vod/episodes");
                //Gets the movie information
                Domain.GetSeasonResponse response = await _proxy.GetSeason(seasonId, querystring, bt);

                if (response != null)
                {
                    GetSeasonResult show = _mapper.Map<GetSeasonResult>(response);
                    //Order the episodes by episode Number
                    show.Episodes = show.Episodes.OrderBy(x => Convert.ToInt16(x.Episode)).ToList();
                    serviceResponse = _responseFactory.CreateServiceResponse(show);
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<GetSeasonResult>(null, proxyExeption);
            }
        }

        #endregion
    }
}
