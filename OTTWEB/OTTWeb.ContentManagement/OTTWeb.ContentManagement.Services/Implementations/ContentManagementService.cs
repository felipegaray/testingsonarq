﻿using AppDynamics;
using AutoMapper;
using Microsoft.Extensions.Options;
using OTTWeb.Common.Services.Infrastructure;
using OTTWeb.Common.Services.Models;
using OTTWeb.ContentManagement.OCM.Interfaces;
using OTTWeb.ContentManagement.Services.Configuration;
using OTTWeb.ContentManagement.Services.Models;
using OTTWEB.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using Domain = OTTWeb.ContentManagement.OCM.Models;

namespace OTTWeb.ContentManagement.Services
{
    public class ContentManagementService : OTTBaseService, IContentManagementService
    {
        #region Injected Services
        public AppSetting _appSettings;
        private IMapper _mapper;
        private IOCMProxy _proxy;
        private IServiceResponseFactory _responseFactory;
        #endregion

        #region Constructors
        public ContentManagementService(IOptions<AppSetting> settings, IMapper mapper, IOCMProxy proxy, IServiceResponseFactory responseFactory)
        {
            _appSettings = settings.Value;
            _mapper = mapper;
            _proxy = proxy;
            _responseFactory = responseFactory;
        }
        #endregion

        #region IContentManagementService Members
        /// <summary>
        /// This method gets an asset by asset id
        /// </summary>
        /// <param name="assetID">The Asset Id</param>
        /// <returns>A Service Response object</returns>
        public async Task<ServiceResponse<SearchAsset>> SearchAssetById(string assetID, BusinessTransaction bt)
        {
            ServiceResponse<SearchAsset> serviceResponse = null;
            try
            {
                Domain.SearchAsset response = await _proxy.SearchAssetById(assetID, bt);
                if (response != null)
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<SearchAsset>(response));
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<SearchAsset>(null, proxyExeption);
            }
        }

        /// <summary>
        /// This method is used to search and return a list of matching content using the filters
        /// </summary>
        /// <param name="queryString">The query string collection</param>        
        /// <returns>A Service Response object</returns>
        public async Task<ServiceResponse<SearchResult>> SearchAssetsByKeyword(SearchByKeywordParameters searchByKeywordParameters, BusinessTransaction bt)
        {
            ServiceResponse<SearchResult> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(searchByKeywordParameters);
                //Calls the Proxy method
                var response = await _proxy.SearchAssetsByKeyword(querystring, bt);
                if (response?.Results != null && response.Results.Count > 0)
                {
                    SetEntitledPropertyOfAssets(response);
                    serviceResponse = CreateSearchKeywordServiceResponse(_mapper.Map<List<SearchAsset>>(response.Results));
                }
                else
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(new SearchResult());
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<SearchResult>(null, proxyExeption);
            }
        }

        /// <summary>
        /// Gets the data for the Page(Lander) defined in the pageID, it is organized in sections, subsections and the list of Assets
        /// The page is represented in a page ID
        /// </summary>
        /// <param name="region">The region for the search of content for the Page</param>
        /// <param name="pageId">The ID of the page to search</param>      
        /// <returns></returns>
        public async Task<ServiceResponse<GetPageResult>> GetPage(string pageId, BusinessTransaction bt)
        {
            ServiceResponse<GetPageResult> serviceResponse = null;
            try
            {
                //Calls the Proxy method
                Domain.GetPageResponse response = await _proxy.GetPage(pageId, bt);
                SetEntitledPropertyOfAssets(response?.Page?.Sections);

                if (response != null)
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<GetPageResult>(response));
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<GetPageResult>(null, proxyExeption);
            }
        }

        /// <summary>
        /// Gets the data for a specific section of a Page(Lander) defined in the pageID,
        /// it is organized in sections, subsections and the list of Assets
        /// The page is represented by the page ID and the section by the section ID 
        /// </summary>
        /// <param name="sectionParameters">The parameters received by querystring</param>
        /// <param name="pageId">The ID of the page to search</param>
        /// <param name="sectionId">The ID of the section inside the page to search</param>        
        /// <returns></returns>
        public async Task<ServiceResponse<GetPageSectionResult>> GetPageSection(int top, string pageId, string sectionId, BusinessTransaction bt)
        {
            ServiceResponse<GetPageSectionResult> serviceResponse = new ServiceResponse<GetPageSectionResult>(new GetPageSectionResult());
            try
            {
                //Calls the Proxy method
                Domain.GetPageSectionResponse response = await _proxy.GetPageSection(pageId, sectionId, bt);
                if (response != null)
                {
                    if (top > 0)
                    {
                        response.Section.Subsections[0].Items = response.Section.Subsections[0].Items.Take(top).ToList();
                    }

                    SetEntitledPropertyOfAssets(new List<Domain.OCM.Section> { response?.Section });

                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<GetPageSectionResult>(response));
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<GetPageSectionResult>(null, proxyExeption);
            }
        }

        #endregion

        #region "PRIVATE METHODS"       
        /// <summary>
        /// This method creates a specific response.
        /// </summary>
        /// <param name="assets"></param>
        /// <returns>A response Object</returns>
        private ServiceResponse<SearchResult> CreateSearchKeywordServiceResponse(List<SearchAsset> assets)
        {
            SearchResult searchResult = new SearchResult() { Assets = new List<SearchAsset>() };
            ServiceResponse<SearchResult> response = new ServiceResponse<SearchResult>(searchResult);

            if (assets != null && assets.Count > 0)
            {
                searchResult.Assets = assets;
            }

            response.IsSuccessful = true;
            response.StatusCode = HttpStatusCode.OK;
            response.StatusDescription = "OK";

            return response;
        }

        private void SetEntitledPropertyOfAssets(List<Domain.OCM.Section> sections)
        {
            sections?.ForEach
            (
                section => section?.Subsections?.ForEach
                (
                    subSection => subSection?.Items?.ForEach
                    (
                        item => item.Entitled = subSection.Entitled?.Any(id => id == item.Id) ?? false
                    )
                )
            );
        }

        private void SetEntitledPropertyOfAssets(Domain.SearchAssetsByKeywordResponse response)
        {
            response?.Results.ForEach(result => result.Entitled = response.Entitled?.Any(id => id == result.Id) ?? false);
        }
        #endregion "PRIVATE METHODS"
    }

}
