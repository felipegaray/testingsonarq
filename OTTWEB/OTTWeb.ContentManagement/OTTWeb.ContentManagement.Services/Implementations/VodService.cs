﻿using AppDynamics;
using AutoMapper;
using Microsoft.Extensions.Options;
using OTTWeb.Common.Proxy.Exceptions;
using OTTWeb.Common.Services.Infrastructure;
using OTTWeb.Common.Services.Models;
using OTTWeb.ContentManagement.OCM.Configuration;
using OTTWeb.ContentManagement.OCM.Interfaces;
using OTTWeb.ContentManagement.Services.Interfaces;
using OTTWeb.ContentManagement.Services.Models;
using OTTWEB.Common.Services;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Domain = OTTWeb.ContentManagement.OCM.Models;

namespace OTTWeb.ContentManagement.Services.Implementations
{
    public class VodService : OTTBaseService, IVodService
    {
        #region Injected Services
        public AppSetting _appSettings;
        private IMapper _mapper;
        private IOCMProxy _proxy;
        private IServiceResponseFactory _responseFactory;
        #endregion

        #region Constructors
        public VodService(IOptions<AppSetting> settings, IMapper mapper, IOCMProxy proxy, IServiceResponseFactory responseFactory)
        {
            _appSettings = settings.Value;
            _mapper = mapper;
            _proxy = proxy;
            _responseFactory = responseFactory;
        }
        #endregion

        #region "IVodService Member Actions"

        public async Task<ServiceResponse<Movie>> GetVodMovie(string vodMovieId, GetScheduledContentParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<Movie> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the asset information in the response, specially the LIVE or VOD Url
                querystring.Add("include", "assets");
                //Gets the movie information
                Domain.RetrieveVODMovie response = await _proxy.RetrieveMovieFromTheOnDemandCatalog(vodMovieId, querystring, bt);

                if (response != null)
                {
                    Movie movie = _mapper.Map<Movie>(response);
                    serviceResponse = _responseFactory.CreateServiceResponse(movie);
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<Movie>(null, proxyExeption);
            }
        }

        public async Task<ServiceResponse<Competition>> GetVodCompetition(string competitionId, GetScheduledContentParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<Competition> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the asset information in the response, specially the LIVE or VOD Url
                querystring.Add("include", "assets");
                //Gets the movie information
                Domain.RetrieveVODCompetition response = await _proxy.RetrieveGameFromTheOnDemandCatalog(competitionId, querystring, bt);

                if (response != null)
                {
                    Competition competition = _mapper.Map<Competition>(response);
                    serviceResponse = _responseFactory.CreateServiceResponse(competition);
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<Competition>(null, proxyExeption);
            }
        }

        public async Task<ServiceResponse<TeamCompetition>> GetVodTeamCompetition(string teamCompetitionId, GetScheduledContentParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<TeamCompetition> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the asset information in the response, specially the LIVE or VOD Url
                querystring.Add("include", "assets");
                //Gets the movie information
                Domain.RetrieveVODTeamCompetition response = await _proxy.RetrieveTeamGameFromTheOnDemandCatalog(teamCompetitionId, querystring, bt);

                if (response != null)
                {
                    TeamCompetition competition = _mapper.Map<TeamCompetition>(response);
                    serviceResponse = _responseFactory.CreateServiceResponse(competition);
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<TeamCompetition>(null, proxyExeption);
            }
        }

        public async Task<ServiceResponse<Episode>> GetVodShowEpisode(string episodeId, GetScheduledContentParameters parameters, BusinessTransaction bt)
        {
            ServiceResponse<Episode> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(parameters);
                //Adding the parameters to get the asset information in the response, specially the LIVE or VOD Url
                querystring.Add("include", "assets");
                //Gets the episode information
                Domain.RetrieveVODShowEpisode response = await _proxy.RetrieveEpisodeFromTheOnDemandCatalog(episodeId, querystring, bt);

                if (response != null)
                {
                    Episode episode = _mapper.Map<Episode>(response);
                    serviceResponse = _responseFactory.CreateServiceResponse(episode);
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<Episode>(null, proxyExeption);
            }
        }
        #endregion "IVodService IMember Actions"
    }
}
