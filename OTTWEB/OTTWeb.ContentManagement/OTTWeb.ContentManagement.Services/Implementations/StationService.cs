﻿using AppDynamics;
using AutoMapper;
using Microsoft.Extensions.Options;
using OTTWeb.Common.Services.Infrastructure;
using OTTWeb.Common.Services.Models;
using OTTWeb.ContentManagement.OCM.Interfaces;
using OTTWeb.ContentManagement.Services.Configuration;
using OTTWeb.ContentManagement.Services.Interfaces;
using OTTWeb.ContentManagement.Services.Models;
using OTTWEB.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Domain = OTTWeb.ContentManagement.OCM.Models;

namespace OTTWeb.ContentManagement.Services.Implementations
{
    public class StationService : OTTBaseService, IStationService
    {
        #region Injected Services
        public AppSetting _appSettings;
        private IMapper _mapper;
        private IOCMProxy _proxy;
        private IServiceResponseFactory _responseFactory;
        #endregion

        #region Constructors
        public StationService(IOptions<AppSetting> settings, IMapper mapper, IOCMProxy proxy, IServiceResponseFactory responseFactory)
        {
            _appSettings = settings.Value;
            _mapper = mapper;
            _proxy = proxy;
            _responseFactory = responseFactory;
        }
        #endregion

        /// <summary>
        /// Gets the Station List
        /// </summary>
        /// <param name="stationParameters">The parameters for filtering the query</param>
        /// <returns>A ServiceResponse Object with the Stations</returns>
        public async Task<ServiceResponse<StationsList>> GetStations(GetStationsParameters stationParameters, BusinessTransaction bt)
        {
            ServiceResponse<StationsList> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(stationParameters);
                querystring.Add("include", "entitlements");

                var response = await _proxy.ReturnsAListOfStations(querystring, bt);
                if (response?.Stations != null && response.Stations.Count > 0)
                {
                    SetEntitledPropertyOfStations(response);
                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<StationsList>(response.Stations));
                }
                else
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(new StationsList());
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<StationsList>(null, proxyExeption);
            }
        }

        public async Task<ServiceResponse<Station>> GetStation(string stationId, GetStationParameters getStationParameters, BusinessTransaction bt)
        {
            ServiceResponse<Station> serviceResponse = null;
            try
            {
                //Creates querystring as a dictionary<string,string> for sending this one to Orbis
                Dictionary<string, string> querystring = CreateQueryStringForProxy(getStationParameters);

                Domain.Station response = await _proxy.ReturnsInformationAboutOneStation(stationId, querystring, bt);
                if (response != null)
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(_mapper.Map<Station>(response));
                }
                else
                {
                    serviceResponse = _responseFactory.CreateServiceResponse(new Station());
                }
                return serviceResponse;
            }
            catch (Exception proxyExeption)
            {
                return _responseFactory.CreateServiceResponse<Station>(null, proxyExeption);
            }
        }

        #region "Private Methods"
        private void SetEntitledPropertyOfStations(Domain.ReturnsAListOfStationsResponse response)
        {
            response?.Stations.ForEach(s => s.Entitled = response.Entitled?.Any(id => id == s.Id) ?? false);
        }
        #endregion "Private Methods"
    }
}
