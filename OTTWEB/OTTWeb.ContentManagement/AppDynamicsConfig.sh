#!/bin/bash
curl http://169.254.170.2/v2/metadata > metadata.info
TASKID=$(cut -d "\"" -f 8 metadata.info | cut -d "/" -f 2)
REV=$(cut -d "\"" -f 16 metadata.info)
echo $REV
sed -i "s/NNAME/R$REV::$TASKID/g" /app/OTTWeb.ContentManagement.API.AppDynamicsConfig.json
dotnet OTTWeb.ContentManagement.API.dll
