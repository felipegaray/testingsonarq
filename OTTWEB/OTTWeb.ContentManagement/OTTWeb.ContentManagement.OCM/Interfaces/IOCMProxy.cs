﻿using AppDynamics;
using OTTWeb.ContentManagement.OCM.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.OCM.Interfaces
{
    public interface IOCMProxy
    {
        /// <summary>
        /// Used for retrieving an asset by the ID (primary key)
        /// </summary>
        /// <remarks>
        /// OCM V2 Orbis Endpoint: Returns information about one asset
        /// </remarks>
        /// <param name="assetID">The Asset Id</param>
        /// <returns>An Asset Object</returns>
        Task<SearchAsset> SearchAssetById(string assetID, BusinessTransaction bt);
        
        /// <summary>
        /// This request is used to search and return a list of matching content using the filters
        /// language and query (keyword) through querystring
        /// </summary>
        /// <remarks>
        /// Orbis Endpoint: Return a list of matching content
        /// </remarks>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <returns>A list of Assets</returns>
        Task<SearchAssetsByKeywordResponse> SearchAssetsByKeyword(Dictionary<string, string> queryString, BusinessTransaction bt);

        /// <summary>
        /// Gets the data for the Page(Lander) defined in the pageID, it is organized in sections, subsections and the list of Assets
        /// </summary>
        /// <param name="pageId">The ID of the page to search</param>
        /// <returns></returns>
        Task<GetPageResponse> GetPage(string pageId, BusinessTransaction bt);

        /// <summary>
        /// Gets the data for a specific section of a Page(Lander) defined in the pageID,
        /// it is organized in sections, subsections and the list of Assets
        /// </summary>
        /// <param name="pageId">The ID of the page to search</param>
        /// <param name="sectionId">The ID of the section inside the page to search</param>
        /// <returns></returns>
        Task<GetPageSectionResponse> GetPageSection(string pageId, string sectionId, BusinessTransaction bt);

        /// <summary>
        /// This request is used to get a collection of schedules filtered by the querystring parameters
        /// <remarks>
        /// Orbis Endpoint: Returns a list of scheduled content
        /// </remarks>
        /// </summary>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <returns>A list of Schedules</returns>
        Task<List<Schedule>> ReturnsAListOfScheduledContent(string querystring, BusinessTransaction bt);

        /// <summary>
        /// This request is used to get a collection of stations filtered by the querystring parameters
        /// </summary>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <returns>A list of Stations</returns>
        Task<ReturnsAListOfStationsResponse> ReturnsAListOfStations(Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// This request is used to get a station
        /// </summary>
        /// <remarks> Orbis Endpoint: Returns information about one station </remarks>
        /// <param name="stationId">The Movie Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <returns>A Station Data</returns>
        Task<Station> ReturnsInformationAboutOneStation(string stationId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Returns a Movie data by the ScheduledMovieId provided
        ///  <remarks>Orbis Endpoint: Retrieve scheduled movie from the EPG</remarks>
        /// </summary>
        /// <param name="scheduledMovieId">The Movie Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Movie Object</returns>
        Task<RetrieveEPGMovie> RetrieveScheduledMovieFromTheEPG(string scheduledMovieId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Returns a Event/Episode data by the ScheduledEpisodeId provided
        /// <remarks>
        /// Orbis Endpoint: Retrieve scheduled show episode from the EPG
        /// </remarks>
        /// </summary>
        /// <param name="scheduledEpisodeId">The Episode Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response Event Object</returns>
        Task<RetrieveEPGShowEpisode> RetrieveScheduledShowEpisodeFromTheEPG(string scheduledEpisodeId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Returns a Event data by the ScheduledEventId provided
        /// <remarks>
        /// Orbis Endpoint: Retrieve scheduled event from the EPG
        /// </remarks>
        /// </summary>
        /// <param name="scheduledEventId">The Event Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></excepti
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response Event Object</returns>
        Task<RetrieveEpgEvent> RetrieveScheduledEventFromTheEPG(string scheduledEventId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Returns a Team Competitions data by the ScheduledTeamCompetitionId provided
        /// <remarks>
        /// Orbis Endpoint: Retrieve scheduled team game from the EPG
        /// </remarks>
        /// </summary>
        /// <param name="scheduledTeamCompetitionId">The Team Competitions Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response Team Competition Object</returns>
        Task<RetrieveEpgTeamCompetition> RetrieveScheduledTeamGameFromTheEPG(string scheduledTeamCompetitionId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Returns a Competition data by the ScheduledCompetitionId provided
        /// <remarks>
        /// Orbis Endpoint: Retrieve scheduled game from the EPG
        /// </remarks>
        /// </summary>
        /// <param name="scheduledCompetitionId">The Competitions Id to be searched</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response Competition Object</returns>
        Task<RetrieveEpgCompetition> RetrieveScheduledGameFromTheEPG(string scheduledCompetitionId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Return the metadata associated with the specified movie from the on-demand catalog.
        ///  <remarks>
        /// Orbis Endpoint: Retrieve movie from the on-demand catalog
        /// </remarks>
        /// </summary>
        /// <param name="vodMovieId">The Movie Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response VOD Movie Object</returns>
        Task<RetrieveVODMovie> RetrieveMovieFromTheOnDemandCatalog(string vodMovieId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Returns a show and its seasons
        /// <remarks>
        /// Orbis Endpoint: Get a single show
        /// </remarks> 
        /// </summary>
        /// <param name="showId">The id of the show to be searched</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>a show response object</returns>
        Task<GetShowResponse> GetShowAndSeasons(string showId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Returns a season detail and its episodes
        /// <remarks>
        /// Orbis Endpoint: Get a single season
        /// </remarks> 
        /// </summary>
        /// <param name="seasonId">The id of the season to be searched</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>a season response object</returns>
        Task<GetSeasonResponse> GetSeason(string seasonId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Return the metadata associated with the specified competition from the on-demand catalog.
        /// </summary>
        /// <param name="competitionId">The Competition Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response VOD Competition Object</returns>
        Task<RetrieveVODCompetition> RetrieveGameFromTheOnDemandCatalog(string competitionId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Return the metadata associated with the specified team competition from the on-demand catalog.
        /// </summary>
        /// <param name="teamCompetitionId">The Team Competition Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response VOD Team Competition Object</returns>
        Task<RetrieveVODTeamCompetition> RetrieveTeamGameFromTheOnDemandCatalog(string teamCompetitionId, Dictionary<string, string> querystring, BusinessTransaction bt);

        /// <summary>
        /// Return the metadata associated with the specified show episode from the on-demand catalog.
        /// <remarks>
        /// Orbis Endpoint: Retrieve show episode from the on-demand catalog
        /// </remarks>
        /// </summary>
        /// <param name="episodeId">The Episode Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="OCMServiceException"></exception>
        /// <exception cref="OCMBusinessException"></exception>
        /// <exception cref="OCMUnhandledException"></exception>
        /// <returns>A Response VOD Show Episode Object</returns>
        Task<RetrieveVODShowEpisode> RetrieveEpisodeFromTheOnDemandCatalog(string episodeId, Dictionary<string, string> querystring, BusinessTransaction bt);
    }
}
