﻿namespace OTTWeb.ContentManagement.OCM.Configuration
{
    public class AppSetting
    {
        public string OCMServiceUrl { get; set; }
        public string DirecTvOttCMApiUri { get; set; }
    }
}
