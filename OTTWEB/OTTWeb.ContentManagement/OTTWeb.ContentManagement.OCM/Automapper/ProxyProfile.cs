﻿using AutoMapper;
using OTTWeb.ContentManagement.OCM.Models;

namespace OTTWeb.ContentManagement.OCM.Automapper
{
    /// <summary>
    /// Contains all mapping configurations needed at Proxy level
    /// </summary>
    class ProxyProfile : Profile
    {
        /// <summary>
        /// Contains all mapping configurations needed at Proxy level
        /// </summary>
        public ProxyProfile()
        {
            CreateMap<GetEpgMovieResponse, RetrieveEPGMovie>();
            CreateMap<GetEpgEpisodeResponse, RetrieveEPGShowEpisode>();
            CreateMap<GetEpgEventResponse, RetrieveEpgEvent>();
            CreateMap<GetVodMovieResponse, RetrieveVODMovie>();
			CreateMap<GetEpgCompetitionResponse, RetrieveEpgCompetition>();
            CreateMap<GetEpgTeamCompetitionResponse, RetrieveEpgTeamCompetition>();
            CreateMap<GetVodCompetitionResponse, RetrieveVODCompetition>();
            CreateMap<GetVodTeamCompetitionResponse, RetrieveVODTeamCompetition>();
            CreateMap<GetVodShowEpisodeResponse, RetrieveVODShowEpisode>();
        }
    }
}
