﻿using Newtonsoft.Json;
using OTTWeb.ContentManagement.OCM.Models.OCM;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetShowResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "shows")]
        public List<Show> Show { get; set; }
        [JsonProperty(PropertyName = "seasons")]
        public List<Season> Seasons { get; set; }
    }
}
