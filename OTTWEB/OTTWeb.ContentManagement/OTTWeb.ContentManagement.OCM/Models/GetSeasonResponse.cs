﻿using Newtonsoft.Json;
using OTTWeb.ContentManagement.OCM.Models.OCM;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetSeasonResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "seasons")]
        public List<Season> Seasons { get; set; }
        [JsonProperty(PropertyName = "vod/episodes")]
        public List<SeasonEpisode> Episodes { get; set; }
    }
}
