﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetVodTeamCompetitionResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "vod/teamCompetitions")]
        public List<VODTeamCompetition> VodTeamCompetitions { get; set; }
    }
}
