﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class RetrieveVODCompetition
    {
        public List<VODCompetition> VodCompetitions { get; set; }
    }
}
