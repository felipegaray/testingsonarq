﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetEpgEventResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "epg/events")]
        public List<EPGEvent> EpgEvents { get; set; }
    }
}
