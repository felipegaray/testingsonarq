﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class RetrieveVODTeamCompetition
    {        
        public List<VODTeamCompetition> VodTeamCompetitions { get; set; }
    }
}
