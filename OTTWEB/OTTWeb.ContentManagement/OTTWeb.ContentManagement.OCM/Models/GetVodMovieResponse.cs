﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetVodMovieResponse : OCMResponse
    {
        [JsonProperty(PropertyName ="assets")]
        public List<OCMAsset> Assets { get; set; }        

        [JsonProperty(PropertyName = "vod/movies")]
        public List<VODMovie> VodMovies { get; set; }
    }
}