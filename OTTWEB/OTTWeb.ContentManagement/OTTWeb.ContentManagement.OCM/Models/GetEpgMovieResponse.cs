﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetEpgMovieResponse : OCMResponse
    {        
        [JsonProperty(PropertyName = "epg/movies")]
        public List<EPGMovie> EpgMovies { get; set; }
    }
}
