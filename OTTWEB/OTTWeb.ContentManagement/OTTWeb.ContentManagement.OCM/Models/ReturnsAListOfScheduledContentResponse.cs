﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class ReturnsAListOfScheduledContentResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "schedule")]
        public List<Schedule> Schedule { get; set; }
    }
}
