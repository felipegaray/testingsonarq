﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class ReturnsAListOfStationsResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "entitled")]
        public List<string> Entitled { get; set; }

        [JsonProperty(PropertyName = "epg/stations")]
        public List<Station> Stations{ get; set; }
    }
}
