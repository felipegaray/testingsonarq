﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class SearchAssetsByKeywordResponse: OCMResponse
    {
        [JsonProperty(PropertyName = "results")]
        public List<SearchAsset> Results { get; set; }
        [JsonProperty(PropertyName = "entitled")]
        public List<string> Entitled { get; set; }
    }
}
