﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class ReturnsInformationAboutOneStationResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "epg/stations")]
        public List<Station> Stations { get; set; }
    }
}
