﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class SeasonEpisode : OCMContent
    {
        [JsonProperty(PropertyName = "showName")]
        public string ShowName { get; set; }
        [JsonProperty(PropertyName = "regionIDs")]
        public string[] RegionIDs { get; set; }
        [JsonProperty(PropertyName = "season")]
        public short Season { get; set; }
        [JsonProperty(PropertyName = "episode")]
        public short Episode { get; set; }      
    }
}
