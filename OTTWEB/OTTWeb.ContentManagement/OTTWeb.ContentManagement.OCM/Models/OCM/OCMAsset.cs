﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class OCMAsset
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "resourceType")]
        public string ResourceType { get; set; }
        [JsonProperty(PropertyName = "publishStart")]
        public string PublishStart { get; set; }
        [JsonProperty(PropertyName = "publishEnd")]
        public string PublishEnd { get; set; }
        [JsonProperty(PropertyName = "published")]
        public bool Published { get; set; }
        [JsonProperty(PropertyName = "pictureID")]
        public string PictureID { get; set; }
        [JsonProperty(PropertyName = "duration")]
        public int Duration { get; set; }
        [JsonProperty(PropertyName = "live")]
        public bool Live { get; set; }
        [JsonProperty(PropertyName = "environment")]
        public string Environment { get; set; }
        [JsonProperty(PropertyName = "liveURLs")]
        public Dictionary<string, Dictionary<string, string>> LiveURLs { get; set; }
        [JsonProperty(PropertyName = "vodURLs")]
        public Dictionary<string, Dictionary<string, string>> VodURLs { get; set; }
        [JsonProperty(PropertyName = "drmID")]
        public string DrmID { get; set; }
        [JsonProperty(PropertyName = "availabilityStartsAt")]
        public string AvailabilityStartsAt { get; set; }
        [JsonProperty(PropertyName = "availabilityEndsAt")]
        public string AvailabilityEndsAt { get; set; }
        [JsonProperty(PropertyName = "contentProvider")]
        public Dictionary<string, string> ContentProvider { get; set; }
        [JsonProperty(PropertyName = "regionIDs")]
        public string[] RegionIDs { get; set; }
    }
}
