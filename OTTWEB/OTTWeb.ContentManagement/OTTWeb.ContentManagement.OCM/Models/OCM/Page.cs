﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models.OCM
{
    public class Page
    {
        /// <summary>
        /// The title of the page
        /// </summary>
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }
        /// <summary>
        /// The list of sections in the page
        /// </summary>
        [JsonProperty(PropertyName = "sections")]
        public List<Section> Sections { get; set; }
    }
}
