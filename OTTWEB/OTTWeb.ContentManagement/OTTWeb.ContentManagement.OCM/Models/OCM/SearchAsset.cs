﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class SearchAsset
    {        
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "resourceType")]
        public string ResourceType { get; set; }
        [JsonProperty(PropertyName = "startTime")]
        public string StartTime { get; set; }
        [JsonProperty(PropertyName = "endTime")]
        public string EndTime { get; set; }
        [JsonProperty(PropertyName = "duration")]
        public int Duration { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "shortDescription")]
        public string ShortDescription { get; set; }
        [JsonProperty(PropertyName = "pictureID")]
        public string PictureID { get; set; }
        [JsonProperty(PropertyName = "pictures")]
        public Dictionary<string,string> Pictures { get; set; }
        [JsonProperty(PropertyName = "liveURLs")]
        public Dictionary<string, Dictionary<string, string>> LiveURLs { get; set; }
        [JsonProperty(PropertyName = "vodURLs")]
        public Dictionary<string, Dictionary<string, string>> VodURLs { get; set; }
        [JsonProperty(PropertyName = "showName")]
        public string ShowName { get; set; }
        [JsonProperty(PropertyName = "showID")]
        public string ShowID { get; set; }
        [JsonProperty(PropertyName = "seasonID")]
        public string SeasonID { get; set; }
        [JsonProperty(PropertyName = "genres")]
        public List<Genre> Genres { get; set; }
        [JsonProperty(PropertyName = "genreIDs")]
        public string[] GenreIDs { get; set; }
        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; }
        [JsonProperty(PropertyName = "ratings")]
        public string[] Ratings { get; set; }
        [JsonProperty(PropertyName = "ratingAdvisories")]
        public string[] RatingAdvisories { get; set; }
        [JsonProperty(PropertyName = "regionIDs")]
        public string[] RegionIDs { get; set; }
        [JsonProperty(PropertyName = "season")]
        public short Season { get; set; }
        [JsonProperty(PropertyName = "seasons")]
        public short? Seasons { get; set; }
        [JsonProperty(PropertyName = "episode")]
        public int Episode { get; set; }
        [JsonProperty(PropertyName = "releaseYear")]
        public short ReleaseYear { get; set; }
        [JsonProperty(PropertyName = "assetIDs")]
        public string[] AssetIDs { get; set; }
        [JsonProperty(PropertyName = "sportID")]
        public string SportID { get; set; }
        [JsonProperty(PropertyName = "sportName")]
        public string SportName { get; set; }
        [JsonProperty(PropertyName = "leagueID")]
        public string LeagueID { get; set; }
        [JsonProperty(PropertyName = "leagueName")]
        public string LeagueName { get; set; }
        [JsonProperty(PropertyName = "tournamentID")]
        public string TournamentID { get; set; }
        [JsonProperty(PropertyName = "tournamentName")]
        public string TournamentName { get; set; }
        [JsonProperty(PropertyName = "primaryPersonID")]
        public string PrimaryPersonID { get; set; }
        [JsonProperty(PropertyName = "primaryPersonName")]
        public string PrimaryPersonName { get; set; }
        [JsonProperty(PropertyName = "primaryPersonPictureID")]
        public string PrimaryPersonPictureID { get; set; }
        [JsonProperty(PropertyName = "secondaryPersonID")]
        public string SecondaryPersonID { get; set; }
        [JsonProperty(PropertyName = "secondaryPersonName")]
        public string SecondaryPersonName { get; set; }
        [JsonProperty(PropertyName = "secondaryPersonPictureID")]
        public string SecondaryPersonPictureID { get; set; }
        [JsonProperty(PropertyName = "primaryTeamID")]
        public string PrimaryTeamID { get; set; }
        [JsonProperty(PropertyName = "primaryTeamName")]
        public string PrimaryTeamName { get; set; }
        [JsonProperty(PropertyName = "primaryTeamPictureID")]
        public string PrimaryTeamPictureID { get; set; }
        [JsonProperty(PropertyName = "secondaryTeamID")]
        public string SecondaryTeamID { get; set; }
        [JsonProperty(PropertyName = "secondaryTeamName")]
        public string SecondaryTeamName { get; set; }
        [JsonProperty(PropertyName = "secondaryTeamPictureID")]
        public string SecondaryTeamPictureID { get; set; }
        [JsonProperty(PropertyName = "position")]
        public short Position { get; set; }
        [JsonProperty(PropertyName = "stationNumber")]
        public string StationNumber { get; set; }
        [JsonProperty(PropertyName = "stationName")]
        public string StationName { get; set; }
        [JsonProperty(PropertyName = "stationID")]
        public string StationID { get; set; }
        [JsonProperty(PropertyName = "provider")]
        public string Provider { get; set; }
        [JsonProperty(PropertyName = "providerID")]
        public string ProviderID { get; set; }
        [JsonProperty(PropertyName = "callSign")]
        public string CallSign { get; set; }      
        [JsonProperty(PropertyName = "blackout")]
        public bool Blackout { get; set; }
        [JsonProperty(PropertyName = "blackoutRegionIDs")]
        public string[] BlackoutRegionIDs { get; set; }
        [JsonProperty(PropertyName = "entitled")]
        public bool Entitled { get; set; }
        [JsonProperty(PropertyName = "publishStart")]
        public string PublishStart { get; set; }
        [JsonProperty(PropertyName = "publishEnd")]
        public string PublishEnd { get; set; }
        [JsonProperty(PropertyName = "published")]
        public bool Published { get; set; }
        [JsonProperty(PropertyName = "live")]
        public bool Live { get; set; }
        [JsonProperty(PropertyName = "environment")]
        public string Environment { get; set; }
        [JsonProperty(PropertyName = "drmID")]
        public string DrmID { get; set; }
        [JsonProperty(PropertyName = "availabilityStartsAt")]
        public string AvailabilityStartsAt { get; set; }
        [JsonProperty(PropertyName = "availabilityEndsAt")]
        public string AvailabilityEndsAt { get; set; }
        [JsonProperty(PropertyName = "contentProvider")]
        public Dictionary<string, string> ContentProvider { get; set; }
        [JsonProperty(PropertyName = "contentLock")]
        public bool ContentLock { get; set; }
        [JsonProperty(PropertyName = "countryOfOrigin")]
        public string[] CountryOfOrigin { get; set; }
        [JsonProperty(PropertyName = "metadata")]
        public AssetMetadata Metadata { get; set; }      
    }
}
