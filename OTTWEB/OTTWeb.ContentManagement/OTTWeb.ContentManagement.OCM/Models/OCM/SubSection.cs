﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models.OCM
{
    public class SubSection
    {
        /// <summary>
        /// The block ID
        /// </summary>
        [JsonProperty(PropertyName = "blockId")]
        public string BlockId { get; set; }
        /// <summary>
        /// The block Label (name) of a subsection
        /// </summary>
        [JsonProperty(PropertyName = "blockLabel")]
        public string BlockLabel { get; set; }
        /// <summary>
        /// The block Label (name) of a subsection
        /// </summary>
        [JsonProperty(PropertyName = "blockType")]
        public string BlockType { get; set; } 
        /// <summary>
        /// The List of Assets
        /// </summary>
        [JsonProperty(PropertyName = "items")]
        public List<SectionAsset> Items { get; set; }
        /// <summary>
        /// The ID's of the entitled items
        /// </summary>
        [JsonProperty(PropertyName = "entitled")]
        public List<string> Entitled { get; set; }
    }
}
