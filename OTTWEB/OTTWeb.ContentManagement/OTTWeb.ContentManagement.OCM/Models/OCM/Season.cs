﻿using Newtonsoft.Json;
using System;

namespace OTTWeb.ContentManagement.OCM.Models.OCM
{
    public class Season
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "resourceType")]
        public string ResourceType { get; set; }
        [JsonProperty(PropertyName = "publishStart")]
        public string PublishStart { get; set; }
        [JsonProperty(PropertyName = "publishEnd")]
        public string PublishEnd { get; set; }
        [JsonProperty(PropertyName = "published")]
        public bool Published { get; set; }
        [JsonProperty(PropertyName = "startTime")]
        public string StartTime { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }
        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; }
        [JsonProperty(PropertyName = "contentLock")]
        public bool ContentLock { get; set; }
        [JsonProperty(PropertyName = "regionIDs")]
        public string[] RegionIDs { get; set; }
        [JsonProperty(PropertyName = "seasonNumber")]
        public short SeasonNumber { get; set; }
        [JsonProperty(PropertyName = "episodeCount")]
        public short EpisodeCount { get; set; }
        [JsonProperty(PropertyName = "showID")]
        public string ShowID { get; set; }
        [JsonProperty(PropertyName = "vod/episodeIDs")]
        public string[] EpisodeIDs { get; set; }

    }
}
