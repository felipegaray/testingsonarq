﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class Show : OCMContent
    {
        [JsonProperty(PropertyName = "seasons")]
        public int Seasons { get; set; }
        [JsonProperty(PropertyName = "seasonIDs")]
        public string[] SeasonIDs { get; set; }
        [JsonProperty(PropertyName = "contentLock")]
        public bool ContentLock { get; set; }
        [JsonProperty(PropertyName = "countryOfOrigin")]
        public string[] CountryOfOrigin { get; set; }
    }
}
