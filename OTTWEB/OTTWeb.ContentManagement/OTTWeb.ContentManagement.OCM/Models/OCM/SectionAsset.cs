﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models.OCM
{
    public class SectionAsset : OCMContent
    {       
  
        /// <summary>
        /// The name of the show in case its a show episode
        /// </summary>
        [JsonProperty(PropertyName = "showName")]
        public string ShowName { get; set; }
         
        /// <summary>
        /// The region IDs
        /// </summary>
        [JsonProperty(PropertyName = "regionIDs")]
        public string[] RegionIDs { get; set; }
        /// <summary>
        /// The number of the season
        /// </summary>
        [JsonProperty(PropertyName = "season")]
        public short Season { get; set; }
        /// <summary>
        /// The total number of seasons
        /// </summary>
        [JsonProperty(PropertyName = "seasons")]
        public short? Seasons { get; set; }
        /// <summary>
        /// The number of episode
        /// </summary>
        [JsonProperty(PropertyName = "episode")]
        public int Episode { get; set; }
        /// <summary>
        /// The release Year
        /// </summary>
        [JsonProperty(PropertyName = "releaseYear")]
        public short ReleaseYear { get; set; }             
        /// <summary>
        /// The sport name
        /// </summary>
        [JsonProperty(PropertyName = "sportName")]
        public string SportName { get; set; }        
        /// <summary>
        /// The tournament Name
        /// </summary>
        [JsonProperty(PropertyName = "tournamentName")]
        public string TournamentName { get; set; }        
        /// <summary>
        /// The primary person Name
        /// </summary>
        [JsonProperty(PropertyName = "primaryPersonName")]
        public string PrimaryPersonName { get; set; }
        /// <summary>
        /// The primery person picture ID
        /// </summary>
        [JsonProperty(PropertyName = "primaryPersonPictureID")]
        public string PrimaryPersonPictureID { get; set; }        
        /// <summary>
        /// The secondary person Name
        /// </summary>
        [JsonProperty(PropertyName = "secondaryPersonName")]
        public string SecondaryPersonName { get; set; }
        /// <summary>
        /// The secondary person picture ID
        /// </summary>
        [JsonProperty(PropertyName = "secondaryPersonPictureID")]
        public string SecondaryPersonPictureID { get; set; }
        /// <summary>
        /// The position of the asset in a subsection
        /// </summary>
        [JsonProperty(PropertyName = "position")]
        public short Position { get; set; }
        /// <summary>
        /// The station Number
        /// </summary>
        [JsonProperty(PropertyName = "stationNumber")]
        public string StationNumber { get; set; }
        /// <summary>
        /// The station Name
        /// </summary>
        [JsonProperty(PropertyName = "stationName")]
        public string StationName { get; set; }
        /// <summary>
        /// The station ID
        /// </summary>
        [JsonProperty(PropertyName = "stationID")]
        public string StationID { get; set; }
        /// <summary>
        /// The provider of the station
        /// </summary>
        [JsonProperty(PropertyName = "provider")]
        public string Provider { get; set; }
        /// <summary>
        /// The provider Id of the station
        /// </summary>
        [JsonProperty(PropertyName = "providerID")]
        public string ProviderID { get; set; }
        /// <summary>
        /// The call sign of the station
        /// </summary>
        [JsonProperty(PropertyName = "callSign")]
        public string CallSign { get; set; }
        /// <summary>
        /// The blackout station
        /// </summary>
        [JsonProperty(PropertyName = "blackout")]
        public bool Blackout { get; set; }
        /// <summary>
        /// The entitled
        /// </summary>
        [JsonProperty(PropertyName = "entitled")]
        public bool Entitled { get; set; }
        /// <summary>
        /// The contentlock
        /// </summary>
        [JsonProperty(PropertyName = "contentLock")]
        public bool ContentLock { get; set; }
        /// <summary>
        /// the country of origin
        /// </summary>
        [JsonProperty(PropertyName = "countryOfOrigin")]
        public string[] CountryOfOrigin { get; set; }   
    }
}
