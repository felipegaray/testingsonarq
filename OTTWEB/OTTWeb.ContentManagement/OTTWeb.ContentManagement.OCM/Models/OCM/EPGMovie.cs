﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class EPGMovie : EPGContent
    {
        [JsonProperty(PropertyName = "releaseYear")]
        public int RealeaseYear { get; set; }
    }
}
