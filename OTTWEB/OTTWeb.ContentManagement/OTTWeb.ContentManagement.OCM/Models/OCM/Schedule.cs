﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class Schedule
    {
        /// <summary>
        /// The Schedule ID
        /// </summary>
        [JsonProperty(PropertyName ="id")]
        public string Id { get; set; }
        /// <summary>
        /// The resource type
        /// </summary>
        [JsonProperty(PropertyName = "resourceType")]
        public string ResourceType { get; set; }
        /// <summary>
        /// The schedule start time
        /// </summary>
        [JsonProperty(PropertyName = "startTime")]
        public DateTime StartTime { get; set; }
        /// <summary>
        /// The schedule end time
        /// </summary>
        [JsonProperty(PropertyName = "endTime")]
        public DateTime EndTime { get; set; }
        /// <summary>
        /// The schedule duration
        /// </summary>
        [JsonProperty(PropertyName = "duration")]
        public string Duration { get; set; }
        /// <summary>
        /// The name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        /// <summary>
        /// The short name
        /// </summary>
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }
        /// <summary>
        /// The description
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        /// <summary>
        /// The short description
        /// </summary>
        [JsonProperty(PropertyName = "shortDescription")]
        public string ShortDescription { get; set; }
        /// <summary>
        /// The picture ID
        /// </summary>
        [JsonProperty(PropertyName = "pictureID")]
        public string PictureID { get; set; }
        /// <summary>
        /// The pictures
        /// </summary>
        [JsonProperty(PropertyName = "pictures")]
        public Dictionary<string, string> Pictures { get; set; }
        /// <summary>
        /// The ratings
        /// </summary>
        [JsonProperty(PropertyName = "ratings")]
        public string[] Ratings { get; set; }
        /// <summary>
        /// The rating advisories
        /// </summary>
        [JsonProperty(PropertyName = "ratingAdvisories")]
        public string[] RatingAdvisories { get; set; }
        /// <summary>
        /// the station ID
        /// </summary>
        [JsonProperty(PropertyName = "stationID")]
        public string StationID { get; set; }
        /// <summary>
        /// The Genre IDs
        /// </summary>
        [JsonProperty(PropertyName = "genreIDs")]
        public string[] GenreIDs { get; set; }
        /// <summary>
        /// The Genre Collection
        /// </summary>
        [JsonProperty(PropertyName = "genres")]
        public List<Genre> Genres { get; set; }
        /// <summary>
        /// The Asset Id
        /// </summary>
        [JsonProperty(PropertyName = "assetIDs")]
        public string[] AssetIDs { get; set; }
        /// <summary>
        /// The Blackout
        /// </summary>
        [JsonProperty(PropertyName = "blackout")]
        public bool Blackout { get; set; }
        /// <summary>
        /// The Season Number
        /// </summary>
        [JsonProperty(PropertyName = "season")]
        public string Season { get; set; }
        /// <summary>
        /// The Episode Number
        /// </summary>
        [JsonProperty(PropertyName = "episode")]
        public string Episode { get; set; }
        /// <summary>
        /// The show name
        /// </summary>
        [JsonProperty(PropertyName = "showName")]
        public string ShowName { get; set; }    
        /// <summary>
        /// The publish start
        /// </summary>
        [JsonProperty(PropertyName = "publishStart")]
        public string PublishStart { get; set; }
        /// <summary>
        /// The publish end
        /// </summary>
        [JsonProperty(PropertyName = "publishEnd")]
        public string PublishEnd { get; set; }
        /// <summary>
        /// The published
        /// </summary>
        [JsonProperty(PropertyName = "published")]
        public bool Published { get; set; }
        /// <summary>
        /// The language
        /// </summary>
        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; }
        /// <summary>
        /// The regionIDs
        /// </summary>
        [JsonProperty(PropertyName = "regionIDs")]
        public string[] RegionIDs { get; set; }
        /// <summary>
        /// The blackout regions
        /// </summary>
        [JsonProperty(PropertyName = "blackoutRegionIDs")]
        public string[] BlackoutRegionIDs { get; set; }
        /// <summary>
        /// The asset metadata
        /// </summary>
        [JsonProperty(PropertyName = "metadata")]
        public AssetMetadata Metadata { get; set; }
        /// <summary>
        /// The release yer for Movie
        /// </summary>
        [JsonProperty(PropertyName = "releaseYear")]
        public short ReleaseYear { get; set; }
        

    }
}
