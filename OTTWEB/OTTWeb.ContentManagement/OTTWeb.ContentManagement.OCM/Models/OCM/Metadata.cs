﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class Metadata
    {
        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }
        [JsonProperty(PropertyName = "totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty(PropertyName = "offset")]
        public int Offset { get; set; }
        [JsonProperty(PropertyName = "ids")]
        public List<string> Ids { get; set; }
    }
}
