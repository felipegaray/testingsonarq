﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class OCMContent
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "resourceType")]
        public string ResourceType { get; set; }
        [JsonProperty(PropertyName = "startTime")]
        public string StartTime { get; set; }
        [JsonProperty(PropertyName = "endTime")]
        public string EndTime { get; set; }
        [JsonProperty(PropertyName = "duration")]
        public Int32 Duration { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "shortDescription")]
        public string ShortDescription { get; set; }
        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; }
        [JsonProperty(PropertyName = "pictureID")]
        public string PictureID { get; set; }
        [JsonProperty(PropertyName = "pictures")]
        public Dictionary<string, string> Pictures { get; set; }
        [JsonProperty(PropertyName = "ratings")]
        public string[] Ratings { get; set; }
        [JsonProperty(PropertyName = "ratingAdvisories")]
        public string[] RatingAdvisories { get; set; }
        [JsonProperty(PropertyName = "genreIDs")]
        public string[] GenreIDs { get; set; }
        [JsonProperty(PropertyName = "genres")]
        public List<Genre> Genres { get; set; }
        [JsonProperty(PropertyName = "assetIDs")]
        public string[] AssetIDs { get; set; }
        [JsonProperty(PropertyName = "regionIDs")]
        public string[] RegionIDs { get; set; }        
        [JsonProperty(PropertyName = "publishStart")]
        public string PublishStart { get; set; }
        [JsonProperty(PropertyName = "publishEnd")]
        public string PublishEnd { get; set; }
        [JsonProperty(PropertyName = "published")]
        public bool Published { get; set; }
        [JsonProperty(PropertyName = "metadata")]
        public AssetMetadata Metadata { get; set; }
    }
}
