﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class Asset
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "keywords")]
        public string Keywords { get; set; }
        [JsonProperty(PropertyName = "categories")]
        public string Categories { get; set; }
        [JsonProperty(PropertyName = "dateBegin")]
        public DateTime? DateBegin { get; set; }
        [JsonProperty(PropertyName = "dateEnd")]
        public DateTime? DateEnd { get; set; }
        [JsonProperty(PropertyName = "eventID")]
        public string EventID { get; set; }
        [JsonProperty(PropertyName = "assetID")]
        public string AssetID { get; set; }
        [JsonProperty(PropertyName = "contentProvider")]
        public string ContentProvider { get; set; }
        [JsonProperty(PropertyName = "livePlaybackURLs")]
        public Dictionary<string,string> LivePlaybackURLs { get; set; }
        [JsonProperty(PropertyName = "vodPlaybackURLs")]
        public Dictionary<string, string> VodPlaybackURLs { get; set; }
        [JsonProperty(PropertyName = "imageURLs")]
        public Dictionary<string, string> ImageURLs { get; set; }
        [JsonProperty(PropertyName = "isLive")]
        public bool IsLive { get; set; }
        [JsonProperty(PropertyName = "adContentAssetID")]
        public string AdContentAssetID { get; set; }
        [JsonProperty(PropertyName = "adParentGroup")]
        public string AdParentGroup { get; set; }
        [JsonProperty(PropertyName = "subType")]
        public string SubType { get; set; }
        [JsonProperty(PropertyName = "contentOwner")]
        public string ContentOwner { get; set; }
        [JsonProperty(PropertyName = "livePubPoint")]
        public string LivePubPoint { get; set; }
        [JsonProperty(PropertyName = "live2VODPubPoint")]
        public string Live2VODPubPoint { get; set; }
        [JsonProperty(PropertyName = "createdAt")]
        public DateTime? CreatedAt { get; set; }
        [JsonProperty(PropertyName = "updatedAt")]
        public DateTime? UpdatedAt { get; set; }
    }
}
