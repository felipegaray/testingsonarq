﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class VODCompetition : VODContent
    {
        [JsonProperty(PropertyName = "sportID")]
        public string SportID { get; set; }
        [JsonProperty(PropertyName = "sportName")]
        public string SportName { get; set; }
        [JsonProperty(PropertyName = "tournamentID")]
        public string TournamentID { get; set; }
        [JsonProperty(PropertyName = "tournamentName")]
        public string TournamentName { get; set; }
        [JsonProperty(PropertyName = "primaryPersonID")]
        public string PrimaryPersonID { get; set; }
        [JsonProperty(PropertyName = "primaryPersonName")]
        public string PrimaryPersonName { get; set; }
        [JsonProperty(PropertyName = "primaryPersonPictureID")]
        public string PrimaryPersonPictureID { get; set; }
        [JsonProperty(PropertyName = "secondaryPersonID")]
        public string SecondaryPersonID { get; set; }
        [JsonProperty(PropertyName = "secondaryPersonName")]
        public string SecondaryPersonName { get; set; }
        [JsonProperty(PropertyName = "secondaryPersonPictureID")]
        public string SecondaryPersonPictureID { get; set; }
    }
}
