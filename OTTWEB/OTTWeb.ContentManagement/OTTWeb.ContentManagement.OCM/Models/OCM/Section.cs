﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models.OCM
{
    public class Section
    {
        /// <summary>
        /// The label
        /// </summary>
        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }
        /// <summary>
        /// The section ID
        /// </summary>
        [JsonProperty(PropertyName = "sectionID")]
        public string SectionID { get; set; }
        /// <summary>
        /// The lis of subsections in a section
        /// </summary>
        [JsonProperty(PropertyName = "subsections")]
        public List<SubSection> Subsections { get; set; }
    }
}
