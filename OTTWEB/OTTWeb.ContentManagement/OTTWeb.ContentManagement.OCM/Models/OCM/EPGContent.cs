﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class EPGContent : OCMContent
    {
        [JsonProperty(PropertyName = "stationID")]
        public string StationID { get; set; }
        [JsonProperty(PropertyName = "blackoutRegionIDs")]
        public string[] BlackoutRegionIDs { get; set; }
        [JsonProperty(PropertyName = "blackout")]
        public bool Blackout { get; set; }
    }
}
