﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class VODContent : OCMContent
    {
        [JsonProperty(PropertyName = "contentLock")]
        public bool ContentLock { get; set; }
        [JsonProperty(PropertyName = "countryOfOrigin")]
        public string[] CountryOfOrigin { get; set; }
    }
}
