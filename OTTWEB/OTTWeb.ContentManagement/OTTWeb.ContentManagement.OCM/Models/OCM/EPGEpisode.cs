﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class EPGEpisode : EPGContent
    {
        [JsonProperty(PropertyName = "showName")]
        public string ShowName { get; set; }
        [JsonProperty(PropertyName = "episode")]
        public string Episode { get; set; }
        [JsonProperty(PropertyName = "season")]
        public string Season { get; set; }
    }
}
