﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class EPGTeamCompetition : EPGContent
    {
        [JsonProperty(PropertyName = "sportID")]
        public string SportID { get; set; }
        [JsonProperty(PropertyName = "sportName")]
        public string SportName { get; set; }
        [JsonProperty(PropertyName = "tournamentID")]
        public string TournamentID { get; set; }
        [JsonProperty(PropertyName = "tournamentName")]
        public string TournamentName { get; set; }
        [JsonProperty(PropertyName = "primaryTeamID")]
        public string PrimaryTeamID { get; set; }
        [JsonProperty(PropertyName = "primaryTeamName")]
        public string PrimaryTeamName { get; set; }
        [JsonProperty(PropertyName = "primaryTeamPictureID")]
        public string PrimaryTeamPictureID { get; set; }
        [JsonProperty(PropertyName = "secondaryTeamID")]
        public string SecondaryTeamID { get; set; }
        [JsonProperty(PropertyName = "secondaryTeamName")]
        public string SecondaryTeamName { get; set; }
        [JsonProperty(PropertyName = "secondaryTeamPictureID")]
        public string SecondaryTeamPictureID { get; set; }
        [JsonProperty(PropertyName = "leagueID")]
        public string LeagueID { get; set; }
        [JsonProperty(PropertyName = "leagueName")]
        public string LeagueName { get; set; }
    }
}
