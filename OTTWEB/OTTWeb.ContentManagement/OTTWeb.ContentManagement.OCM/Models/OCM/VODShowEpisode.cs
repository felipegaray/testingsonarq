﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class VODShowEpisode : VODContent
    {
        [JsonProperty(PropertyName = "showName")]
        public string ShowName { get; set; }
        [JsonProperty(PropertyName = "episode")]
        public short EpisodeNumber { get; set; }
        [JsonProperty(PropertyName = "season")]
        public short SeasonNumber { get; set; }
        [JsonProperty(PropertyName = "showID")]
        public string ShowID { get; set; }
        [JsonProperty(PropertyName = "seasonID")]
        public string SeasonID { get; set; }

    }
}
