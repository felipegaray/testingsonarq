﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class Station
    {
        /// <summary>
        /// The Station ID
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        /// <summary>
        /// The Respurce Type
        /// </summary>
        [JsonProperty(PropertyName = "resourceType")]
        public string ResourceType { get; set; }
        /// <summary>
        /// The Station Name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        /// <summary>
        /// the Station Short Name
        /// </summary>
        [JsonProperty(PropertyName = "shortName")]
        public string ShortName { get; set; }
        /// <summary>
        /// The Station Number
        /// </summary>
        [JsonProperty(PropertyName = "stationNumber")]
        public string StationNumber { get; set; }
        /// <summary>
        /// The Station Language
        /// </summary>
        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; }
        /// <summary>
        /// The Station Language Region
        /// </summary>
        [JsonProperty(PropertyName = "languageRegion")]
        public string LanguageRegion { get; set; }
        /// <summary>
        /// The Picture Id
        /// </summary>
        [JsonProperty(PropertyName = "pictureID")]
        public string PictureID { get; set; }
        /// <summary>
        /// The Call Sign
        /// </summary>
        [JsonProperty(PropertyName = "callSign")]
        public string CallSign { get; set; }
        /// <summary>
        /// The Blackout Station
        /// </summary>
        [JsonProperty(PropertyName = "blackoutStation")]
        public string BlackoutStation { get; set; }
        /// <summary>
        /// The Region IDs
        /// </summary>
        [JsonProperty(PropertyName = "regionIDs")]
        public string[] RegionIDs { get; set; }
        /// <summary>
        /// The Asset IDs
        /// </summary>
        [JsonProperty(PropertyName = "assetIDs")]
        public string[] AssetIDs { get; set; }
        /// <summary>
        /// The entitled property
        /// </summary>
        [JsonProperty(PropertyName = "entitled")]
        public bool Entitled { get; set; }      
        /// <summary>
        /// The assets metadata
        /// </summary>
        [JsonProperty(PropertyName = "metadata")]
        public AssetMetadata Metadata { get; set; }
        /// <summary>
        /// The ingestionLock
        /// </summary>
        [JsonProperty(PropertyName = "ingestionLock")]
        public bool IngestionLock { get; set; }
        /// <summary>
        /// the publish start
        /// </summary>
        [JsonProperty(PropertyName = "publishStart")]
        public string PublishStart { get; set; }
        /// <summary>
        ///The publish end
        /// </summary>
        [JsonProperty(PropertyName = "publishEnd")]
        public string PublishEnd { get; set; }
        /// <summary>
        /// The published property
        /// </summary>
        [JsonProperty(PropertyName = "published")]
        public bool Published { get; set; }
    }
}
