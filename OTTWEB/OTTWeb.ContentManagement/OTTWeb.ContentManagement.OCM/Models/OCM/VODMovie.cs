﻿using Newtonsoft.Json;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class VODMovie : VODContent
    {
        [JsonProperty(PropertyName = "releaseYear")]
        public int RealeaseYear { get; set; }
    }
}
