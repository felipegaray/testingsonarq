﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class SearchAssetById : OCMResponse
    {
        [JsonProperty(PropertyName = "assets")]
        public List<SearchAsset> Assets { get; set; }
    }
}
