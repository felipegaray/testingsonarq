﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetEpgEpisodeResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "epg/episodes")]
        public List<EPGEpisode> EpgEpisodes { get; set; }
    }
}
