﻿using Newtonsoft.Json;
using OTTWeb.ContentManagement.OCM.Models.OCM;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetPageResponse: OCMResponse
    {
        [JsonProperty(PropertyName = "page")]
        public Page Page { get; set; }
    }
}
