﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class RetrieveEPGShowEpisode
    {
        public List<EPGEpisode> EpgEpisodes { get; set; }
    }
}
