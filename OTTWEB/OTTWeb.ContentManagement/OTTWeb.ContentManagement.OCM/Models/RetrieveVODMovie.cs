﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class RetrieveVODMovie
    {
        public List<OCMAsset> Assets { get; set; }      
        public List<VODMovie> VodMovies { get; set; }
    }
}
