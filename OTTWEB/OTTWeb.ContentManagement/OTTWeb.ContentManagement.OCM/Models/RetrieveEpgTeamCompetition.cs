﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class RetrieveEpgTeamCompetition
    {
        public List<EPGTeamCompetition> EpgTeamCompetitions { get; set; }
    }
}
