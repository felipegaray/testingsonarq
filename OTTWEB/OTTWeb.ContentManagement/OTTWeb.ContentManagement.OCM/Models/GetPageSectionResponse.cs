﻿using Newtonsoft.Json;
using OTTWeb.ContentManagement.OCM.Models.OCM;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetPageSectionResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "section")]
        public Section Section { get; set; }
    }
}
