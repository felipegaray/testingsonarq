﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class RetrieveEpgEvent
    {
        public List<EPGEvent> EpgEvents { get; set; }
    }
}
