﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetEpgCompetitionResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "epg/competitions")]
        public List<EPGCompetition> EpgCompetitions { get; set; }
    }
}
