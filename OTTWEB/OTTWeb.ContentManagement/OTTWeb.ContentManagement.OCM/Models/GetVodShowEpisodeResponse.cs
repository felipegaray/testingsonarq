﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetVodShowEpisodeResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "assets")]
        public List<OCMAsset> Assets { get; set; }    

        [JsonProperty(PropertyName = "vod/episodes")]
        public List<VODShowEpisode> VodShowEpisodes { get; set; }
    }
}
