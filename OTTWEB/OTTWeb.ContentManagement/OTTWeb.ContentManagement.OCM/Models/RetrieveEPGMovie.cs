﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class RetrieveEPGMovie
    {
        public List<EPGMovie> EpgMovies { get; set; }
    }
}
