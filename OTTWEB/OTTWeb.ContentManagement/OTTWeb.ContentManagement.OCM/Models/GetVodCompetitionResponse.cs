﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetVodCompetitionResponse : OCMResponse
    {
        [JsonProperty(PropertyName = "vod/competitions")]
        public List<VODCompetition> VodCompetitions { get; set; }
    }
}
