﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class RetrieveVODShowEpisode
    {
        public List<OCMAsset> Assets { get; set; }  
        public List<VODShowEpisode> VodShowEpisodes { get; set; }
    }
}
