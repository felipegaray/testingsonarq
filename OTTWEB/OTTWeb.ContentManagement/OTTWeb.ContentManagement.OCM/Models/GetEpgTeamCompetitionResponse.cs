﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class GetEpgTeamCompetitionResponse : OCMResponse
    {        
        [JsonProperty(PropertyName = "epg/teamCompetitions")]
        public List<EPGTeamCompetition> EpgTeamCompetitions { get; set; }
    }
}
