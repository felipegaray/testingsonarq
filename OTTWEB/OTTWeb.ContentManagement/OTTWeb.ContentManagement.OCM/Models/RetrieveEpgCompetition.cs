﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class RetrieveEpgCompetition
    {
        public List<EPGCompetition> EpgCompetitions { get; set; }
    }
}
