﻿using Newtonsoft.Json;
using OTTWeb.Common.Proxy.Models;

namespace OTTWeb.ContentManagement.OCM.Models
{
    public class OCMResponse : OrbisResponseModel
    {
        [JsonProperty(PropertyName = "metadata")]
        public Metadata Metadata { get; set; }
    }
}
