﻿using AppDynamics;
using AutoMapper;
using Microsoft.Extensions.Logging;
using OTTWeb.Common.Http;
using OTTWeb.Common.Http.Extensions;
using OTTWeb.Common.Http.Interfaces;
using OTTWeb.Common.Proxy.Exceptions;
using OTTWeb.Common.Proxy.Models.Errors;
using OTTWeb.ContentManagement.OCM.Interfaces;
using OTTWeb.ContentManagement.OCM.Models;
using OTTWEB.Common.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.OCM.Implementations
{
    public class OCMProxy : OTTBaseProxy, IOCMProxy
    {
        #region Injected Services

        private readonly string _orbisUrl;
        private IMapper _mapper;
        private Microsoft.Extensions.Logging.ILogger _logger;
        private IRequestFactory _requestFactory;
        private IProxyExceptionFactory _proxyExceptionFactory;

        #endregion

        #region "Constructor"

        public OCMProxy(IMapper mapper, IRequestFactory requestFactory, ILoggerFactory loggerFactory, IProxyExceptionFactory proxyExceptionFactory)
        {
            _orbisUrl = Environment.GetEnvironmentVariable("ENV-ORBIS-URL");
            _mapper = mapper;
            _requestFactory = requestFactory;
            _logger = loggerFactory.CreateLogger<OCMProxy>();
            _proxyExceptionFactory = proxyExceptionFactory;
        }

        #endregion "Constructor"

        #region "IOCMProxy Members"

        /// <summary>
        /// Used for retrieving an asset by the ID (primary key)
        /// </summary>
        /// <remarks>
        /// OCM V2 Orbis Endpoint: Returns information about one asset
        /// </remarks>
        /// <param name="assetID">The Asset Id</param>
        /// <returns>An Asset Object</returns>
        public async Task<SearchAsset> SearchAssetById(string assetID, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/assets");
            try
            {
                SearchAssetById OcmAssetResponse;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/assets/{assetID}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                        $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                        $"- Method: {GetActualAsyncMethodName()} " +
                                        $"- Parameters: AssetID = {assetID}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);
                //Calls the Get Method
                var response = await _requestFactory.Get(uriService);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    OcmAssetResponse = response.ContentAsType<SearchAssetById>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {OcmAssetResponse.TimeStamp} " +
                                    $"- Orbis RequestID: {OcmAssetResponse.RequestID} " +
                                    $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                    $"- Response Count: {OcmAssetResponse.Metadata.Count} Assets");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return OcmAssetResponse.Assets.FirstOrDefault();
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// This request is used to search and return a list of matching content using the filters
        /// language and query (keyword) through querystring
        /// </summary>
        /// <remarks>
        /// OCM V2 Orbis Endpoint: Return a list of matching content
        /// </remarks>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <returns>A list of Assets</returns>
        public async Task<SearchAssetsByKeywordResponse> SearchAssetsByKeyword(Dictionary<string, string> queryString, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/search");
            try
            {
                SearchAssetsByKeywordResponse OcmAssetsResponse;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/search");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                        $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                        $"- Method: {GetActualAsyncMethodName()} " +
                                        $"- Parameters: {string.Join(", ", queryString.Select(kvp => kvp.Key + "= " + kvp.Value.ToString()))} ");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);
                //Calls the Get Method with the querystring collection
                var response = await _requestFactory.Get(uriService, null, queryString);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    var contentAsString = response.ContentAsString();
                    OcmAssetsResponse = response.ContentAsType<SearchAssetsByKeywordResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {OcmAssetsResponse.TimeStamp} " +
                                            $"- Orbis RequestID: {OcmAssetsResponse.RequestID} " +
                                            $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath} " +
                                            $"- Response Count: {OcmAssetsResponse.Metadata.Count} Assets");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return OcmAssetsResponse;
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Gets the data for the Page(Lander) defined in the pageID, it is organized in sections, subsections and the list of Assets
        /// </summary>
        /// <param name="pageId">The ID of the page to search</param>
        /// <returns></returns>
        public async Task<GetPageResponse> GetPage(string pageId, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v3/pages");
            try
            {
                GetPageResponse ocmPageResponse;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v3/pages/{pageId}?include=entitlements");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: PageId: {pageId}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);
                //Calls the Get Method with the querystring collectionsss
                var response = await _requestFactory.Get(uriService);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmPageResponse = response.ContentAsType<GetPageResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmPageResponse.TimeStamp} " +
                                            $"- Orbis RequestID: {ocmPageResponse.RequestID} " +
                                            $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                            $"- Response Count: {ocmPageResponse?.Page?.Sections?.Count ?? 0} Sections");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return ocmPageResponse;
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Gets the data for a specific section of a Page(Lander) defined in the pageID,
        /// it is organized in sections, subsections and the list of Assets
        /// </summary>
        /// <param name="querystring">The parameters received by querystring</param>
        /// <param name="pageId">The ID of the page to search</param>
        /// <param name="sectionId">The ID of the section inside the page to search</param>
        /// <returns></returns
        public async Task<GetPageSectionResponse> GetPageSection(string pageId, string sectionId, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v3/pages/sections");
            try
            {
                GetPageSectionResponse ocmPageResponse;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v3/pages/{pageId}/sections/{sectionId}?include=entitlements");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()}" +
                                    $"- Parameters: PageId = {pageId} ,SectionId = {sectionId}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);
                //Calls the Get Method with the querystring collectionsss
                var response = await _requestFactory.Get(uriService);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmPageResponse = response.ContentAsType<GetPageSectionResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmPageResponse.TimeStamp} " +
                                        $"- Orbis RequestID: {ocmPageResponse.RequestID} " +
                                        $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                        $"- Response Count: {ocmPageResponse?.Section?.Subsections?.FirstOrDefault().Items?.Count ?? 0} Sections");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return ocmPageResponse;
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// This request is used to get a collection of schedules filtered by the querystring parameters
        /// <remarks>
        /// Orbis Endpoint: Returns a list of scheduled content
        /// </remarks>
        /// </summary>
        /// <param name="querystring">The query string collection with all possible parameters</param>
        /// <returns>A list of Schedules</returns>
        public async Task<List<Schedule>> ReturnsAListOfScheduledContent(string querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/schedule");
            try
            {
                ReturnsAListOfScheduledContentResponse OcmScheduleContentResponse;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/schedule?{querystring}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: {querystring}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method with the querystring collection
                var response = await _requestFactory.Get(uriService, null);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    OcmScheduleContentResponse = response.ContentAsType<ReturnsAListOfScheduledContentResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {OcmScheduleContentResponse.TimeStamp} " +
                                    $"- Orbis RequestID: {OcmScheduleContentResponse.RequestID} " +
                                    $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                    $"- Response Count: {OcmScheduleContentResponse.Metadata.Count} Assets");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return OcmScheduleContentResponse.Schedule;
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// This request is used to get a collection of stations filtered by the querystring parameters
        /// </summary>
        /// <remarks>
        /// Orbis Endpoint: Returns a list of stations
        /// </remarks>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <returns>A list of Stations</returns>
        public async Task<ReturnsAListOfStationsResponse> ReturnsAListOfStations(Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/epg/stations");
            try
            {
                ReturnsAListOfStationsResponse OcmStationsResponse;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/epg/stations");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: {string.Join(", ", querystring.Select(kvp => kvp.Key + "= " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method with the querystring collection
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    OcmStationsResponse = response.ContentAsType<ReturnsAListOfStationsResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {OcmStationsResponse.TimeStamp} " +
                                    $"- Orbis RequestID: {OcmStationsResponse.RequestID} " +
                                    $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                    $"- Response Count: {OcmStationsResponse.Metadata.Count} Assets");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return OcmStationsResponse;
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// This request is used to get a station
        /// <remarks>
        /// Orbis Endpoint: Returns information about one station
        /// </remarks>
        /// </summary>
        /// <param name="stationId">The Movie Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <returns>A Station Data</returns>
        public async Task<Station> ReturnsInformationAboutOneStation(string stationId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/epg/stations/stationId");
            try
            {
                ReturnsInformationAboutOneStationResponse OcmStationResponse;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/epg/stations/{stationId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                        $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                        $"- Method: {GetActualAsyncMethodName()} " +
                                        $"- Parameters: StationId = {stationId} , " +
                                        $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method with the querystring collection
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    OcmStationResponse = response.ContentAsType<ReturnsInformationAboutOneStationResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {OcmStationResponse.TimeStamp} " +
                                            $"- Orbis RequestID: {OcmStationResponse.RequestID} " +
                                            $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                            $"- Response Count: {OcmStationResponse.Metadata.Count} Assets");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return OcmStationResponse.Stations.SingleOrDefault();
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Returns a Movie data by the ScheduledMovieId provided
        /// <remarks>Orbis Endpoint: Retrieve scheduled movie from the EPG</remarks>
        /// </summary>
        /// <param name="scheduledMovieId">The Movie Id to be searched</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Movie Object</returns>
        public async Task<RetrieveEPGMovie> RetrieveScheduledMovieFromTheEPG(string scheduledMovieId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/epg/movies");
            try
            {
                GetEpgMovieResponse ocmResponseMovie;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/epg/movies/{scheduledMovieId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: MovieId = {scheduledMovieId} , " +
                                    $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmResponseMovie = response.ContentAsType<GetEpgMovieResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmResponseMovie.TimeStamp} " +
                                        $"- Orbis RequestID: {ocmResponseMovie.RequestID} " +
                                        $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                        $"- Response Count: {ocmResponseMovie.Metadata.Count} Asset");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return _mapper.Map<RetrieveEPGMovie>(ocmResponseMovie);
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Returns a Event/Episote data by the ScheduledEpisodeId provided
        /// <remarks>
        /// Orbis Endpoint: Retrieve scheduled show episode from the EPG
        /// </remarks>
        /// </summary>
        /// <param name="scheduledEpisodeId">The Episode Id to be searched</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response Movie Object</returns>
        public async Task<RetrieveEPGShowEpisode> RetrieveScheduledShowEpisodeFromTheEPG(string scheduledEpisodeId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/epg/episodes");
            try
            {
                GetEpgEpisodeResponse ocmResponseEpisode;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/epg/episodes/{scheduledEpisodeId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: EpisodeId = {scheduledEpisodeId} , " +
                                    $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmResponseEpisode = response.ContentAsType<GetEpgEpisodeResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmResponseEpisode.TimeStamp} " +
                                        $"- Orbis RequestID: {ocmResponseEpisode.RequestID} " +
                                        $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                        $"- Response Count: {ocmResponseEpisode.Metadata.Count} Asset");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return _mapper.Map<RetrieveEPGShowEpisode>(ocmResponseEpisode);
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Returns a Event data by the ScheduledEventId provided
        /// <remarks>
        /// Orbis Endpoint: Retrieve scheduled event from the EPG
        /// </remarks>
        /// </summary>
        /// <param name="scheduledEventId">The Event Id to be searched</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response Event Object</returns>
        public async Task<RetrieveEpgEvent> RetrieveScheduledEventFromTheEPG(string scheduledEventId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/epg/events");
            try
            {
                GetEpgEventResponse ocmResponseEvent;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/epg/events/{scheduledEventId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: EventId = {scheduledEventId} ," +
                                    $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmResponseEvent = response.ContentAsType<GetEpgEventResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmResponseEvent.TimeStamp} " +
                                    $"- Orbis RequestID: {ocmResponseEvent.RequestID} " +
                                    $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                    $"- Response Count: {ocmResponseEvent.Metadata.Count} Asset");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return _mapper.Map<RetrieveEpgEvent>(ocmResponseEvent);
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Returns a Team Competitions data by the ScheduledTeamCompetitionId provided
        /// <remarks>
        /// Orbis Endpoint: Retrieve scheduled team game from the EPG
        /// </remarks>
        /// </summary>
        /// <param name="scheduledTeamCompetitionId">The Team Competitions Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response Team Competition Object</returns>
        public async Task<RetrieveEpgTeamCompetition> RetrieveScheduledTeamGameFromTheEPG(string scheduledTeamCompetitionId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/epg/teamCompetitions");
            try
            {
                GetEpgTeamCompetitionResponse ocmResponseTeamCompetition;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/epg/teamCompetitions/{scheduledTeamCompetitionId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: Team Competition Id = {scheduledTeamCompetitionId} , " +
                                    $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmResponseTeamCompetition = response.ContentAsType<GetEpgTeamCompetitionResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmResponseTeamCompetition.TimeStamp} " +
                                        $"- Orbis RequestID: {ocmResponseTeamCompetition.RequestID} " +
                                        $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                        $"- Response Count: {ocmResponseTeamCompetition.Metadata.Count} Asset");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return _mapper.Map<RetrieveEpgTeamCompetition>(ocmResponseTeamCompetition);
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Returns a Competition data by the ScheduledCompetitionId provided
        /// <remarks>
        /// Orbis Endpoint: Retrieve scheduled game from the EPG
        /// </remarks>
        /// </summary>
        /// <param name="scheduledCompetitionId">The Competitions Id to be searched</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response Competition Object</returns>
        public async Task<RetrieveEpgCompetition> RetrieveScheduledGameFromTheEPG(string scheduledCompetitionId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/epg/competitions");
            try
            {
                GetEpgCompetitionResponse ocmResponseCompetition;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/epg/competitions/{scheduledCompetitionId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: Competition Id = {scheduledCompetitionId} , " +
                                    $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmResponseCompetition = response.ContentAsType<GetEpgCompetitionResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmResponseCompetition.TimeStamp} " +
                                    $"- Orbis RequestID: {ocmResponseCompetition.RequestID} " +
                                    $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                    $"- Response Count: {ocmResponseCompetition.Metadata.Count} Asset");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return _mapper.Map<RetrieveEpgCompetition>(ocmResponseCompetition);
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Return the metadata associated with the specified movie from the on-demand cata_logger.
        /// <remarks>
        /// Orbis Endpoint: Retrieve movie from the on-demand catalog
        /// </remarks>
        /// </summary>
        /// <param name="vodMovieId">The Movie Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>A Response VOD Movie Object</returns>
        public async Task<RetrieveVODMovie> RetrieveMovieFromTheOnDemandCatalog(string vodMovieId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/vod/movies");
            try
            {
                GetVodMovieResponse ocmResponseMovie;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/vod/movies/{vodMovieId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: MovieId = {vodMovieId} , " +
                                    $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmResponseMovie = response.ContentAsType<GetVodMovieResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmResponseMovie.TimeStamp} " +
                                    $"- Orbis RequestID: {ocmResponseMovie.RequestID} " +
                                    $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                    $"- Response Count: {ocmResponseMovie.Metadata.Count} Asset");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return _mapper.Map<RetrieveVODMovie>(ocmResponseMovie);
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Returns a show and its seasons
        /// <remarks>
        /// Orbis Endpoint: Get a single show
        /// </remarks>
        /// </summary>
        /// <param name="showId">The id of the show to be searched</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>a show response object</returns>
        public async Task<GetShowResponse> GetShowAndSeasons(string showId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/shows");
            try
            {
                GetShowResponse ocmShowResponse;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/shows/{showId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                        $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                        $"- Method: {GetActualAsyncMethodName()} " +
                                        $"- Parameters: Show Id = {showId} , " +
                                        $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmShowResponse = response.ContentAsType<GetShowResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmShowResponse.TimeStamp} " +
                                    $"- Orbis RequestID: {ocmShowResponse.RequestID} " +
                                    $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                    $"- Response Count: {ocmShowResponse.Metadata.Count} Records");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return ocmShowResponse;
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Returns a season detail and its episodes
        /// <remarks>
        /// Orbis Endpoint: Get a single season
        /// </remarks>
        /// </summary>
        /// <param name="seasonId">The id of the season to be searched</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="Exceptions.OCMServiceException"></exception>
        /// <exception cref="Exceptions.OCMBusinessException"></exception>
        /// <exception cref="Exceptions.OCMUnhandledException"></exception>
        /// <returns>a season response object</returns>
        public async Task<GetSeasonResponse> GetSeason(string seasonId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/seasons");
            try
            {
                GetSeasonResponse ocmShowResponse;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/seasons/{seasonId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: SeasonId = {seasonId} , " +
                                    $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmShowResponse = response.ContentAsType<GetSeasonResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmShowResponse.TimeStamp} " +
                                        $"- Orbis RequestID: {ocmShowResponse.RequestID} " +
                                        $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                        $"- Response Count: {ocmShowResponse.Metadata.Count} Records");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return ocmShowResponse;
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Return the metadata associated with the specified competition from the on-demand cata_logger.
        /// <remarks>
        /// Orbis Endpoint: Retrieve game from the on-demand catalog
        /// </remarks>
        /// </summary>
        /// <param name="competitionId">The Competition Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="OCMServiceException"></exception>
        /// <exception cref="OCMBusinessException"></exception>
        /// <exception cref="OCMUnhandledException"></exception>
        /// <returns>A Response VOD Competition Object</returns>
        public async Task<RetrieveVODCompetition> RetrieveGameFromTheOnDemandCatalog(string competitionId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/vod/competitions");
            try
            {
                GetVodCompetitionResponse ocmResponseCompetition;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/vod/competitions/{competitionId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                        $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                        $"- Method: {GetActualAsyncMethodName()} " +
                                        $"- Parameters: CompetitionId = {competitionId} , " +
                                        $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmResponseCompetition = response.ContentAsType<GetVodCompetitionResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmResponseCompetition.TimeStamp} " +
                                    $"- Orbis RequestID: {ocmResponseCompetition.RequestID} " +
                                    $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                    $"- Response Count: {ocmResponseCompetition.Metadata.Count} Asset");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return _mapper.Map<RetrieveVODCompetition>(ocmResponseCompetition);
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Return the metadata associated with the specified team competition from the on-demand cata_logger.
        /// <remarks>
        /// Orbis Endpoint: Retrieve team game from the on-demand catalog
        /// </remarks>
        /// </summary>
        /// <param name="teamCompetitionId">The Team Competition Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="OCMServiceException"></exception>
        /// <exception cref="OCMBusinessException"></exception>
        /// <exception cref="OCMUnhandledException"></exception>
        /// <returns>A Response VOD Team Competition Object</returns>
        public async Task<RetrieveVODTeamCompetition> RetrieveTeamGameFromTheOnDemandCatalog(string teamCompetitionId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/vod/teamCompetitions");
            try
            {
                GetVodTeamCompetitionResponse ocmResponseTeamCompetition;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/vod/teamCompetitions/{teamCompetitionId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.UtcNow} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: TeamCompetitionId = {teamCompetitionId} , " +
                                    $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmResponseTeamCompetition = response.ContentAsType<GetVodTeamCompetitionResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmResponseTeamCompetition.TimeStamp} " +
                                    $"- Orbis RequestID: {ocmResponseTeamCompetition.RequestID} " +
                                    $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                    $"- Response Count: {ocmResponseTeamCompetition.Metadata.Count} Asset");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return _mapper.Map<RetrieveVODTeamCompetition>(ocmResponseTeamCompetition);
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        /// <summary>
        /// Return the metadata associated with the specified show episode from the on-demand cata_logger.
        /// <remarks>
        /// Orbis Endpoint: Retrieve show episode from the on-demand catalog
        /// </remarks>
        /// </summary>
        /// <param name="episodeId">The Episode Id to be searched</param>
        /// <param name="queryString">The query string collection with all possible parameters</param>
        /// <param name="userSessionToken">The user session token</param>
        /// <exception cref="Newtonsoft.Json.JsonException"></exception>
        /// <exception cref="OCMServiceException"></exception>
        /// <exception cref="OCMBusinessException"></exception>
        /// <exception cref="OCMUnhandledException"></exception>
        /// <returns>A Response VOD Show Episode Object</returns>
        public async Task<RetrieveVODShowEpisode> RetrieveEpisodeFromTheOnDemandCatalog(string episodeId, Dictionary<string, string> querystring, BusinessTransaction bt)
        {
            var exitCall = AgentSDK.CreateExitCall(bt, "HTTP", "GET ocm/v2/vod/episodes");
            try
            {
                GetVodShowEpisodeResponse ocmResponseShowEpisode;
                //Creates the Service Urls data
                UriService uriService = CreateUriService(_orbisUrl, $"ocm/v2/vod/episodes/{episodeId}");
                //Creates the log prior the service call
                _logger.LogDebug($"UTC Time: {DateTime.Now.ToString()} " +
                                    $"- Url: {uriService.ServiceBaseUri}/{uriService.ServiceRelativeUri} " +
                                    $"- Method: {GetActualAsyncMethodName()} " +
                                    $"- Parameters: EpisodeID = {episodeId} , " +
                                    $"{string.Join(", ", querystring.Select(kvp => kvp.Key + " = " + kvp.Value.ToString()))}");

                AgentSDK.AddIdentifyingPropertyToExitCall(exitCall, "OCM Proxy Method", GetActualAsyncMethodName());
                AgentSDK.StartExitCall(exitCall);

                //Calls the Get Method
                var response = await _requestFactory.Get(uriService, null, querystring);
                //Check if the response was success
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    ocmResponseShowEpisode = response.ContentAsType<GetVodShowEpisodeResponse>();
                    //Creates the log when response is succesful
                    _logger.LogDebug($"UTC Time: {ocmResponseShowEpisode.TimeStamp} " +
                                    $"- Orbis RequestID: {ocmResponseShowEpisode.RequestID} " +
                                    $"- SUCCESS response - Source: {response.RequestMessage.RequestUri.AbsolutePath}" +
                                    $"- Response Count: {ocmResponseShowEpisode.Metadata.Count} Asset");
                }
                else
                {
                    ErrorData errorData = response.ContentAsType<ErrorData>();
                    AgentSDK.AddErrorToExitCall(exitCall, uriService.ServiceRelativeUri, $"Orbis RequestID: {errorData.RequestID}", false);
                    AgentSDK.AddErrorToTransaction(bt, uriService.ServiceRelativeUri, $"Description: {errorData.ErrorMessage} Orbis RequestID: {errorData.RequestID}", Convert.ToInt16(errorData.ErrorCode), false);
                    //If it was not successful, raise a custom exception with the error data and a message
                    throw _proxyExceptionFactory.CreateException(response, _logger);
                }
                return _mapper.Map<RetrieveVODShowEpisode>(ocmResponseShowEpisode);
            }
            catch (Exception ex) when (!(ex is ProxyBaseException))
            {
                var exceptionType = ex.GetType().ToString();
                _logger.LogError(ex, $"UTC Time: {DateTime.UtcNow} - {exceptionType}");

                ErrorData errorData = new ErrorData() { ErrorCode = exceptionType, ErrorMessage = ex.Message };
                AgentSDK.AddErrorToExitCall(exitCall, errorData.ErrorCode, errorData.ErrorMessage, false);
                AgentSDK.AddErrorToTransaction(bt, errorData.ErrorCode, errorData.ErrorMessage, 0, false);

                throw;
            }
            finally
            {
                AgentSDK.StopExitCall(exitCall);
            }
        }

        #endregion "IOCMProxy Members"
    }
}