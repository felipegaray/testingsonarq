pipeline {
  // Created by: Miguel Llacuna Chávez
  // Date: 08 Oct 2018
  // Project: Directv OTT Web & QC
  // Provider: Globant
  agent {
    label 'master'
  }

  environment {
    // ----------------------------------------------
    // PATHS
    PATHCOMMON = "/OTTWEB/OTTWEB.Common"
    PATHBE = "/OTTWEB/OTTWeb.ContentManagement"
    PATHAPI = "/OTTWEB/OTTWeb.ContentManagement/OTTWeb.ContentManagement.API"
    PATHENV = "/BE/RT"
    PATHSRV = "content"
    FILEENV = "rt.env"
    // ----------------------------------------------
    // ECR SERVICE
    ECR = "617167195340.dkr.ecr.us-east-1.amazonaws.com"
    ECRURL="http://${ECR}"
    ECRCRED = "ecr:us-east-1:dtv-ott-ecs-credentials"
    ENVIMG = "dtv-ott-img-rt"
    VERSION = "latest"
    NOW = "nothing"
    // ----------------------------------------------
    // ECS SERVICE
    ENVCLS = "DTV-OTT-CLS-RT"
    PREFIX = "dtv-ott"
    SERVICE = "apicontent-rt"
    LOGGROUPECS = "/ecs/dtv-ott"
    LOGGROUPAPP = "/app/dtv-ott"
    ARNTGG = "arn:aws:elasticloadbalancing:us-east-1:617167195340:targetgroup/dtv-ott-tgg-apicontent-rt/3a1003cf0999c9a5"
    URLLB = "https://dtv-ott-alb-website-rt-2017766070.us-east-1.elb.amazonaws.com/content/keepalive"
    URLBE = "https://rt.directvgo.com/content/keepalive"
    // ----------------------------------------------
    // GENERAL
    URLGITREPOAPI = "https://miguel_llacuna@bitbucket.org/directvla/dtvapi.git"
    URLGITREPOENV = "https://miguel_llacuna@bitbucket.org/directvla/dtvservicesenv.git"
    GIT_CREDS = "cb15b714-36d1-4305-b6b0-01cdeb715f23"
    BRANCHAPI = "release_testing"
    BRANCHENV = "master"
    SLACK_CHANNEL = "#g-s-dc-dtvott-blbackn"
    // ----------------------------------------------
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '10'))
    disableConcurrentBuilds()
  }

  stages {
    stage('Checkout DTVAPI and DTVSERVICEENV Repositories') {
      steps {
        dir("${WORKSPACE}"){
          // Checkout code repo
          git credentialsId: "${GIT_CREDS}", url: "${URLGITREPOAPI}", branch: "${BRANCHAPI}"
            script {
              VERSION = sh(returnStdout: true, script: 'git rev-parse HEAD').take(7)
            }
        }
        // Checkout environment variables repo
        dir("${WORKSPACE}/servicesenv"){
            git credentialsId: "${GIT_CREDS}", url: "${URLGITREPOENV}", branch: "${BRANCHENV}"
        }
      }
    }
    stage('Build Image API Content') {
      steps {
        script {
          NOW = sh(returnStdout: true, script:"date +'%Y%m%d-%H%M%S%3N'").trim()
          IMAGE = "${ENVIMG}:${SERVICE}-${VERSION}-${NOW}"
          dir("${WORKSPACE}${PATHCOMMON}") {
            // Move nupkgs folder to service path
            sh("mv nupkgs ${WORKSPACE}${PATHBE}/")
          }
          dir("${WORKSPACE}${PATHAPI}") {
            // Insert service name for log app
            SEARCH = "OTT-Web-Backend-Content"
            REPLACE = "${LOGGROUPAPP}-${SERVICE}"
            sh("sed -i 's|${SEARCH}|${REPLACE}|g' log4net.config")
          }
          dir("${WORKSPACE}${PATHBE}") {
            // Build image
            docker.build("${ECR}/${IMAGE}")
          }
        }
      }
    }
    stage ('Deploy Service API Content') {
      steps {
        script {
          dir("${WORKSPACE}${PATHBE}") {
            // Login to AWS
            sh("eval \$(aws ecr get-login --no-include-email | sed 's|https://||')")
            // Push in ECR
            docker.withRegistry("${ECRURL}", "${ECRCRED}") {
              docker.image("${ECR}/${IMAGE}").push()
            }
            // Delete local image
            sh("docker rmi -f ${ECR}/${IMAGE}")
            // Copy environment, docker and container files to service path
            dir("${WORKSPACE}/servicesenv${PATHENV}") {
              sh("cp ${FILEENV} ${WORKSPACE}${PATHBE}/")
              sh("cp ${PATHSRV}/*.* ${WORKSPACE}${PATHBE}/")
            }
            // Insert service name
            sh("sed -i 's|${PREFIX}|${PREFIX}-${SERVICE}|g' docker-compose-aws.yml")
            // Insert image name
            SEARCH='replaceimage'
            REPLACE="${ECR}/${IMAGE}"
            sh("sed -i 's|${SEARCH}|${REPLACE}|g' docker-compose-aws.yml")
            // Insert log group
            SEARCH='replacegroup'
            REPLACE="${LOGGROUPECS}-${SERVICE}"
            sh("sed -i 's|${SEARCH}|${REPLACE}|g' docker-compose-aws.yml")
            // Launch service in ECS Fargate
            sh("ecs-cli compose --file docker-compose-aws.yml --project-name ${PREFIX}-${SERVICE} --cluster ${ENVCLS} service up --create-log-groups --launch-type FARGATE --target-group-arn ${ARNTGG} --container-name ${PREFIX}-${SERVICE} --container-port 80 --timeout 15")
            sh("ecs-cli compose --file docker-compose-aws.yml --project-name ${PREFIX}-${SERVICE} --cluster ${ENVCLS} service scale 3")
          }
        }
      }
      post {
        success {
            slackSend (
                channel: "${env.SLACK_CHANNEL}",
                color: "good",
                message: "JOB: ${env.JOB_NAME} - #${env.BUILD_NUMBER} ${currentBuild.currentResult} after ${currentBuild.durationString.replace(' and counting', '')} (<${RUN_DISPLAY_URL}|Open>)\n SERVICE: ${PREFIX}-${SERVICE} deployed in Cluster ${ENVCLS}\n IMAGE: ${IMAGE}\n URL: ${URLBE}\n URL: ${URLLB}"
            )
        }
        failure {
            slackSend (
                channel: "${env.SLACK_CHANNEL}",
                color: "danger",
                message: "JOB: ${env.JOB_NAME} - #${env.BUILD_NUMBER} ${currentBuild.currentResult} after ${currentBuild.durationString.replace(' and counting', '')} (<${RUN_DISPLAY_URL}|Open>)\n SERVICE: ${PREFIX}-${SERVICE} deploy failed in Cluster ${ENVCLS}\n IMAGE: ${IMAGE}\n URL: ${URLBE}\n URL: ${URLLB}"
            )
        }
      }
    }
  }
  post {
    always {
      cleanWs()
    }
  }
}
