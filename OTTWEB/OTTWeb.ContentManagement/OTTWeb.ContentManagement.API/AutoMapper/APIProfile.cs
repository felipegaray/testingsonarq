﻿using AutoMapper;
using APIMODEL = OTTWeb.ContentManagement.API.Models;
using SERVICE = OTTWeb.ContentManagement.Services.Models;

namespace OTTWeb.ContentManagement.API.AutoMapper
{
    /// <summary>
    /// Contains all mapping configurations needed at API level
    /// </summary>
    public class APIProfile : Profile
    {
        /// <summary>
        /// The Mapper Profile Implementation needed at API level
        /// </summary>
        public APIProfile()
        {
            //Mapping from SERVICE to API
            CreateMap<SERVICE.Asset, APIMODEL.Asset>();
            CreateMap<SERVICE.SearchAsset, APIMODEL.SearchAsset>();
            CreateMap<SERVICE.SearchResult, APIMODEL.SearchResult>();                   
            CreateMap<SERVICE.Schedule, APIMODEL.Schedule>();
            CreateMap<SERVICE.Station, APIMODEL.Station>();
            CreateMap<SERVICE.SchedulesByStation, APIMODEL.SchedulesByStation>();
            CreateMap<SERVICE.GetPageResult, APIMODEL.PageResponse>();
            CreateMap<SERVICE.GetPageSectionResult, APIMODEL.PageSectionResponse>();
            CreateMap<SERVICE.Page, APIMODEL.Page>();
            CreateMap<SERVICE.Section, APIMODEL.Section>();
            CreateMap<SERVICE.SubSection, APIMODEL.SubSection>();
            CreateMap<SERVICE.SectionAsset, APIMODEL.SectionAsset>();

            CreateMap<SERVICE.Genre, APIMODEL.Genre>();
            CreateMap<SERVICE.Movie, APIMODEL.EPGMovie>();
            CreateMap<SERVICE.Movie, APIMODEL.VODMovie>();
            CreateMap<SERVICE.Episode, APIMODEL.EPGEpisode>();
            CreateMap<SERVICE.Episode, APIMODEL.VODEpisode>();
            CreateMap<SERVICE.Event, APIMODEL.Event>();
            CreateMap<SERVICE.TeamCompetition, APIMODEL.EPGTeamCompetition>();
            CreateMap<SERVICE.Competition, APIMODEL.EPGCompetition>();
            CreateMap<SERVICE.Competition, APIMODEL.VODCompetition>();
            CreateMap<SERVICE.TeamCompetition, APIMODEL.VODTeamCompetition>();
            CreateMap<SERVICE.Season, APIMODEL.Season>();
            CreateMap<SERVICE.SeasonEpisode, APIMODEL.SeasonEpisode>();
            CreateMap<SERVICE.GetShowResult, APIMODEL.ShowResult>();
            CreateMap<SERVICE.GetSeasonResult, APIMODEL.SeasonResult>();

            //Mapping from API to SERVICE
            CreateMap<APIMODEL.ScheduleParameters, SERVICE.ScheduleParameters>();
            CreateMap<APIMODEL.GetStationsParameters, SERVICE.GetStationsParameters>();
            CreateMap<APIMODEL.GetStationParameters, SERVICE.GetStationParameters>();
            CreateMap<APIMODEL.GetScheduledContentParameters, SERVICE.GetScheduledContentParameters>();
            CreateMap<APIMODEL.GetPageSectionParameters, SERVICE.GetPageSectionParameters>();
            CreateMap<APIMODEL.SearchByKeywordParameters, SERVICE.SearchByKeywordParameters>();
        }
    }

  
}

