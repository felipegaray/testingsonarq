﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    public class SectionGroup
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string AssetListUri { get; set; }
       
    }
}
