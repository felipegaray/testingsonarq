﻿namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The station querystring parameters
    /// </summary>
    public class GetStationsParameters
    {
        /// <summary>
        /// The language especifier [es|en|pt|*]
        /// </summary>
        //[Required]
        public string Language { get; set; } = "*";
        /// <summary>
        /// The region specifier
        /// </summary>
        //[Required]
        public string Region { get; set; } = "*";
        /// <summary>
        /// Maximum number of results returned, used for pagination. If this parameter is not set, by default 10 records will be returned
        /// </summary>
        public int Count { get; set; } = 10;
        /// <summary>
        /// Offset of the returned result set, used for pagination. If this parameter is not set, by default is set in 0
        /// </summary>
        public int Offset { get; set; } = 0;
    }
}
