﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The Show Model
    /// </summary>
    public class Show
    {
        /// <summary>
        /// The Show Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Resource Type
        /// </summary>
        public string ResourceType { get; set; }

        /// <summary>
        /// The Show Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Short Show Name
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// The Show Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The Short Show Description
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// The Show Language (es | en | pt)
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// The Picture Id
        /// </summary>
        public string PictureID { get; set; }

        /// <summary>
        /// The Collection of Picture
        /// </summary>
        public Dictionary<string, string> Pictures { get; set; }

        /// <summary>
        /// The number of seasons
        /// </summary>
        public int Seasons { get; set; }

        /// <summary>
        /// The list with all season ids
        /// </summary>
        public string[] SeasonIDs { get; set; }
    }
}
