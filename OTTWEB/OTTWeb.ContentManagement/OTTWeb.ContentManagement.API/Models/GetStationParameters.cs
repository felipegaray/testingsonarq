﻿namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The list of parameters
    /// </summary>
    public class GetStationParameters
    {
        /// <summary>
        /// The language especifier <remarks>[es|en|pt]</remarks>
        /// </summary>
        public string Language { get; set; } = "*";

        ///// <summary>
        ///// The list of object, separeted by comma, to be included in the response (e.g. assets)
        ///// </summary>
        //public string Include { get; set; }
    }
}
