﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The result of searching an asset by keyword
    /// </summary>
    public class SearchResult
    {
        /// <summary>
        /// List of assets that match the query
        /// </summary>
        public List<SearchAsset> Assets { get; set; }
    }
}
