﻿using System.ComponentModel.DataAnnotations;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The parameters for the Get Section
    /// </summary>
    public class GetPageSectionParameters
    {        
        /// <summary>
        /// Maximum number of results returned
        /// </summary>        
        public int Top { get; set; }        
    }
}
