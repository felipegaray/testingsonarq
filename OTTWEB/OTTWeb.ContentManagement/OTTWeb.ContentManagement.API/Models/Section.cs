﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// Section of the Page
    /// </summary>
    public class Section
    {
        /// <summary>
        /// The label (name) of the section
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// The section ID
        /// </summary>
        public string SectionID { get; set; }
        /// <summary>
        /// The list of subsections contained
        /// </summary>
        public List<SubSection> Subsections { get; set; }
    }
}
