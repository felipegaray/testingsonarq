﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// Sub section of the page inside a Section
    /// </summary>
    public class SubSection
    {
        /// <summary>
        /// The block ID
        /// </summary>        
        public string BlockId { get; set; }
        /// <summary>
        /// The block Label (name) of a subsection
        /// </summary>        
        public string BlockLabel { get; set; }
        /// <summary>
        /// The block name of a subsection
        /// </summary>        
        public string SubsectionName { get; set; }
        /// <summary>
        /// List of assets inside the subsection
        /// </summary>
        public List<SectionAsset> Items { get; set; }
    }
}
