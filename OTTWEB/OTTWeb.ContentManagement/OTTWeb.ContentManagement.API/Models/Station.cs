﻿namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The Station Model
    /// </summary>
    public class Station
    {
        /// <summary>
        /// The Station ID
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The Respurce Type
        /// </summary>
        public string ResourceType { get; set; }
        /// <summary>
        /// The Station Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The Picture Id
        /// </summary>
        public string PictureID { get; set; }
        /// <summary>
        /// The asset id
        /// </summary>
        public string AssetID { get; set; }
        /// <summary>
        /// Is the user entitled to this station
        /// </summary>
        public bool Entitled { get; set; }
    }
}
