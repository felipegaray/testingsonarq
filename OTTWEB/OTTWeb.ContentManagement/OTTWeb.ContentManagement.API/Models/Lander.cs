﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    public class Lander
    {
        public string LanderId { get; set; }
        public SectionGroup FeaturedSection { get; set; }
        public List<SectionGroup> SectionGroups { get; set; }
    }
}
