﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The Lander Page
    /// </summary>
    public class Page
    {
        /// <summary>
        /// The Title of the section
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// The list of sections inside the Page
        /// </summary>
        public List<Section> Sections { get; set; }
    }
}
