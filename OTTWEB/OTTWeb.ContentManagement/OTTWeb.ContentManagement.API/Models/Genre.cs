﻿namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The Genre Model
    /// </summary>
    public class Genre
    {
        /// <summary>
        /// The Genre Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Genre Name
        /// </summary>
        public string Name { get; set; }
    }
}
