﻿using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The shcedule Model
    /// </summary>
    public class Schedule
    {
        /// <summary>
        /// The Schedule ID
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The resource type
        /// </summary>
        public string ResourceType { get; set; }
        /// <summary>
        /// The schedule start time
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// The schedule end time
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// The schedule duration
        /// </summary>
        public string Duration { get; set; }
        /// <summary>
        /// The name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The short name
        /// </summary>
        public string ShortName { get; set; }
        /// <summary>
        /// The description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The short description
        /// </summary>
        public string ShortDescription { get; set; }
        /// <summary>
        /// The picture ID
        /// </summary>
        public string PictureID { get; set; }
        /// <summary>
        /// The pictures
        /// </summary>
        public Dictionary<string, string> Pictures { get; set; }
        /// <summary>
        /// The ratings
        /// </summary>
        public string[] Ratings { get; set; }
        /// <summary>
        /// The rating advisories
        /// </summary>
        public string[] RatingAdvisories { get; set; }
        /// <summary>
        /// the station ID
        /// </summary>
        public string StationID { get; set; }
        /// <summary>
        /// The Genre IDs
        /// </summary>
        public string[] GenreIDs { get; set; }
        /// <summary>
        /// The Blackout
        /// </summary>
        public bool Blackout { get; set; }
        /// <summary>
        /// The Season Number
        /// </summary>
        public string Season { get; set; }
        /// <summary>
        /// The Episode Number
        /// </summary>
        public string Episode { get; set; }
        /// <summary>
        /// The show name
        /// </summary>
        public string ShowName { get; set; }
    }
}
