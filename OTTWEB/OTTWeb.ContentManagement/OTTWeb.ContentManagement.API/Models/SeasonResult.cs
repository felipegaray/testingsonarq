﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The model for the Season details and episodes information
    /// </summary>
    public class SeasonResult : Season
    {

        /// <summary>
        /// The list of episodes of a season
        /// </summary>
        [JsonProperty(Order = 1)]
        public List<SeasonEpisode> Episodes { get; set; }
    }
}
