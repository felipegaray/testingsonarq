﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OTTWeb.ContentManagement.API.Models
{
    public class Asset
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Keywords { get; set; }
        public List<string> Categories { get; set; }       
        public Dictionary<string,string> LivePlaybackURLs { get; set; }
        public Dictionary<string, string> VodPlaybackURLs { get; set; }
        public Dictionary<string, string> ImageURLs { get; set; }
        public bool IsLive { get; set; }      
        public string SubType { get; set; }
       
    }
}
