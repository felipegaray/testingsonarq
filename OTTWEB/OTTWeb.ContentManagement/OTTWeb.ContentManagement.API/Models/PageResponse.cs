﻿namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// Page Response
    /// </summary>
    public class PageResponse
    {
        /// <summary>
        /// The Lander Page
        /// </summary>
        public Page Page { get; set; }
      
    }
}
