﻿namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The Page type to be obtained
    /// </summary>
    public enum PageType
    {
        /// <summary>
        /// Home Page
        /// </summary>
        HOME = 1,
        /// <summary>
        /// Video On Demand page
        /// </summary>
        VOD = 2,
        /// <summary>
        /// Sports page
        /// </summary>
        SPORTS = 3,
        /// <summary>
        /// Live page
        /// </summary>
        LIVE = 4

    }   
}
