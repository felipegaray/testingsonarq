﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The episode that belongs to a season
    /// </summary>
    public class SeasonEpisode
    {
        /// <summary>
        /// The id of the episode
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The name of the episode
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The name of the show
        /// </summary>
        public string ShowName { get; set; }
        /// <summary>
        /// The resource type of the item
        /// </summary>
        public string ResourceType { get; set; }
        /// <summary>
        /// The description of the episode
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The language of the episode
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// The picture ID
        /// </summary>
        public string PictureID { get; set; }
        /// <summary>
        /// The dictionary of pictures
        /// </summary>
        public Dictionary<string, string> Pictures { get; set; }
        /// <summary>
        /// The rating of the episode
        /// </summary>
        public string Rating { get; set; }
        /// <summary>
        /// The release year of the episode
        /// </summary>
        public int ReleaseYear { get; set; }
        /// <summary>
        /// The season number of the season
        /// </summary>
        public string Season { get; set; }
        /// <summary>
        /// The episode number
        /// </summary>
        public string Episode { get; set; }
        /// <summary>
        /// The duration of the episode
        /// </summary>
        public Int32 Duration { get; set; }
        /// <summary>
        /// The asset IDs
        /// </summary>
        public string[] AssetIDs { get; set; }
    }
}
