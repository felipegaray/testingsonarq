﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The Station with all Schedules related to this one
    /// </summary>
    public class SchedulesByStation
    {
        /// <summary>
        /// The Station ID
        /// </summary>
        public string StationId { get; set; }
        /// <summary>
        /// The Schedule list of the Station
        /// </summary>
        public List<Schedule> Schedules { get; set; }
    }
}
