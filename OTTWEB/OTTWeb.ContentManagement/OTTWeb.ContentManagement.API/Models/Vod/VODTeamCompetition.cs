﻿using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The Team Competition Model
    /// </summary>
    public class VODTeamCompetition
    {
        /// <summary>
        /// The movie Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Respurce Type
        /// </summary>
        public string ResourceType { get; set; }

        /// <summary>
        /// The duration
        /// </summary>
        public Int32 Duration { get; set; }

        /// <summary>
        /// The Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The Asset ID of the Content
        /// </summary>
        public string AssetID { get; set; }
        
        /// <summary>
        /// The picture ID
        /// </summary>
        public string PictureID { get; set; }
        /// <summary>
        /// The dictionary of pictures
        /// </summary>
        public Dictionary<string, string> Pictures { get; set; }

        /// <summary>
        /// The Parental Rating
        /// </summary>
        public string Rating { get; set; }

        /// <summary>
        /// The rating advisories
        /// </summary>
        public string[] RatingAdvisories { get; set; }

        /// <summary>
        /// The Genres
        /// </summary>
        public List<Genre> Genres { get; set; }

        /// <summary>
        /// The Sport Name
        /// </summary>
        public string SportName { get; set; }

        /// <summary>
        /// The Tournament Name
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        /// The Primary Person Name
        /// </summary>
        public string PrimaryTeamName { get; set; }

        /// <summary>
        /// The Primary Person Picture ID
        /// </summary>
        public string PrimaryTeamPictureID { get; set; }

        /// <summary>
        /// The Secondary Person Name
        /// </summary>
        public string SecondaryTeamName { get; set; }

        /// <summary>
        /// The Secondary Person Picture ID
        /// </summary>
        public string SecondaryTeamPictureID { get; set; }
    }
}
