﻿namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// Response of the section of a page
    /// </summary>
    public class PageSectionResponse
    { 
        /// <summary>
        /// The section of a Page
        /// </summary>
        public Section Section { get; set; }
    }
}
