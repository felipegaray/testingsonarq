﻿using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The Episode Model
    /// </summary>
    public class EPGEpisode
    {
        /// <summary>
        /// The movie Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Resource Type
        /// </summary>
        public string ResourceType { get; set; }

        /// <summary>
        /// the start time
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// The end time
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// The duration
        /// </summary>
        public Int32 Duration { get; set; }

        /// <summary>
        /// The Season number which belongs the event
        /// </summary>
        public string SeasonNumber { get; set; }

        /// <summary>
        /// The Episode number
        /// </summary>
        public string EpisodeNumber { get; set; }

        /// <summary>
        /// The Episode name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The name of the show
        /// </summary>
        public string ShowName { get; set; }

        /// <summary>
        /// The Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The picture ID
        /// </summary>
        public string PictureID { get; set; }
        /// <summary>
        /// The dictionary of pictures
        /// </summary>
        public Dictionary<string, string> Pictures { get; set; }
        /// <summary>
        /// The Asset ID of the Content
        /// </summary>
        public string AssetID { get; set; }

        /// <summary>
        /// The Parental Rating
        /// </summary>
        public string Rating { get; set; }

        /// <summary>
        /// The rating advisories
        /// </summary>
        public string[] RatingAdvisories { get; set; }

        /// <summary>
        /// The Genres
        /// </summary>
        public List<Genre> Genres { get; set; }
    }
}
