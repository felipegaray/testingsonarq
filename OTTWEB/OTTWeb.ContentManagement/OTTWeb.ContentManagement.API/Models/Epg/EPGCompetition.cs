﻿using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The Competition Model
    /// </summary>
    public class EPGCompetition
    {
        /// <summary>
        /// The movie Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Resource Type
        /// </summary>
        public string ResourceType { get; set; }

        /// <summary>
        /// the start time
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// The end time
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// The duration
        /// </summary>
        public Int32 Duration { get; set; }

        /// <summary>
        /// The Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The picture ID
        /// </summary>
        public string PictureID { get; set; }

        /// <summary>
        /// The dictionary of pictures
        /// </summary>
        public Dictionary<string, string> Pictures { get; set; }

        /// <summary>
        /// The parental rating
        /// </summary>
        public string Rating { get; set; }

        /// <summary>
        /// The rating advisories
        /// </summary>
        public string[] RatingAdvisories { get; set; }

        /// <summary>
        /// The Asset ID of the Content
        /// </summary>
        public string AssetID { get; set; }

        /// <summary>
        /// The Genres
        /// </summary>
        public List<Genre> Genres { get; set; }

        /// <summary>
        /// The Name of the sport
        /// </summary>
        public string SportName { get; set; }

        /// <summary>
        /// The Tournament Name
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        /// Primary Person Name
        /// </summary>
        public string PrimaryPersonName { get; set; }

        /// <summary>
        /// Primary Person Picture ID
        /// </summary>
        public string PrimaryPersonPictureID { get; set; }

        /// <summary>
        /// Secondary Person Name
        /// </summary>
        public string SecondaryPersonName { get; set; }

        /// <summary>
        /// Secondary Person Picture ID
        /// </summary>
        public string SecondaryPersonPictureID { get; set; }
    }
}
