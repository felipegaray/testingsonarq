﻿using System;
using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The EPG Movie Model
    /// </summary>
    public class EPGMovie
    {
        /// <summary>
        /// The movie Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the start time
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// The end time
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// The Resource Type
        /// </summary>
        public string ResourceType { get; set; }

        /// <summary>
        /// The duration
        /// </summary>
        public Int32 Duration { get; set; }

        /// <summary>
        /// The Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The release year
        /// </summary>
        public int RealeaseYear { get; set; }

        /// <summary>
        /// The picture ID
        /// </summary>
        public string PictureID { get; set; }

        /// <summary>
        /// The dictionary of pictures
        /// </summary>
        public Dictionary<string, string> Pictures { get; set; }

        /// <summary>
        /// The Asset ID of the Content
        /// </summary>
        public string AssetID { get; set; }

        /// <summary>
        /// The Parental Rating
        /// </summary>
        public string Rating { get; set; }

        /// <summary>
        /// The rating advisories
        /// </summary>
        public string[] RatingAdvisories { get; set; }

        /// <summary>
        /// The Genres
        /// </summary>
        public List<Genre> Genres { get; set; }
    }
}
