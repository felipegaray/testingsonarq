﻿using System.ComponentModel.DataAnnotations;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The parameters for search by keyword
    /// </summary>
    public class SearchByKeywordParameters
    {
        /// <summary>
        /// The query parameter to match
        /// </summary>
        [Required]
        public string Query { get; set; }

        /// <summary>
        /// The language parameter to match
        /// </summary>
        [Required]
        public string Language { get; set; }

        /// <summary>
        /// The type parameter to match
        /// </summary>
        public string Types { get; set; }

        /// <summary>
        /// The number of records to be retrieved for pagination
        /// </summary>
        [Required]
        public int Count { get; set; }

        /// <summary>
        /// The offset property for pagination
        /// </summary>
        [Required]
        public int Offset { get; set; }
    }
}
