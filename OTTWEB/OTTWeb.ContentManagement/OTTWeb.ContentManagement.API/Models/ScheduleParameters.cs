﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The Scheduled querystring parameters
    /// </summary>
    public class ScheduleParameters
    {
        /// <summary>
        /// A list of station IDs separated by comma. 
        /// </summary>
        [Required]
        public string StationIDs { get; set; }
        /// <summary>
        /// The language especifier <remarks>[es|en|pt]</remarks>
        /// </summary>
        [Required]
        public string Language { get; set; }
        /// <summary>
        /// The region specifier
        /// </summary>
        [Required]
        public string Region { get; set; }
        /// <summary>
        /// Maximum number of results returned, used for pagination. If this parameter is not set, by default 10 records will be returned. Max Schedules per request 100
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// Filter schedule events with startTime greater than or equal to given datetime in ISO-8601 format
        /// </summary>
        [Required]
        public DateTime FromDatetime { get; set; }
        /// <summary>
        /// Filter schedule events with startTime less than or equal to given datetime in ISO-8601 format
        /// </summary>
        [Required]
        public DateTime ToDatetime { get; set; }
        /// <summary>
        /// The Resource types to be used as filter. A list of resource type separated by comma
        /// </summary>
        public string Types { get; set; }
    }
}
