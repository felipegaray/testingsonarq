﻿using System.ComponentModel.DataAnnotations;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The parameters for getting the season content
    /// </summary>
    public class GetSeasonParameters
    {
        /// <summary>
        /// The language parameter to match (es, en, pt)
        /// If the language is unknown you have to send "*"
        /// </summary>
        [Required]
        public string Language { get; set; }
    }
}
