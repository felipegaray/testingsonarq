﻿using System.Collections.Generic;

namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// The model for the Show details and season information
    /// </summary>
    public class ShowResult
    {
        /// <summary>
        /// The id of the show
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The name of the show
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The description of the show
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The rating of the show
        /// </summary>
        public string Rating { get; set; }
        /// <summary>
        /// Language of the show
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// The picture ID
        /// </summary>
        public string PictureID { get; set; }
        /// <summary>
        /// The release year of the episode
        /// </summary>
        public int ReleaseYear { get; set; }
        /// <summary>
        /// The list of pictures
        /// </summary>
        public Dictionary<string, string> Pictures { get; set; }
        /// <summary>
        /// The number of seasons
        /// </summary>
        public int Seasons { get; set; }    
        /// <summary>
        /// The detailed list of seasons
        /// </summary>
        public List<Season> SeasonsList { get; set; }
        /// <summary>
        /// The Genres
        /// </summary>
        public List<Genre> Genres { get; set; }
    }
}
