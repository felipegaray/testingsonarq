﻿namespace OTTWeb.ContentManagement.API.Models
{
    /// <summary>
    /// Represents the seasons of a show
    /// </summary>
    public class Season
    {
        /// <summary>
        /// The Id of the season
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The name of the season
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The short Name of the season
        /// </summary>
        public string ShortName { get; set; }
        /// <summary>
        /// The language of the season
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// The Region IDS
        /// </summary>
        public string[] RegionIDs { get; set; }
        /// <summary>
        /// The release year of the episode
        /// </summary>
        public int ReleaseYear { get; set; }
        /// <summary>
        /// The season number
        /// </summary>
        public short SeasonNumber { get; set; }
        /// <summary>
        /// The episode count
        /// </summary>
        public short EpisodeCount { get; set; }
        /// <summary>
        /// The ID list of episodes
        /// </summary>
        public string[] EpisodeIDs { get; set; }
    }
}
