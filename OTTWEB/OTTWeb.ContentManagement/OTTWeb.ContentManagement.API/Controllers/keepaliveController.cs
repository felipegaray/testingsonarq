﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace OTTWeb.ContentManagement.API.Controllers
{
    [Route("content")]
    [Produces("application/json")]
    public class keepAliveController
        : ControllerBase
    {
        [Route("keepAlive")]
        [HttpGet]
        public IActionResult GetAlive()
        {
            return Ok(DateTime.UtcNow);
        }
    }
}