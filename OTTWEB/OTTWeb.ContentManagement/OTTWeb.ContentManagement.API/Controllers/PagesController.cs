﻿using AppDynamics;
using AutoMapper;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OTTWeb.Common.API.Controllers;
using OTTWeb.Common.API.Infrastructure;
using OTTWeb.Common.API.Models.Errors;
using OTTWeb.ContentManagement.API.Models;
using OTTWeb.ContentManagement.Services;
using System;
using System.Threading.Tasks;
using SERVICE = OTTWeb.ContentManagement.Services.Models;

namespace OTTWeb.ContentManagement.API.Controllers
{
    /// <summary>
    /// This controller manages all resources related to the pages or landers
    /// </summary>
    [Produces("application/json")]
    [Route("content/Pages")]
    [ValidateModel]
    public class PagesController
        : OTTBaseController
    {
        #region Injected Services

        private IContentManagementService _service;
        private IMapper _mapper;
        private Microsoft.Extensions.Logging.ILogger _logger;
        private IErrorActionResultFactory _errorResultFactory;

        #endregion

        #region Constructors

        /// <summary>
        /// The constructor
        /// </summary>
        /// <param name="contentManagementService">Service to be injected</param>
        /// <param name="mapper">Mapper to be injected</param>
        /// /// <param name="errorResultFactory">Factory class to instantiate an ActionResult in case of error</param>
        /// <param name="loggerFactory"></param>
        public PagesController(IContentManagementService contentManagementService, IMapper mapper, IErrorActionResultFactory errorResultFactory, ILoggerFactory loggerFactory)
        {
            _service = contentManagementService;
            _mapper = mapper;
            _errorResultFactory = errorResultFactory;
            _logger = loggerFactory.CreateLogger<PagesController>();
        }

        #endregion

        /// <summary>
        /// Gets the page using the type of the page and the region of the user account
        /// </summary>
        /// <param name="pageTypeId">The type of the page to search</param>
        /// <returns>The structure of the page with, sections, subsections and assets</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>

        #region Response Attributes

        [ProducesResponseType(type: typeof(PageResponse), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route(template: "{pageTypeId}")]
        // GET: content/Pages/{pageTypeId}
        public async Task<IActionResult> Get(PageType pageTypeId)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/Pages", "ASP_DOTNET", "");

                //Calls the Get Page Implementation
                var result = await _service.GetPage(pageTypeId.ToString().ToUpper(), bt);

                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<PageResponse>(result.ContentData));
            }
        }

        /// <summary>
        /// Gets the data for a specific section of a Page(Lander) defined in the pageID,
        /// it is organized in sections, subsections and the list of Assets
        /// </summary>
        /// <param name="pageTypeId">The type of the page to search</param>
        /// <param name="sectionId">The type of the section to search</param>
        /// <param name="sectionParameters">The queryString parameters</param>
        /// <returns>The structure of the page with, sections, subsections and assets</returns>
        /// /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>

        #region Response Attributes

        [ProducesResponseType(type: typeof(PageResponse), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route(template: "{pageTypeId}/sections/{sectionId}")]
        // GET: content/Pages/{pageTypeId}/sections/{sectionId}
        public async Task<IActionResult> GetSection(PageType pageTypeId, string sectionId, [FromQuery] GetPageSectionParameters sectionParameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/Pages/Sections", "ASP_DOTNET", "");
                //Mapping of Parameters
                SERVICE.GetPageSectionParameters request = _mapper.Map<SERVICE.GetPageSectionParameters>(sectionParameters);
                //Calls the Get Section Implementation
                var result = await _service.GetPageSection(sectionParameters.Top, pageTypeId.ToString().ToUpper(), sectionId, bt);

                if (!result.IsSuccessful)
                {
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }

                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<PageSectionResponse>(result.ContentData));
            }
        }
    }
}