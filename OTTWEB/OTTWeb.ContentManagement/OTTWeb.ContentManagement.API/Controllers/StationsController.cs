﻿using AppDynamics;
using AutoMapper;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OTTWeb.Common.API.Controllers;
using OTTWeb.Common.API.Infrastructure;
using OTTWeb.Common.API.Models.Errors;
using OTTWeb.ContentManagement.API.Models;
using OTTWeb.ContentManagement.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SERVICE = OTTWeb.ContentManagement.Services.Models;

namespace OTTWeb.ContentManagement.API.Controllers
{
    /// <summary>
    /// This Controller manages all resources related to Stations
    /// </summary>
    [Produces("application/json")]
    [Route("content/[controller]")]
    [ValidateModel]
    public class StationsController
        : OTTBaseController
    {
        #region Injected Services

        private IStationService _service { get; set; }
        private IMapper _mapper;
        private Microsoft.Extensions.Logging.ILogger _logger;
        private IErrorActionResultFactory _errorResultFactory;

        #endregion

        #region Constructors

        /// <summary>
        /// The constructor
        /// </summary>
        /// <param name="stationService">Service to be injected</param>
        /// <param name="mapper">Mapper to be injected</param>
        /// <param name="errorResultFactory">Factory class to instantiate an ActionResult in case of error</param>
        /// <param name="loggerFactory"></param>
        public StationsController(IStationService stationService, IMapper mapper, IErrorActionResultFactory errorResultFactory, ILoggerFactory loggerFactory)
        {
            _service = stationService;
            _mapper = mapper;
            _errorResultFactory = errorResultFactory;
            _logger = loggerFactory.CreateLogger<StationsController>();
        }

        #endregion

        /// <summary>
        /// This method gets a list of stations
        /// </summary>
        /// <param name="stationParameters">The querystring parameters</param>
        /// <returns>A station object</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Stations

        #region Response Attributes

        [ProducesResponseType(type: typeof(List<Station>), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetStations([FromQuery]GetStationsParameters stationParameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/Stations", "ASP_DOTNET", "");
                //Mapping from Models.stationParameters => Service.Models.stationParameters
                var request = _mapper.Map<SERVICE.GetStationsParameters>(stationParameters);
                //Calls the Get Asset Implementation
                var result = await _service.GetStations(request, bt);

                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<List<Station>>(result.ContentData.List));
            }
        }

        /// <summary>
        /// This method gets a station data
        /// </summary>
        /// <param name="stationId">The station Id</param>
        /// <param name="getStationParameters">The querystring parameters</param>
        /// <returns>A station object</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Stations/{stationId}

        #region Response Attributes

        [ProducesResponseType(type: typeof(Station), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("{stationId}")]
        public async Task<IActionResult> GetStation(string stationId, [FromQuery]GetStationParameters getStationParameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/Stations/Station", "ASP_DOTNET", "");
                //Mapping from Models.stationParameters => Service.Models.stationParameters
                var request = _mapper.Map<SERVICE.GetStationParameters>(getStationParameters);
                //Calls the Get Asset Implementation
                var result = await _service.GetStation(stationId, request, bt);

                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<Station>(result.ContentData));
            }
        }
    }
}