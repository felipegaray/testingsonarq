using AppDynamics;
using AutoMapper;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OTTWeb.Common.API.Controllers;
using OTTWeb.Common.API.Infrastructure;
using OTTWeb.Common.API.Models.Errors;
using OTTWeb.ContentManagement.API.Models;
using OTTWeb.ContentManagement.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SERVICE = OTTWeb.ContentManagement.Services.Models;

namespace OTTWeb.ContentManagement.API.Controllers
{
    /// <summary>
    /// This Controller manages all resources related to an asset
    /// </summary>
    [Produces("application/json")]
    [Route("content/Assets")]
    [ValidateModel]
    public class AssetsController
        : OTTBaseController
    {
        #region Injected Services

        private IContentManagementService _service { get; set; }
        private IMapper _mapper;
        private Microsoft.Extensions.Logging.ILogger _logger;
        private IErrorActionResultFactory _errorResultFactory;

        #endregion

        #region Constructors

        /// <summary>
        /// The constructor
        /// </summary>
        /// <param name="contentManagementService">Service to be injected</param>
        /// <param name="mapper">Mapper to be injected</param>
        /// <param name="errorResultFactory">Factory class to instantiate an ActionResult in case of error</param>
        /// <param name="loggerFactory"></param>
        public AssetsController(IContentManagementService contentManagementService, IMapper mapper, IErrorActionResultFactory errorResultFactory, ILoggerFactory loggerFactory)
        {
            _service = contentManagementService;
            _mapper = mapper;
            _errorResultFactory = errorResultFactory;
            _logger = loggerFactory.CreateLogger<AssetsController>();
        }

        #endregion

        /// <summary>
        /// Get an asset by Id
        /// </summary>
        /// <param name="assetId">The Asset Id</param>
        /// <returns>The asset that match the Id provided</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Assets/{id}

        #region Response Attributes

        [ProducesResponseType(type: typeof(SearchAsset), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("getById/{assetId}")]
        public async Task<IActionResult> GetAssetById(string assetId)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
				var test;
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/getById", "ASP_DOTNET", "");
                //Calls the Get Asset Implementation
                var result = await _service.SearchAssetById(assetId, bt);
                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<SearchAsset>(result.ContentData));
            }
        }

        /// <summary>
        /// Search assets by keyword
        /// </summary>
        /// <param name="searchByKeywordParameters">The parameters to perform a search</param>
        /// <response code="200">The matching assets</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Assets/SearchByKeyword?language={}&query={}

        #region Response Attributes

        [ProducesResponseType(type: typeof(SearchResult[]), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("searchByKeyword")]
        public async Task<IActionResult> SearchAssetsByKeyword([FromQuery]SearchByKeywordParameters searchByKeywordParameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/searchByKeyword", "ASP_DOTNET", "");
                //Mapping of parameters
                var request = _mapper.Map<SERVICE.SearchByKeywordParameters>(searchByKeywordParameters);
                //Returns the Status Code from Orbis
                var result = await _service.SearchAssetsByKeyword(request, bt);

                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<List<SearchAsset>>(result.ContentData.Assets));
            }
        }
    }
}