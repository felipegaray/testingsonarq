﻿using AppDynamics;
using AutoMapper;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OTTWeb.Common.API.Controllers;
using OTTWeb.Common.API.Infrastructure;
using OTTWeb.Common.API.Models.Errors;
using OTTWeb.ContentManagement.API.Models;
using OTTWeb.ContentManagement.Services.Interfaces;
using System;
using System.Threading.Tasks;
using SERVICE = OTTWeb.ContentManagement.Services.Models;

namespace OTTWeb.ContentManagement.API.Controllers
{
    /// <summary>
    /// This Controller manages all resources related to Shows
    /// </summary>
    [Produces("application/json")]
    [Route("content/Shows")]
    [ValidateModel]
    public class ShowsController
        : OTTBaseController
    {
        #region Injected Services

        private IShowService _service { get; set; }
        private IMapper _mapper;
        private Microsoft.Extensions.Logging.ILogger _logger;
        private IErrorActionResultFactory _errorResultFactory;

        #endregion

        #region Constructors

        /// <summary>
        /// The constructor
        /// </summary>
        /// <param name="showService">Service to be injected</param>
        /// <param name="mapper">Mapper to be injected</param>
        /// <param name="errorResultFactory">Factory class to instantiate an ActionResult in case of error</param>
        /// <param name="loggerFactory"></param>
        public ShowsController(IShowService showService, IMapper mapper, IErrorActionResultFactory errorResultFactory, ILoggerFactory loggerFactory)
        {
            _service = showService;
            _mapper = mapper;
            _errorResultFactory = errorResultFactory;
            _logger = loggerFactory.CreateLogger<ShowsController>();
        }

        #endregion

        /// <summary>
        /// This method gets the detail of a show and its seasons
        /// </summary>
        /// <param name="showId">The id of the show to be searched</param>
        /// <param name="showParameters">The querystring parameters</param>
        /// <returns>A show object</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Show

        #region Response Attributes

        [ProducesResponseType(type: typeof(ShowResult), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("{showId}")]
        public async Task<IActionResult> GetShow(string showId, [FromQuery]GetShowParameters showParameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                var bt = AgentSDK.StartBusinessTransaction("/Content/Shows", "ASP_DOTNET", "");

                SERVICE.GetShowParameters request = _mapper.Map<SERVICE.GetShowParameters>(showParameters);
                //Calls the Get Show Implementation
                var result = await _service.GetShowAndSeasons(showId, request, bt);
                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<ShowResult>(result.ContentData));
            }
        }

        /// <summary>
        /// Returns a season detail and its episodes
        /// </summary>
        /// <param name="seasonId">The id of the season to be searched</param>
        /// <param name="seasonParameters">The querystring parameters</param>
        /// <returns>A season object</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Show/Season

        #region Response Attributes

        [ProducesResponseType(type: typeof(SeasonResult), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("Seasons/{seasonId}")]
        public async Task<IActionResult> GetSeason(string seasonId, [FromQuery]GetSeasonParameters seasonParameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                var bt = AgentSDK.StartBusinessTransaction("/Content/Shows/Seasons", "ASP_DOTNET", "");

                SERVICE.GetSeasonParameters request = _mapper.Map<SERVICE.GetSeasonParameters>(seasonParameters);
                //Calls the Get Season Implementation
                var result = await _service.GetSeason(seasonId, request, bt);
                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<SeasonResult>(result.ContentData));
            }
        }
    }
}