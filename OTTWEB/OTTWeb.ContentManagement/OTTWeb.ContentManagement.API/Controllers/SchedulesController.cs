﻿using AppDynamics;
using AutoMapper;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OTTWeb.Common.API.Controllers;
using OTTWeb.Common.API.Infrastructure;
using OTTWeb.Common.API.Models.Errors;
using OTTWeb.ContentManagement.API.Models;
using OTTWeb.ContentManagement.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SERVICE = OTTWeb.ContentManagement.Services.Models;

namespace OTTWeb.ContentManagement.API.Controllers
{
    /// <summary>
    /// This Controller manages all resources related to Schedules EPG
    /// </summary>
    [Produces("application/json")]
    [Route("content/[controller]")]
    [ValidateModel]
    public class SchedulesController
        : OTTBaseController
    {
        #region Injected Services

        private IScheduleService _service;
        private IMapper _mapper;
        private Microsoft.Extensions.Logging.ILogger _logger;
        private IErrorActionResultFactory _errorResultFactory;

        #endregion

        #region Constructors

        /// <summary>
        /// The constructor
        /// </summary>
        /// <param name="scheduleService">Service to be injected</param>
        /// <param name="mapper">Mapper to be injected</param>
        /// <param name="errorResultFactory">Factory class to instantiate an ActionResult in case of error</param>
        /// <param name="loggerFactory"></param>
        public SchedulesController(IScheduleService scheduleService, IMapper mapper, IErrorActionResultFactory errorResultFactory, ILoggerFactory loggerFactory)
        {
            _service = scheduleService;
            _mapper = mapper;
            _errorResultFactory = errorResultFactory;
            _logger = loggerFactory.CreateLogger<SchedulesController>();
        }

        #endregion

        /// <summary>
        /// This method gets a list of programs grouped by station
        /// </summary>
        /// <param name="scheduleParameters">The querystring parameters</param>
        /// <returns>A shcedule object</returns>
        /// <response code="200">Returns a list of schedules grouped by station</response>
        /// <response code="400">For any error</response>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Schedules

        #region Response Attributes

        [ProducesResponseType(type: typeof(List<SchedulesByStation>), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetSchedules([FromQuery]ScheduleParameters scheduleParameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/Schedules", "ASP_DOTNET", "");
                //Mapping from Models.ScheduleParameters => Service.Models.ScheduleParameters
                var request = _mapper.Map<SERVICE.ScheduleParameters>(scheduleParameters);
                //Calls the Get Asset Implementation
                var result = await _service.GetSchedules(request, bt);

                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);
                return Ok(_mapper.Map<List<SchedulesByStation>>(result.ContentData.List));
            }
        }

        /// <summary>
        /// This Method gets all data of a movie from EPG
        /// </summary>
        /// <param name="scheduleMovieId">The Movie Id</param>
        /// <param name="parameters">The parameters to be sent in the querystring</param>
        /// <returns>A movie object</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Schedules/Movies/scheduleMovieId

        #region Response Attributes

        [ProducesResponseType(type: typeof(EPGMovie), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("Movies/{scheduleMovieId}")]
        public async Task<IActionResult> GetScheduledMovie(string scheduleMovieId, [FromQuery]GetScheduledContentParameters parameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/Schedules/Movies", "ASP_DOTNET", "");

                SERVICE.GetScheduledContentParameters request = _mapper.Map<SERVICE.GetScheduledContentParameters>(parameters);
                //Calls the Get Asset Implementation
                var result = await _service.GetScheduledMovie(scheduleMovieId, request, bt);
                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<EPGMovie>(result.ContentData));
            }
        }

        /// <summary>
        /// This Method gets all data of a Episode from EPG
        /// </summary>
        /// <param name="scheduleEpisodeId">The Episode Id</param>
        /// <param name="parameters">The parameters to be sent in the querystring</param>
        /// <returns>A Episode object</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Schedules/Episodes/scheduleEpisodeId

        #region Response Attributes

        [ProducesResponseType(type: typeof(EPGEpisode), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("Episodes/{scheduleEpisodeId}")]
        public async Task<IActionResult> GetScheduledEpisode(string scheduleEpisodeId, [FromQuery]GetScheduledContentParameters parameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/Schedules/Episodes", "ASP_DOTNET", "");

                SERVICE.GetScheduledContentParameters request = _mapper.Map<SERVICE.GetScheduledContentParameters>(parameters);
                //Calls the Get Implementation
                var result = await _service.GetScheduledEpisode(scheduleEpisodeId, request, bt);

                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<EPGEpisode>(result.ContentData));
            }
        }

        /// <summary>
        /// This Method gets all data of an Event from EPG
        /// </summary>
        /// <param name="scheduleEventId">The Event Id</param>
        /// <param name="parameters">The parameters to be sent in the querystring</param>
        /// <returns>A Event object</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Schedules/Events/scheduleEventId

        #region Response Attributes

        [ProducesResponseType(type: typeof(Event), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("Events/{scheduleEventId}")]
        public async Task<IActionResult> GetScheduledEvent(string scheduleEventId, [FromQuery]GetScheduledContentParameters parameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/Schedules/Events", "ASP_DOTNET", "");

                SERVICE.GetScheduledContentParameters request = _mapper.Map<SERVICE.GetScheduledContentParameters>(parameters);
                //Calls the Get Asset Implementation
                var result = await _service.GetScheduledEvent(scheduleEventId, request, bt);

                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<Event>(result.ContentData));
            }
        }

        /// <summary>
        /// This Method gets all data of a Team Competition from EPG
        /// </summary>
        /// <param name="scheduleTeamCompetitionId">The Team Competition Id</param>
        /// <param name="parameters">The parameters to be sent in the querystring</param>
        /// <returns>A TeamCompetition object</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Schedules/TeamCompetitions/scheduleTeamCompetitionId

        #region Response Attributes

        [ProducesResponseType(type: typeof(EPGTeamCompetition), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("TeamCompetitions/{scheduleTeamCompetitionId}")]
        public async Task<IActionResult> GetScheduledTeamCompetition(string scheduleTeamCompetitionId, [FromQuery]GetScheduledContentParameters parameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/Schedules/TeamCompetitions", "ASP_DOTNET", "");

                SERVICE.GetScheduledContentParameters request = _mapper.Map<SERVICE.GetScheduledContentParameters>(parameters);
                //Calls the Get Asset Implementation
                var result = await _service.GetScheduledTeamCompetition(scheduleTeamCompetitionId, request, bt);

                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<EPGTeamCompetition>(result.ContentData));
            }
        }

        /// <summary>
        /// This Method gets all data of a Competition from EPG
        /// </summary>
        /// <param name="scheduleCompetitionId">The Competition Id</param>
        /// <param name="parameters">The parameters to be sent in the querystring</param>
        /// <returns>A Competition object</returns>
        /// <response code="200">The asset that match the Id provided</response>
        /// <response code="404">The requested asset doesn´t exist</response>
        /// <response code="400"> Bad Request - Client request is malformed or syntactically incorrect.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that ErrorResultModel will include detailed information of the error intended for a developer using the API. </response>
        /// <response code="422"> Unprocessable Entity - Reserved for Business Errors returned by ORBIS service.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note that the "message" field on "ErrorResultModel" will include the error message and the "code" field will include the error code, both forwarded from ORBIS services.</response>
        /// <response code="401"> Unauthorized - Authorization fails in ORBIS service. The authorization token (SessionToken or APItoken) is invalid for some reason.
        /// ----------------------------------------------------------------------------------------------------------------------------------------------------
        /// Note That ORBIS error code will be included in the response to help the client application to determine the specific reason for the error </response>
        /// <response code="502"> Bad Gateway - Unhandled exception thrown on ORBIS service.
        ///----------------------------------------------------------------------------------------------------------------------------------------------------
        /// This error can occur due to:
        /// -Timeout or network errors accessing ORBIS services from OTTWeb Back-end service.
        /// -Deserialization error caused by an unexpected contract breaking change in ORBIS service.</response>
        /// <response code="500"> Internal Server Error - Unhandled exception thrown on OTTWeb Back-end service. </response>
        // GET: content/Schedules/Competitions/scheduleCompetitionId

        #region Response Attributes

        [ProducesResponseType(type: typeof(VODCompetition), statusCode: 200)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 400)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 422)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 401)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 502)]
        [ProducesResponseType(type: typeof(ErrorResultModel), statusCode: 500)]

        #endregion

        [HttpGet]
        [Route("Competitions/{scheduleCompetitionId}")]
        public async Task<IActionResult> GetScheduledCompetition(string scheduleCompetitionId, [FromQuery]GetScheduledContentParameters parameters)
        {
            using (LogicalThreadContext.Stacks["NDC"].Push(Guid.NewGuid().ToString()))
            {
                //Begining of BT for AppDynamics
                var bt = AgentSDK.StartBusinessTransaction("/Content/Schedules/Competitions", "ASP_DOTNET", "");

                SERVICE.GetScheduledContentParameters request = _mapper.Map<SERVICE.GetScheduledContentParameters>(parameters);
                //Calls the Get Asset Implementation
                var result = await _service.GetScheduledCompetition(scheduleCompetitionId, request, bt);

                if (!result.IsSuccessful)
                {
                    //End of BT for AppDynamics
                    AgentSDK.StopBusinessTransaction(bt);
                    return _errorResultFactory.GetErrorActionResult(result);
                }
                //End of BT for AppDynamics
                AgentSDK.StopBusinessTransaction(bt);

                return Ok(_mapper.Map<VODCompetition>(result.ContentData));
            }
        }
    }
}