﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OTTWeb.ContentManagement.API
{
    public partial class Startup
    {
        
        private void ConfigureServices_Swagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Content Management API",
                    Description = "This API provides a set of endpoint to manage all actions related to the content",
                    TermsOfService = "",
                    Contact = new Contact()
                });
                //Adding Bearer Authorization
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "Please enter JWT with Bearer into field",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                    { "Bearer", Enumerable.Empty<string>() },
                });

                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "OTTWeb.ContentManagement.API.xml");
                c.IncludeXmlComments(xmlPath);
                c.DescribeAllEnumsAsStrings();
                c.DescribeAllParametersInCamelCase();
            });           
        }

        private void Configure_Swagger(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger(c => {
                c.RouteTemplate = "content_swagger/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/content_swagger/v1/swagger.json", "Content management API V1");
                c.RoutePrefix = "content_swagger";
            });
        }

    }
}
