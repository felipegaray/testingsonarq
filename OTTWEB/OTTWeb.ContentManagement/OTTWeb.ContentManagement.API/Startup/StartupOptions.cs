﻿using Microsoft.Extensions.DependencyInjection;
using OTTWeb.ContentManagement.OCM.Configuration;

namespace OTTWeb.ContentManagement.API
{
    public partial class Startup
    {
        
        private void ConfigureServices_Options(IServiceCollection services)
        {
            services.AddOptions();            
            services.Configure<AppSetting>(Configuration.GetSection("AppSetting"));
            
        }
        
    }
}
