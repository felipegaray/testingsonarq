﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;


namespace OTTWeb.ContentManagement.API
{
    public partial class Startup
    {        
        // This method gets called by the runtime. Use this method to add services to the container.
        private void ConfigureServices_Logs(IServiceCollection services)
        {
            
        }

        private void Configure_Logs(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddLog4Net();
        }
    }
}
