﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Converters;
using OTTWeb.Common.API.Infrastructure;
using OTTWeb.Common.Http.API.Configuration;
using OTTWeb.Common.Http.Extensions.Microsoft.DependencyInjection;
using OTTWeb.Common.Middlewares;
using OTTWeb.Common.Proxy.Exceptions;
using OTTWeb.Common.Services.Infrastructure;
using OTTWeb.ContentManagement.OCM.Implementations;
using OTTWeb.ContentManagement.OCM.Interfaces;
using OTTWeb.ContentManagement.Services;
using OTTWeb.ContentManagement.Services.Implementations;
using OTTWeb.ContentManagement.Services.Infrastructure;
using OTTWeb.ContentManagement.Services.Interfaces;
using System;

namespace OTTWeb.ContentManagement.API
{
    public partial class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureServices_Options(services);
            ConfigureServices_Logs(services);
            ConfigureServices_Swagger(services);
            services.AddAutoMapper()
                    .AddHForwardHeadersService(GetProxySettings());

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                builder => builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            });

            //Add and Configure MVC
            AddAndConfigureMvc(services);

            services.AddScoped<IErrorActionResultFactory, ErrorActionResultFactory>();
            services.AddScoped<IProxyExceptionFactory, ProxyExceptionFactory>();
            services.AddScoped<IServiceResponseFactory, ServiceResponseFactory>();

            //Injects the Services
            services.AddTransient<IContentManagementService, ContentManagementService>();
            services.AddTransient<IScheduleService, ScheduleService>();
            services.AddTransient<IStationService, StationService>();
            services.AddTransient<IVodService, VodService>();
            services.AddTransient<IShowService, ShowService>();
            services.AddTransient<IOCMProxy, OCMProxy>();
            services.AddSingleton<IParentalRating, ParentalRating>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            int swagger = 0;
            int cors = 0;
            bool parsed = Int32.TryParse(Environment.GetEnvironmentVariable("ENV-CONTENT-SWG"), out swagger);

            if (parsed && swagger == 1)
            {
                Configure_Swagger(app, env);
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            bool parsedCors = Int32.TryParse(Environment.GetEnvironmentVariable("ENV-ENABLE-CORS"), out cors);
            if (parsedCors && cors == 1)
            {
                app.UseCors("CorsPolicy");
            }

            Configure_Logs(app, env, loggerFactory);
            app.UseForwardedHeadersMiddlewere()
                .UseMvc();
        }

        #region "PRIVATE METHODS"

        private void AddAndConfigureMvc(IServiceCollection service)
        {
            IMvcBuilder mvcConfigurations = service.AddMvc();

            //Adding a Custom Attribute to Validate the Model State
            mvcConfigurations.AddMvcOptions(option => option.Filters.Add(new ValidateModelAttribute()));
            //Configure Json Options
            mvcConfigurations.AddJsonOptions(SetJsonOptions());
        }

        private static System.Action<Microsoft.AspNetCore.Mvc.MvcJsonOptions> SetJsonOptions()
        {
            return options =>
            {
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.Converters.Add(new StringEnumConverter { CamelCaseText = true });
            };
        }

        /// <summary>
        /// Gets the proxy settings after validations from the enviroment variable
        /// </summary>
        /// <returns></returns>
        private ProxySettings GetProxySettings()
        {
            var proxySetting = Configuration.GetSection("ProxySettings").Get<ProxySettings>();

            bool parsed = Int32.TryParse(Environment.GetEnvironmentVariable(proxySetting.RequestBuilderSettings.TimeoutEnvironmentVariableName), out int timeout);

            if (parsed)
            {
                proxySetting.RequestBuilderSettings.Timeout = timeout;
            }
            return proxySetting;
        }

        #endregion "PRIVATE METHODS"
    }
}